<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
// defined('GCM_URL') 					OR define('GCM_URL', 'https://fcm.googleapis.com/fcm/send');
defined('GCM_URL') 					OR define('GCM_URL', 'https://fcm.googleapis.com/v1/projects/fh-point/messages:send');
defined('GCM_API') 					OR define('GCM_API', 'AAAAOQUOlVU:APA91bEx_GqBIEn_LHKK7o0DiOHl-GGkVmNmat9d0ZENyb6AXPm5-GlOkpIN5tnTN_650-vxm0l7iC2jF1XVPU3KD4gbFSuKp3WjTILU06H8JoGRjjxhfhNPIh3L27vdVojvZMOpbzEF');
defined('GOSEND_CLIENT_ID') 		OR define('GOSEND_CLIENT_ID', 'sukses-jaya-engine');
defined('GOSEND_PASS_KEY') 			OR define('GOSEND_PASS_KEY', '3af72f53ba3e250d1fd008362d4c87eefb178c59b20e6e974e06c6c580a71302');
defined('GOSEND_ESTIMATE_URL') 		OR define('GOSEND_ESTIMATE_URL', 'https://integration-kilat-api.gojekapi.com/gokilat/v10/calculate/price');
defined('GOSEND_BOOKING_URL') 		OR define('GOSEND_BOOKING_URL', 'https://integration-kilat-api.gojekapi.com/gokilat/v10/booking');

// ASSETS PATH
defined('BASE_URL_ASSETS')      	   	OR define('BASE_URL_ASSETS', './assets/');
defined('URL_TOKENS')      	   			OR define('URL_TOKENS', BASE_URL_ASSETS.'tokens/');


// DEFAULT VALUES
defined('EMPTY_DATE')      	   			OR define('EMPTY_DATE', '0000-00-00 00:00:00');
defined('DEFAULT_WAREHOUSE')      	   	OR define('DEFAULT_WAREHOUSE', 'ID2');
defined('BANDAHARA_WAREHOUSE')      	OR define('BANDAHARA_WAREHOUSE', 'ID-190121-105451-0003');
defined('UNDEFINED_QUEUE')      		OR define('UNDEFINED_QUEUE', '#####');
/* End of file constants.php */
/* Location: ./application/config/constants.php */