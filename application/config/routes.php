<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/
// define( 'API_VER', 'ver' );
 
// array_key_exists( API_VER, $_REQUEST ) ? $v = $_REQUEST[ API_VER ] : $v = '1';

// if ( file_exists( realpath(dirname(__FILE__) . '/..') ."/modules/api/controllers/" ."V{$v}.php" ) ) {
// 	$route["api/(:any)"] = "api/v{$v}/$1";
// } else {
//     $error  			= new stdClass();
//     $error->error    	= true;
//     $error->message  	= 'INVALID_API_VERSION';
//     echo json_encode( $error );
//     exit;
// }

// if ( file_exists( realpath(dirname(__FILE__) . '/..') ."/modules/api_driver/controllers/" ."V{$v}.php" ) ) {
// 	$route["api_driver/(:any)"] = "api_driver/v{$v}/$1";
// } else {
//     $error  			= new stdClass();
//     $error->error    	= true;
//     $error->message  	= 'INVALID_API_VERSION';
//     echo json_encode( $error );
//     exit;
// }

$route['default_controller'] 	 = "dashboard";
$route['404_override'] 			 = 'error/error_404';

// $route['master/position']        = 'position';
// $route['master/position/(:any)'] = 'position/$1';
// $route['master/study']    	     = 'study';
// $route['master/study/(:any)']    = 'study/$1';
// $route['master/edulevel'] 	     = 'edulevel';
// $route['master/edulevel/(:any)'] = 'edulevel/$1';
// $route['master/adver'] 	         = 'adver';
// $route['master/adver/(:any)']    = 'adver/$1';
// $route['master/student'] 	     = 'pasien';
// $route['master/student/(:any)']  = 'pasien/$1';
// $route['master/teacher'] 		 = 'doctor';
// $route['master/teacher/(.*)'] 	 = 'doctor/$1';

/* End of file routes.php */
/* Location: ./application/config/routes.php */