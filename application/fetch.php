<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fetch extends MX_Controller {

    private $rule_valid     = 'xss_clean|encode_php_tags';

    function __construct() 
    {
        parent::__construct();

    }

    /* Start Fetch Core Controller */

    public function index()
    {   
        return false;
    }

    public function position( $id = null )
    {
        if ( is_null( $id ) )
        {
            $q          = $_GET['q'];
            $records = $this->m_global->get_data_all( 'position', NULL, ['position_status' => '1', 'position_name LIKE' => "%$q%"], '*', null, null, 0, 10 );

            $data = [];
        
            for ( $i = 0; $i < count( $records ); $i++ ) 
            {
                $data[$i] = ['id' => $records[$i]->position_id, 'name' => $records[$i]->position_name];
            }

            echo json_encode( ['item' => $data] );
        }
        else
        {
            $records    = $this->m_global->get_data_all( 'position', null, ['position_id' => $id]);
            $tmp        = [
                            ['id' => $id, 'name' => $records[0]->position_name]
                        ];

            echo json_encode( $tmp );
        }
    }

    public function employee( $id = null )
    {
        if ( is_null( $id ) )
        {
            $q          = $_GET['q'];
            $employee   = $this->m_global->get_data_all( 'employee', null, ['employee_name LIKE' => '%'.$q.'%', 'employee_status' => '1'], '*', null, null, 0, 10 );
            
            $data = [];
            for ( $i = 0; $i < count( $employee ); $i++ ) 
            {
                $data[$i] = ['id' => $employee[$i]->employee_id, 'name' => $employee[$i]->employee_name];
            }

            echo json_encode( ['item' => $data] );
        }
        else
        {
            $employee    = $this->m_global->get_data_all( 'employee', null, ['employee_id' => $id]);
            $tmp        = [
                            ['id' => $id, 'name' => $employee[0]->employee_name]
                        ];

            echo json_encode( $tmp );
        }
    }

    public function dokter( $id = null )
    {
        if ( is_null( $id ) )
        {
            $q          = $_GET['q'];
            $select     = 'employee_id, employee_name, schedule_datetime';
            $join       = [ ['table' => 'schedule', 'on' => 'employee_id=schedule_employee_id', 'tipe' => 'left'] ];
            $employee   = $this->m_global->get_data_all( 'employee', $join, ['employee_name LIKE' => '%'.$q.'%', 'employee_status' => '1'], $select, "schedule_datetime IS NULL", null, 0, 10 );
            
            $data = [];
            for ( $i = 0; $i < count( $employee ); $i++ ) 
            {
                $data[$i] = ['id' => $employee[$i]->employee_id, 'name' => $employee[$i]->employee_name];
            }

            echo json_encode( ['item' => $data] );
        }
        else
        {
            $employee    = $this->m_global->get_data_all( 'employee', null, ['employee_id' => $id]);
            $tmp        = [
                            ['id' => $id, 'name' => $employee[0]->employee_name]
                        ];

            echo json_encode( $tmp );
        }
    }

    public function religion( $id = NULL )
    {
        $tmp    = [
                    ['id' => 'Islam', 'name' => 'Islam'],
                    ['id' => 'Protestan', 'name' => 'Protestan'],
                    ['id' => 'Katolik', 'name' => 'Katolik'],
                    ['id' => 'Hindu', 'name' => 'Hindu'],
                    ['id' => 'Budha', 'name' => 'Budha'],
                    ['id' => 'Konghucu', 'name' => 'Konghucu']
                ];

        if ( is_null( $id ) )
        {

            echo json_encode( ['item' => $tmp] );

        } else {

            $hasil = [['id' => $id, 'name' => $id ]];

            echo json_encode( $hasil );

        }
    }

    public function birth_place( $q )
    {
        $records = $this->m_global->get_data_all( 'employee', NULL, ['employee_birthplace LIKE' => "%$q%"], 'DISTINCT(employee_birthplace)', null, null, 0, 10);

        $data = [];
        foreach ( $records as $rows ) 
        {
            $data[] = ['value' => $rows->employee_birthplace];
        }

        echo json_encode( $data );

    }

    public function edulevel( $id = null )
    {
        if ( is_null( $id ) )
        {
            $q       = $_GET['q'];
            $records = $this->m_global->get_data_all( 'educational_level', NULL, ['edulevel_name LIKE' => "%$q%"], '*', null, null, 0, 5 );

            $data = [];
        
            for ( $i = 0; $i < count( $records ); $i++ ) 
            {
                $data[$i] = ['id' => $records[$i]->edulevel_id, 'name' => $records[$i]->edulevel_name];
            }

            echo json_encode( ['item' => $data] );
        }
        else
        {
            $records    = $this->m_global->get_data_all( 'educational_level', null, ['edulevel_id' => $id]);
            $tmp        = [
                            ['id' => $id, 'name' => $records[0]->edulevel_name]
                        ];

            echo json_encode( $tmp );
        }
    }

    public function specialist( $id = null )
    {
        if ( is_null( $id ) )
        {
            $q       = $_GET['q'];
            $pos_id  = $_GET['id'];
            $records = $this->m_global->get_data_all( 'study', NULL, ['study_name LIKE' => "%$q%", 'study_position_id' => $pos_id], '*', null, null, 0, 5 );

            $data = [];
        
            for ( $i = 0; $i < count( $records ); $i++ ) 
            {
                $data[$i] = ['id' => $records[$i]->study_id, 'name' => $records[$i]->study_name];
            }

            echo json_encode( ['item' => $data] );
        }
        else
        {
            $records    = $this->m_global->get_data_all( 'study', null, ['study_id' => $id]);
            $tmp        = [
                            ['id' => $id, 'name' => $records[0]->study_name]
                        ];

            echo json_encode( $tmp );
        }
    }

    public function post_category( $id = null )
    {
        if ( is_null( $id ) )
        {
            $q       = $_GET['q'];
            $records = $this->m_global->get_data_all( 'post_category', NULL, ['category_name LIKE' => "%$q%"], '*', null, null, 0, 10 );

            $data = [];
        
            for ( $i = 0; $i < count( $records ); $i++ ) 
            {
                $data[$i] = ['id' => $records[$i]->category_id, 'name' => $records[$i]->category_name];
            }

            echo json_encode( ['item' => $data] );
        }
        else
        {
            $records    = $this->m_global->get_data_all( 'post_category', null, ['category_id' => $id]);
            $tmp        = [
                            ['id' => $id, 'name' => $records[0]->category_name]
                        ];

            echo json_encode( $tmp );
        }
    }

    public function pasien( $id = null )
    {
        if ( is_null( $id ) )
        {
            $q          = $_GET['q'];
            $member   = $this->m_global->get_data_all( 'member', null, ['member_name LIKE' => '%'.$q.'%', 'member_status' => '1'], '*', null, null, 0, 10 );
            
            $data = [];
            for ( $i = 0; $i < count( $member ); $i++ ) 
            {
                $data[$i] = ['id' => $member[$i]->member_id, 'name' => $member[$i]->member_name];
            }

            echo json_encode( ['item' => $data] );
        }
        else
        {
            $member    = $this->m_global->get_data_all( 'member', null, ['member_id' => $id]);
            $tmp        = [
                            ['id' => $id, 'name' => $member[0]->member_name]
                        ];

            echo json_encode( $tmp );
        }
    }

    public function session($emp_id, $day_select){
        $day = date('D', strtotime($day_select));
        // echo $emp_id; exit;

        $join = [
                    ['table' => 'employee', 'on' => 'schedule_employee_id=employee_id'],
                    ['table' => 'employee_position', 'on' => 'empost_employee_id=employee_id'],
                    ['table' => 'study', 'on' => 'empost_study_id=study_id'],
                ];

        $dayarr = ['Mon' => 'senin','Tue' => 'selasa','Wed' => 'rabu','Thu' => 'kamis','Fri' => 'jumat','Sat' => 'sabtu','Sun' => 'minggu'];

        $data = $this->m_global->get_data_all('schedule',$join,['schedule_employee_id' => $emp_id])[0];

        $session = json_decode($data->schedule_datetime, true);
        $session = $session[$dayarr[$day]];
        $val = [];
        for ($i=0; $i < count($session); $i++) { 
            if(!empty($session)){
                $val[$i] = ['id' => str_replace(' ', '_', $session[$i]['session']), 'name' => $session[$i]['session']];
            }else{
                $val[$i] = ['id' => '--', 'name' => '--'];
            }
        }
        echo json_encode(['item' => $val]);
    }

    public function session_blast($emp_id){
        $session = $this->m_global->get_data_all('appointment', null,['appointment_employee_id' => $emp_id, 'appointment_status' => '1'], '*', null, null, 0, null, 'appointment_session');
        $data = [];
        for ($i=0; $i < count($session); $i++) {
            $val_id =  $session[$i]->appointment_day.'x'.str_replace(' ', '_', $session[$i]->appointment_session);
            $data[$i] = ['id' =>  $val_id, 'name' => tgl_format_day($session[$i]->appointment_day).', '. tgl_format($session[$i]->appointment_day).' - '.$session[$i]->appointment_session];
        }
        // echo "<pre>";
        // print_r($data);
        echo json_encode( ['item' => $data] );
    }

    public function queue($session, $day, $emp_id){
        $session = str_replace('_', ' ', $session);

        $queue = $this->m_global->count_data_all('appointment', null,['appointment_employee_id' => $emp_id, 'appointment_day' => $day, 'appointment_session' => $session, 'appointment_queue_status' => '1']);
        $queue = $queue + 1;
        if(strlen($queue) == '1'){
            $queue = 'D00'.$queue;
        }else if(strlen($queue) == '2'){
            $queue = 'D0'.$queue;
        }else{
            $queue = 'D'.$queue;
        }

        echo $queue;
    }

}

/* End of file config.php */
/* Location: ./application/modules/config/controllers/config.php */