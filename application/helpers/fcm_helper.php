<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Firebase\JWT\JWT;

function generate_jwt() {
    $serviceAccount = json_decode(file_get_contents(FCPATH . 'fcm-service-account.json'), true);

    $now = time();
    $token = [
        'iss' => $serviceAccount['client_email'],
        'scope' => 'https://www.googleapis.com/auth/firebase.messaging',
        'aud' => 'https://oauth2.googleapis.com/token',
        'iat' => $now,
        'exp' => $now + 3600,
    ];

    return JWT::encode($token, $serviceAccount['private_key'], 'RS256');
}

function get_access_token($jwt) {
    $url = 'https://oauth2.googleapis.com/token';

    $postFields = http_build_query([
        'grant_type' => 'urn:ietf:params:oauth:grant-type:jwt-bearer',
        'assertion' => $jwt,
    ]);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/x-www-form-urlencoded']);

    $response = curl_exec($ch);
    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    if ($httpCode != 200) {
        throw new Exception('Failed to obtain access token. HTTP Code: ' . $httpCode . '. Response: ' . $response);
    }

    $responseData = json_decode($response, true);
    return $responseData['access_token'];
}

function gcm_sender($fields, $access_token){
    $payload['message'] = $fields;
    $payload['message']['apns']['payload']['aps']['sound'] = 'default';
    if(isset($fields['data']['message']) && isset($fields['data']['title'])){
        $alert = [ 'body' => $fields['data']['message'], 'title' => $fields['data']['title']. (isset($fields['data']['info']) ? ' ('.$fields['data']['info'].')': '')];
    }else{
        $alert = [ 'body' => '...', 'title' => '...'];
    }
    $payload['message']['apns']['payload']['aps']['alert'] = $alert;
    $headers = ['Authorization: Bearer ' . $access_token,'Content-Type: application/json'];
    $ch = curl_init();
    curl_setopt( $ch, CURLOPT_URL, GCM_URL );
    curl_setopt( $ch, CURLOPT_POST, true );
    curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
    curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $payload ) );
    curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false);
    $result = curl_exec($ch);
    curl_close($ch);

    return $result;
}

function send_notification($receiver, $title, $msg,$type,$payload,$print_response = true){
    $CI =& get_instance();

    if($receiver != 'all') {
        $receiver = 'user'.$receiver;
    }

    if($receiver == 'all') {
        $receiver = 'user';
    }

    $fields = [
        'data'  => [    
            'title' => $title,
            'message' => $msg,
            'type' => $type,
            'target_user' => $receiver
        ]
    ];

    $payload       = json_decode($payload, true);
    if(count($payload) > 0){
        $fields['data']     = array_merge($fields['data'], $payload);
    }

    $jwt            = generate_jwt();
    $access_token   = get_access_token($jwt);

    $fields['topic'] = ENVIRONMENT.'_'.$receiver;
    $result_fcm      = gcm_sender($fields, $access_token);

    $result = [
        'response' => json_decode($result_fcm)
    ];
    $result['data'] = $fields['data'];
    $result['topic'] = $fields['topic'];
    if($print_response){
        echo json_encode($result);
    }
}

?>
