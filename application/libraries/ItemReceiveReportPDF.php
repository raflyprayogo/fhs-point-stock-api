<?php 

require_once APPPATH.'third_party/fpdf/fpdf-1.8.php'; 
class ItemReceiveReportPDF extends FPDF {
	function header_report(){

      $this->AddFont('MS Sans Serif','','ms-sans-serif-6.php'); 
      $this->AddFont('MS Sans Serif','B','ms-sans-serif-6.php'); 
      
      $this->SetY(2);
      $this->SetX(3);
      $this->SetFont('MS Sans Serif','',18);
      $this->Cell(50,10,"PENERIMAAN BARANG",0,1);

      $this->SetY(2);
      $this->SetX(-43);
      $this->SetFont('Arial','I',12);
      $this->Cell(50,10,"SUKSES JAYA",0,1);

      $this->SetLineWidth(1);
      $this->Line(4,11,204,11);
    }
    
    function body($data, $username){
      $this->SetY(11);
      $this->SetX(3);
      $this->SetFont('MS Sans Serif','B',10);
      $this->Cell(50,10,"No Bukti        : ".$data['order_code'],0,1);

      $this->SetY(17);
      $this->SetX(3);
      $this->SetFont('MS Sans Serif','B',10);
      $this->Cell(50,10,"No Request  : ".$data['parent_code'],0,1);

      $this->SetY(11);
      $this->SetX(130);
      $this->SetFont('MS Sans Serif','B',10);
      $this->Cell(50,10,"Warehouse Request     : ".$data['wh_request']['title'],0,1);

      $this->SetY(11);
      $this->SetX(70);
      $this->SetFont('MS Sans Serif','B',10);
      $this->Cell(50,10,"Tanggal     : ".revertDate($data['date']),0,1);

      $this->SetY(17);
      $this->SetX(130);
      $this->SetFont('MS Sans Serif','B',10);
      $this->Cell(50,10,"Warehouse Receive     : ".$data['wh_receive']['title'],0,1);

      $this->SetY(24);
      $this->SetX(3);
      $this->SetFont('MS Sans Serif','B',10);
      $this->Cell(50,10,"Keterangan   : ".$data['description'],0,1);

      $this->SetLineWidth(.3);
      $this->Line(4,33,204,33);
      
      $this->SetY(33);
      $this->SetX(10);
      $this->SetFont('MS Sans Serif','',10);
      $this->Cell(5,9,"No",0,0,"C");
      $this->Cell(30,9,"Kode Barang",0,0,"C");
      $this->Cell(80,9,"Nama Barang",0,0,"C");
      $this->Cell(25,9,"Jumlah",0,0,"C");
      $this->Cell(25,9,"Harga",0,0,"C");
      $this->Cell(25,9,"Total",0,1,"C");

      $this->Line(4,42,204,42);

      // for ($i=2; $i < 11; $i++) { 
      //   $data['products'][$i] = [
      //     'product_code' => '893475983453',
      //     'product_name' => 'PRIMERA - TEPUNG ROTI PUTIH LOKAL 500gr',
      //     'product_qty_input' => '11',
      //     'product_uom_input' => '1',
      //     'product_note' => '',
      //     'product_hpp' => 10000,
      //     'product_hpp_total' => 110000,
      //     'product_prices' => [
      //       [
      //         'uom' => 'CRT'
      //       ]
      //     ]
      //   ];
      // }
      
      $n = 1;
      $init_pos = 43;
      $total_price = 0;
      foreach($data['products'] as $rows){
        $uom_index = ((Int) $rows['product_uom_input']) - 1;
      	$this->SetFont('Arial','',9);

        $this->SetY($init_pos);
        $this->SetX(10);
        $this->MultiCell(7,5,$n,0,"L");

        $this->SetY($init_pos);
        $this->SetX(17);
        $this->MultiCell(30,5,$rows['product_code'],0,"L");

        $this->SetY($init_pos);
        $this->SetX(48);
        $this->MultiCell(80,5,$rows['product_name'],0,"L");

        $this->SetY($init_pos);
        $this->SetX(-83);
        $this->MultiCell(8,5,$rows['product_qty_input'].'.',0,"C");

        $this->SetY($init_pos);
        $this->SetX(-77);
        $this->MultiCell(17,5,$rows['product_prices'][$uom_index]['uom'],0,"C");

        $this->SetY($init_pos);
        $this->SetX(-61);
        $this->MultiCell(25,5,"0",0,"R");

        $this->SetY($init_pos);
        $this->SetX(-35);
        $this->MultiCell(25,5,"0",0,"R");

        $n++;
        $init_pos = $init_pos + (strlen($rows['product_name']) > 40 ? 11 : 6);
        if($init_pos > 140) {
          $this->AddPage();
          $init_pos = 5;
        }
      }

      if($init_pos > 110) {
        $this->AddPage();
      }

      $this->SetY(110);
      $this->SetX(-60);
      $this->SetFont('MS Sans Serif','B',10);
      $this->Cell(50,10,"Sub Total       ". number_format($total_price),0,1, "R");

      $this->SetLineWidth(1);
      $this->Line(4,119,204,119);

      $this->SetY(117);
      $this->SetX(23);
      $this->SetFont('MS Sans Serif','B',10);
      $this->Cell(50,10,"Mengetahui,",0,1);

      $this->SetY(117);
      $this->SetX(-33);
      $this->SetFont('MS Sans Serif','B',10);
      $this->Cell(50,10,"Packer",0,1);

      $this->SetY(135);
      $this->SetX(3);
      $this->SetFont('MS Sans Serif','',10);
      $this->Cell(50,10,date('d-m-Y'),0,1);

      $this->SetY(135);
      $this->SetX(50);
      $this->SetFont('MS Sans Serif','B',10);
      $this->Cell(50,10,'Dibuat   : '.$username,0,1);
      
    }
}




?>
