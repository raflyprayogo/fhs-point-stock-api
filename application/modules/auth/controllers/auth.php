<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller {

	function __construct() {
        parent::__construct();
        $this->headers          = $this->input->request_headers();
        $this->api_version      = $this->headers['Api-Version'] ?: '1';
        date_default_timezone_set('UTC');
        header('Content-Type: application/json');
    }

    public function index(){   
        echo "FH Auth Rest API";
        // echo md5_mod('apa', 'raflyprayogo');
    }

    function login_point(){
        if($this->api_version == '1'){
            $username       = $this->input->post('username');
            $password       = $this->input->post('password');
            $check_data     = $this->m_global->get_data_all('tusers_point', null, ['user_name' => $username, 'is_delete' => '0']);
            if(!empty($check_data)){
                if($check_data[0]->user_pass == $password){
                    if($check_data[0]->user_type == '0'){
                        $uid        = $check_data[0]->person_no;
                        $get_person = $this->m_global->get_data_all('tperson', null, ['person_no' => $uid], 'person_name, person_code');
                        $name       = $get_person[0]->person_name;
                        $code       = $get_person[0]->person_code;
                        $is_staff   = false;

                        $token       = $this->generate_file_token($uid, trim($check_data[0]->user_name));
                        $this->save_fcm_token($uid, '1');

                        $res_data = (Object) [
                            'id' => $uid,
                            'name' => $name,
                            'code' => $code,
                            'username' => $check_data[0]->user_name,
                            'is_staff' => $is_staff,
                            'token' => $token
                        ];

                        echo response_builder(true, 200, $res_data);
                    }else{
                        echo response_builder(false, 403, null, 'Staff hanya boleh masuk di Back-End');
                    }
                }else{
                    echo response_builder(false, 412, null, 'Kata sandi salah');
                }
            }else{
                echo response_builder(false, 403, null, 'Customer tidak ditemukan');
            }
        }else if($this->api_version == '2'){
            $username       = $this->input->post('username');
            $password       = $this->input->post('password');
            $user_data      = $this->m_global->get_data_all('tusers_point', null, ['user_name' => $username, 'is_delete' => '0']);

            if (!empty($user_data)) {
                $this->login_point_validate($user_data[0], $password);
            } else {
                $user_by_phone      = $this->m_global->get_data_all('tperson', null, ['person_hp' => $username, 'is_delete' => '0']);
                if (!empty($user_by_phone)) {
                    $user_data      = $this->m_global->get_data_all('tusers_point', null, ['person_no' => $user_by_phone[0]->person_no, 'is_delete' => '0']);
                    if(!empty($user_data)){
                        $this->login_point_validate($user_data[0], $password);
                    }else{
                        echo response_builder(false, 403, null, 'Customer tidak ditemukan');
                    }
                } else {
                    echo response_builder(false, 403, null, 'Customer tidak ditemukan');
                }
            }
        }else{
            echo response_builder(false, 900);
        }
        
    }

    function login_point_validate($user_data, $password) {
        if($user_data->user_pass == $password){
            if($user_data->user_type == '0'){
                $uid        = $user_data->person_no;
                $get_person = $this->m_global->get_data_all('tperson', null, ['person_no' => $uid], 'person_name, person_code');
                $name       = $get_person[0]->person_name;
                $code       = $get_person[0]->person_code;
                $is_staff   = false;

                $token       = $this->generate_file_token($uid, trim($user_data->user_name));
                $this->save_fcm_token($uid, '1');

                $res_data = (Object) [
                    'id' => $uid,
                    'name' => $name,
                    'code' => $code,
                    'username' => $user_data->user_name,
                    'is_staff' => $is_staff,
                    'token' => $token
                ];

                echo response_builder(true, 200, $res_data);
            }else{
                echo response_builder(false, 403, null, 'Staff hanya boleh masuk di Back-End');
            }
        }else{
            echo response_builder(false, 412, null, 'Kata sandi salah');
        }
    }

    function login_stock(){
        if($this->api_version == '1'){
            $username       = $this->input->post('username');
            $password       = $this->input->post('password');
            $check_data     = $this->m_global->get_data_all('tusers', null, ['user_name' => $username, 'is_delete' => '0']);
            if(!empty($check_data)){
                if($check_data[0]->user_pass == $password && (strpos($check_data[0]->user_right, 'ASO') !== false || $check_data[0]->is_Super == 1)){
                    $uid        = $check_data[0]->User_id;
                    $uname      = $check_data[0]->user_name;

                    $token      = $this->generate_file_token($uid, trim($uname));
                    $this->save_fcm_token($uid, '1');

                    $res_data = (Object) [
                        'id' => $uid,
                        'gud_no' => $check_data[0]->gud_no,
                        'cat_gud_no' => $check_data[0]->cat_gud_no,
                        'right' => $check_data[0]->user_right . ($check_data[0]->is_Super == 1 ? ' ASD' : ''),
                        'username' => $uname,
                        'token' => $token
                    ];

                    echo response_builder(true, 200, $res_data);
                }else{
                    echo response_builder(false, 412, null, 'password wrong');
                }
            }else{
                echo response_builder(false, 403, null, 'user not found');
            }
        } else if($this->api_version == '2'){
            $username       = $this->input->post('username');
            $password       = $this->input->post('password');
            $join           = [['table' => 'tgudang tgd', 'on' => 'tu.gud_no=tgd.gud_no']];
            $check_data     = $this->m_global->get_data_all('tusers tu', $join, ['tu.user_name' => $username, 'tu.is_delete' => '0']);
            if(!empty($check_data)){
                if($check_data[0]->user_pass == $password && (strpos($check_data[0]->user_right, 'ASO') !== false || $check_data[0]->is_Super == 1)){
                    $uid        = $check_data[0]->User_id;
                    $uname      = $check_data[0]->user_name;

                    $token      = $this->generate_file_token($uid, trim($uname));
                    $this->save_fcm_token($uid, '1');

                    $res_data = (Object) [
                        'id' => $uid,
                        'warehouse_id' => $check_data[0]->gud_no,
                        'warehouse_name' => $check_data[0]->gud_code .' '. $check_data[0]->gud_name,
                        'cat_gud_no' => $check_data[0]->cat_gud_no,
                        'right' => $check_data[0]->user_right,
                        'username' => $uname,
                        'is_super' => $check_data[0]->is_Super == '1' ? true : false,
                        'token' => $token
                    ];

                    echo response_builder(true, 200, $res_data);
                }else{
                    echo response_builder(false, 412, null, 'password wrong');
                }
            }else{
                echo response_builder(false, 403, null, 'user not found');
            }
        }else{
            echo response_builder(false, 900);
        }
        
    }

    function generate_file_token($id,$email){
        $fcm_token              = $this->headers['Fcm-Token'];
        $model_name             = $this->headers['Model-Name'];
        $salt                   = $fcm_token.'%'.$model_name;
        $token                  = md5_mod($salt, $id.'%'.$email);
        $data['Api-Token']      = $token; 
        $data['Fcm-Token']      = $fcm_token;
        $data['User-Id']        = $id; 
        $data['Email']          = $email; 
        $data['Created']        = date('Y-m-d H:i:s'); 

        file_put_contents(URL_TOKENS.$token.'.json',json_encode($data));
        return $token;
    }

    function save_fcm_token($user_id, $type){
        $token                  = $this->headers['Fcm-Token'];
        $model_name             = $this->headers['Model-Name'];
        $app_version            = $this->headers['App-Version'];
        $os_version             = $this->headers['Os-Version'];
        $platform               = $this->headers['Platform'];
        $salt                   = $model_name.'-'.$app_version.'-'.$os_version.'-'.$platform;
        $device_uuid            = md5_mod($user_id, $salt);

        $data['device_fcm_token']  = $token;   
        $data['device_status']     = '1';   

        $check_token = $this->m_global->get_data_all('devices', null, ['device_user_id' => $user_id, 'device_user_type' => $type,'device_uuid' => $device_uuid]);
        if(count($check_token) == 0){
            $data['device_user_id']        = $user_id;
            $data['device_user_type']      = $type;
            $data['device_platform']       = $platform;
            $data['device_uuid']           = $device_uuid;
            $data['device_app_version']    = $app_version;
            $data['device_os']             = $os_version;
            $data['device_model']          = $model_name;
            $data['device_createddate']    = date('Y-m-d H:i:s');
            $result = $this->m_global->insert('devices', $data);
        }else{
            $result = $this->m_global->update('devices', $data, ['device_user_id' => $user_id, 'device_user_type' => $type, 'device_uuid' => $device_uuid]);
        }
    }

    // function disable_fcm_token(){
    //     if($this->api_version == '1'){
    //         $token      = $this->headers['Fcm-Token'];
    //         $buyer_id    = $this->headers['User-Id'];

    //         $data['fcm_status']   = '0';

    //         $result     = $this->m_global->update('fcm', $data, ['fcm_buyer_id' => $buyer_id, 'fcm_token' => $token]);
    //         if($result){
    //             echo response_builder(true, 201);
    //         }else{
    //             echo response_builder(false, 406, null, 'failed update data');
    //         }
    //     }else{
    //         echo response_builder(false, 900);
    //     }
    // }

    function register(){
        if($this->api_version == '1'){
            $full_name      = $this->input->post('full_name');
            $username       = $this->input->post('username');
            $password       = $this->input->post('password');
            $email          = $this->input->post('email');
            $address        = $this->input->post('address');
            $province       = $this->input->post('province');
            $city           = $this->input->post('city');
            $district       = $this->input->post('district');
            $postal_code    = $this->input->post('postal_code');
            $phone          = $this->input->post('phone');

            $this->check_username($username);
            $this->check_email($email);
            $this->check_phone($phone);

            $data['user_fullname']      = $full_name;
            $data['user_username']      = $username;
            $data['user_password']      = $password;
            $data['user_email']         = $email;
            $data['user_address']       = $address.', '.$district;
            $data['user_phone']         = $phone;
            $data['user_city']          = $city;
            $data['user_province']      = $province;
            $data['user_postal_code']   = $postal_code;
            $data['user_createddate']   = date('Y-m-d H:i:s');

            $result = $this->m_global->insert('user_register/mobile', $data);
            if($result['status']){
                echo response_builder(true, 201);
            }else{
                echo response_builder(false, 406, null, 'failed create data');
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    function forgot_password(){
        if($this->api_version == '1'){
            $email          = $this->input->post('email');
            $check_data     = $this->m_global->get_data_all('tusers_point', null, ['user_email' => $email]);
            if(count($check_data) > 0){
                $new_password   = $this->random_password();
                $result         = $this->m_global->update('tusers_point', ['user_pass' => $new_password], ['person_no' => $check_data[0]->person_no]); 
                if($result){
                    $this->send_password_email($check_data[0]->person_no);
                    echo response_builder(true, 201);
                }else{
                    echo response_builder(false, 406, null, 'Gagal membuat kata sandi baru');
                }
            }else{
                echo response_builder(false, 403, null, 'Customer dengan email ini tidak ditemukan');
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    public function send_password_email($id){
        $data['records']    = $this->m_global->get_data_all('tusers_point tup', [['table' => 'tperson tp', 'on' => 'tp.person_no=tup.person_no']], ['tup.person_no' => $id])[0];
        $message            = $this->load->view('forgot_password_email', $data, TRUE);
        send_email_html('reportappsuksesjaya@gmail.com', $data['records']->user_email, 'Kata Sandi Baru Customer Sukses Jaya','Berikut detail & informasi kata sandi', $message);
    }

    function change_password(){
        if($this->api_version == '1'){
            $user_id        = $this->headers['User-Id'];
            $old_password   = $this->input->post('old_password');
            $new_password   = $this->input->post('new_password');

            $check_data     = $this->m_global->get_data_all('tusers_point', null, ['person_no' => $user_id, 'user_pass' => $old_password]);
            if(count($check_data) > 0){
                $result     = $this->m_global->update('tusers_point', ['user_pass' => $new_password], ['person_no' => $user_id]); 
                if($result){
                    echo response_builder(true, 201);
                }else{
                    echo response_builder(false, 406, null, 'Gagal mengubah kata sandi');
                }
            }else{
                echo response_builder(false, 403, null, 'Kata sandi lama salah');
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    private function random_password() {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array();
        $alphaLength = strlen($alphabet) - 1;
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass);
    }

    function province(){
        if($this->api_version == '1'){
            $data = $this->m_global->get_data_all('ec_provinces/mobile', null, null, '*', null, ['prov_name', 'ASC']);
            $res_data = [];
            foreach($data as $value){
                $res_data[] = ['id' => $value->prov_id, 'title' => $value->prov_name];
            }
            echo response_builder(true, 200, $res_data);
        }else{
            echo response_builder(false, 900);
        }
    }

    function city(){
        if($this->api_version == '1'){
            $data = $this->m_global->get_data_all('ec_cities/mobile', null, ['prov_id' => $this->input->post('province_id')], '*', null, ['city_name', 'ASC']);
            $res_data = [];
            foreach($data as $value){
                $res_data[] = ['id' => $value->city_id, 'title' => $value->city_name];
            }
            echo response_builder(true, 200, $res_data);
        }else{
            echo response_builder(false, 900);
        }
    }

    function district(){
        if($this->api_version == '1'){
            $data = $this->m_global->get_data_all('ec_districts/mobile', null, ['city_id' => $this->input->post('city_id')], '*', null, ['dis_name', 'ASC']);
            $res_data = [];
            foreach($data as $value){
                $res_data[] = ['id' => $value->dis_id, 'title' => $value->dis_name];
            }
            echo response_builder(true, 200, $res_data);
        }else{
            echo response_builder(false, 900);
        }
    }

    function postal_code(){
        if($this->api_version == '1'){
            $data = $this->m_global->get_data_all('ec_postalcode/mobile', null, ['city_id' => $this->input->post('city_id')]);
            $res_data = [];
            foreach($data as $value){
                $res_data[] = ['id' => $value->postal_id, 'title' => $value->postal_code];
            }
            echo response_builder(true, 200, $res_data);
        }else{
            echo response_builder(false, 900);
        }
    }

    function check_username($username){
        $main_check = $this->m_global->count_data_all('tusers_point', null, ['user_name' => $username]);
        $mobile_check = $this->m_global->count_data_all('user_register/mobile', null, ['user_username' => $username]);
        if($main_check > 0 || $mobile_check > 0){
            echo response_builder(false, 406, null, 'Username sudah digunakan');
            exit;
        }
    }

    function check_email($email){
        $main_check = $this->m_global->count_data_all('tusers_point', null, ['user_email' => $email]);
        $mobile_check = $this->m_global->count_data_all('user_register/mobile', null, ['user_email' => $email]);
        if($main_check > 0 || $mobile_check > 0){
            echo response_builder(false, 406, null, 'Email sudah digunakan');
            exit;
        }
    }

    function check_phone($phone){
        $mobile_check = $this->m_global->count_data_all('user_register/mobile', null, ['user_phone' => $phone]);
        if($mobile_check > 0){
            echo response_builder(false, 406, null, 'No HP sudah digunakan');
            exit;
        }
    }

    function gosend_webhooks(){
        if($this->headers['Api-Token'] != 'LHKK7o0DiOHl-GGkVmNmat9d0ZENyb6AXPm5-GlOkpIN5tnTN_650') {
            echo response_builder(false, 401);
            exit;
        }
        $input_data = json_decode(trim(file_get_contents('php://input')), true);
        $gosend_no      = $input_data['booking_id'];
        $status         = $input_data['status'];
        $driver_name    = $input_data['driver_name'];
        $driver_phone   = $input_data['driver_phone'];
        $cancel_reason  = $input_data['cancellation_reason'];
        $order_status   = "";

        if( $status == 'out_for_pickup'){
            $order_status = "pickup";
        }else if($status == 'picked' || $status == 'out_for_delivery'){
            $order_status = "delivery";
        }else if($status == 'cancelled' || $status == 'rejected'){
            $order_status = "canceled";
        }else if($status == 'no_driver'){
            $order_status = "payment_confirmation";
        }

        $data['order_status'] = $order_status;
        if($driver_name != ''){
            $data['order_driver_name']      = $driver_name;
            $data['order_driver_phone']     = $driver_phone;
        }
        if($cancel_reason != ""){
            $data['order_cancel_reason']    = $cancel_reason;
        }

        if($order_status != ""){
            $this->m_global->update('order/mobile', $data, ['order_gosend_no' => $gosend_no])[0];
            echo response_builder(true, 200, null, 'Success update');
            exit;
        }
        echo response_builder(true, 200, null, 'Nothing to update');
    }
}

/* End of file config.php */
/* banner: ./application/modules/config/controllers/config.php */