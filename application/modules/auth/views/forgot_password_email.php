<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>Pendaftaran Berhasil</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0 " />
    <meta name="format-detection" content="telephone=no" />
    <!--[if !mso]><!-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <!--<![endif]-->
    <style type="text/css">
      body {
      -webkit-text-size-adjust: 100% !important;
      -ms-text-size-adjust: 100% !important;
      -webkit-font-smoothing: antialiased !important;
      }
      img {
      border: 0 !important;
      outline: none !important;
      }
      p {
      Margin: 0px !important;
      Padding: 0px !important;
      }
      table {
      border-collapse: collapse;
      mso-table-lspace: 0px;
      mso-table-rspace: 0px;
      }
      td, a, span {
      border-collapse: collapse;
      mso-line-height-rule: exactly;
      }
      .ExternalClass * {
      line-height: 100%;
      }
      span.MsoHyperlink {
      mso-style-priority:99;
      color:inherit;}
      span.MsoHyperlinkFollowed {
      mso-style-priority:99;
      color:inherit;}
      @media only screen and (min-width:481px) and (max-width:599px) {
      table[class=em_main_table] {
      width: 100% !important;
      }
      table[class=em_wrapper] {
      width: 100% !important;
      }
      td[class=em_hide], br[class=em_hide] {
      display: none !important;
      }
      img[class=em_full_img] {
      width: 100% !important;
      height: auto !important;
      }
      td[class=em_align_cent] {
      text-align: center !important;
      }
      td[class=em_pad_top]{
      padding-top:20px !important;
      }
      td[class=em_aside]{
      padding-left:10px !important;
      padding-right:10px !important;
      }
      td[class=em_height]{
      height: 20px !important;
      }
      td[class=em_space]{
      width:10px !important;	
      }
      td[class=em_width55] {
      width:80px !important;
	  text-align:center !important;
      }
      td[class=em_width75] {
      width:100px !important;
      }
      td[class=em_font]{
      font-size:14px !important;	
      }
      td[class=em_font2] {
      text-align:center !important;
      }
      td[class=em_align_cent1] {
      text-align: center !important;
      padding-bottom: 10px !important;
      }
      }
      @media only screen and (max-width:480px) {
      table[class=em_main_table] {
      width: 100% !important;
      }
      table[class=em_wrapper] {
      width: 100% !important;
      }
      td[class=em_hide], br[class=em_hide], span[class=em_hide] {
      display: none !important;
      }
      img[class=em_full_img] {
      width: 100% !important;
      height: auto !important;
      }
      td[class=em_align_cent] {
      text-align: center !important;
      }
      td[class=em_pad_top]{
      padding-top:20px !important;
      }
      td[class=em_height]{
      height: 20px !important;
      }
      td[class=em_aside]{
      padding-left:10px !important;
      padding-right:10px !important;
      } 
      td[class=em_font]{
      font-size:14px !important;
      line-height:28px !important;
      }
      td[class=em_font1]{
      font-size:14px !important;
      line-height:18px !important;
      }
      td[class=em_font2]{
      font-size:14px !important;
      line-height:18px !important;
      text-align:center !important;
      }
      td[class=em_space]{
      width:10px !important;	
      }
      span[class=em_br]{
      display:block !important;
      }
      td[class=em_width55] {
      width:55px !important;
      font-size:15px !important;
      line-height:19px !important;
	  text-align:center !important;
      }
      td[class=em_width75] {
      width:75px !important;
      font-size:15px !important;
      line-height:19px !important;
      }
      td[class=em_align_cent1] {
      text-align: center !important;
      padding-bottom: 10px !important;
      }
      }
    </style>
  </head>
  <body style="margin:0px; padding:0px;" bgcolor="#ffffff">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
    <!-- === PRE HEADER SECTION=== -->  
      <tr>
        <td align="center" valign="top"  bgcolor="#30373b">
          <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="em_main_table" style="table-layout:fixed;">
            <tr>
              <td style="line-height:0px; font-size:0px;" width="600" class="em_hide" bgcolor="#30373b"><img src="https://www.sendwithus.com/assets/img/emailmonks/images/spacer.gif" height="1"  width="600" style="max-height:1px; min-height:1px; display:block; width:600px; min-width:600px;" border="0" alt="" /></td>
            </tr>
            <tr>
              <td valign="top">
                <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="em_wrapper">
                  <tr>
                    <td height="10" class="em_height" style="font-size:1px; line-height:1px;">&nbsp;</td>
                  </tr>
                  <tr>
                    <td valign="top">
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td valign="top">
                            <table width="150" border="0" cellspacing="0" cellpadding="0" align="right" class="em_wrapper">
                              <tr>
                                <td align="right" class="em_align_cent1" style="font-family:'Open Sans', Arial, sans-serif; font-size:12px; line-height:16px; color:#848789; text-decoration:underline;">
                                  <!-- <a href="#" target="_blank" style="text-decoration:underline; color:#848789;">View online</a> -->
                                </td>
                              </tr>
                            </table>
                            <table width="400" border="0" cellspacing="0" cellpadding="0" align="left" class="em_wrapper">
                              <tr>
                                <td align="left" class="em_align_cent" style="font-family:'Open Sans', Arial, sans-serif; font-size:12px; line-height:18px; color:#848789; text-decoration:none;">
                                  <!-- Snippet text here lorem ipsum is dummy text -->
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                  <tr>
                    <td height="10" class="em_height" style="font-size:1px; line-height:1px;">&nbsp;</td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <!-- === //PRE HEADER SECTION=== -->  
      <!-- BODY  -->
      <tr>
        <td align="center" valign="top"  bgcolor="#ffffff">
          <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="em_main_table" style="table-layout:fixed;">
            <!-- === LOGO SECTION === -->
            <tr>
              <td height="40" class="em_height">&nbsp;</td>
            </tr>
            <tr>
              <td align="center"><a href="#" target="_blank" style="text-decoration:none;"><img src="https://suksesjayamlg.com/Images/RootCategory/supermarket-sukses-jaya-malang.png" width="100" style="display:block;font-family: Arial, sans-serif; font-size:15px; line-height:18px; color:#30373b;  font-weight:bold;" border="0" alt="" /></a></td>
            </tr>
            <tr>
              <td height="10" class="em_height">&nbsp;</td>
            </tr>
            <tr>
              <td align="center" style="font-family:'Open Sans', Arial, sans-serif; font-size:20px; color:#30373b;"><b>Kata Sandi Baru</b></td>
            </tr>
            <tr>
              <td height="10" class="em_height">&nbsp;</td>
            </tr>
            <!-- === //LOGO SECTION === -->
            <!-- === NEVIGATION SECTION === -->
            <tr>
              <td height="14" style="font-size:1px; line-height:1px;">&nbsp;</td>
            </tr>
            <tr>
              <td height="1" bgcolor="#fed69c" style="font-size:0px; line-height:0px;"><img src="https://www.sendwithus.com/assets/img/emailmonks/images/spacer.gif" width="1" height="1" style="display:block;" border="0" alt="" /></td>
            </tr>
            <!-- === //NEVIGATION SECTION === -->
            <!-- THANK YOU SECTION -->
            <tr>
              <td valign="top" class="em_aside">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td height="14" style="font-size:1px; line-height:1px;">&nbsp;</td>
                  </tr>
                  <tr>
                    <td height="14" style="font-size:1px; line-height:1px;">&nbsp;</td>
                  </tr>
                  <tr>
                    <td align="center" style="font-family:'Open Sans', Arial, sans-serif; font-size:16px; color:#30373b;">Untuk <b><?php echo @$records->person_name?></b>,</td>
                  </tr>
                  <tr>
                    <td height="8" style="font-size:1px; line-height:1px;">&nbsp;</td>
                  </tr>
                  <tr>
                    <td align="center" style="font-family:'Open Sans', Arial, sans-serif; font-size:16px; color:#30373b;">Selamat! Kamu telah berhasil membuat kata sandi baru, gunakan kata sandi ini untuk masuk dan mengubah kata sandi-mu sendiri.</td>
                  </tr>
                  <tr>
                    <td height="25" class="em_height">&nbsp;</td>
                  </tr>
                  <tr>
                    <td height="1" bgcolor="#d6d7d8" style="font-size:0px;line-height:0px;"><img src="https://www.sendwithus.com/assets/img/emailmonks/images/spacer.gif" width="1" height="1" alt="" style="display:block;" border="0" /></td>
                  </tr>
                  <tr>
                    <td height="27" class="em_height">&nbsp;</td>
                  </tr>
                  <tr>
                    <td valign="top" align="center" style="font-family:'Open Sans', Arial, sans-serif; font-size:17px;line-height:22px; font-weight:bold; color:#30373b; text-transform:uppercase;">Customer Detail</td>
                  </tr>
                  <tr>
                    <td height="23" class="em_height">&nbsp;</td>
                  </tr>
                  <tr>
                    <td valign="top">
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td valign="top">
                            <table width="290" border="0" cellspacing="0" cellpadding="0" align="left" class="em_wrapper" bgcolor="#f6f7f8">
                              <tr>
                                <td align="center" valign="middle" class="em_font1" height="42" style="font-family:'Open Sans', Arial, sans-serif; font-size:17px; font-weight:bold; color:#30373b; text-transform:uppercase;">
                                  <span style="color:#feae39;">username :</span> <?php echo @$records->user_name?>
                                </td>
                              </tr>
                            </table>
                            <table width="290" border="0" cellspacing="0" cellpadding="0" align="right" class="em_wrapper">
                              <tr>
                                <td valign="top" class="em_pad_top">
                                  <table width="100%" border="0" cellspacing="0" cellpadding="0"  bgcolor="#f6f7f8" >
                                    <tr>
                                      <td align="center" valign="middle" class="em_font1" height="42" style="font-family:'Open Sans', Arial, sans-serif; font-size:17px; font-weight:bold; color:#30373b; text-transform:uppercase;">
                                        <span style="color:#feae39;">nama :</span> <?php echo @$records->person_name?>
                                      </td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                  <tr>
                    <td height="20" style="font-size:1px; line-height:1px;">&nbsp;</td>
                  </tr>
                  <tr>
                    <td valign="top">
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td valign="top">
                            <table width="290" border="0" cellspacing="0" cellpadding="0" align="left" class="em_wrapper" bgcolor="#f6f7f8">
                              <tr>
                                <td align="center" valign="middle" class="em_font1" height="42" style="font-family:'Open Sans', Arial, sans-serif; font-size:17px; font-weight:bold; color:#30373b;">
                                  <span style="color:#feae39;">kata sandi :</span> <?php echo @$records->user_pass?>
                                </td>
                              </tr>
                            </table>
                            <table width="290" border="0" cellspacing="0" cellpadding="0" align="right" class="em_wrapper">
                              <tr>
                                <td valign="top" class="em_pad_top">
                                  <table width="100%" border="0" cellspacing="0" cellpadding="0"  bgcolor="#f6f7f8" >
                                    <tr>
                                      <td align="center" valign="middle" class="em_font1" height="42" style="font-family:'Open Sans', Arial, sans-serif; font-size:17px; font-weight:bold; color:#30373b; text-transform:uppercase;">
                                        <span style="color:#feae39;">no hp :</span> <?php echo @$records->person_hp?>
                                      </td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                  <tr>
                    <td height="25" class="em_height">&nbsp;</td>
                  </tr>
                  <tr>
                    <td valign="top" align="center" style="font-family:'Open Sans', Arial, sans-serif; font-size:17px;line-height:22px; font-weight:bold; color:#30373b; text-transform:uppercase;">Butuh bantuan?</td>
                  </tr>
                  <tr>
                    <td height="12" style="font-size:1px; line-height:1px;">&nbsp;</td>
                  </tr>
                  <tr>
                    <td height="24" style="font-size:1px; line-height:1px;">&nbsp;</td>
                  </tr>
                  <tr>
                    <td valign="top" align="center">
                      <table width="210" border="0" cellspacing="0" cellpadding="0" align="center">
                        <tr>
                          <td valign="middle" align="center" height="45" bgcolor="#feae39" style="font-family:'Open Sans', Arial, sans-serif; font-size:17px; font-weight:bold; color:#ffffff; text-transform:uppercase;"><a href="mailto:reportappsuksesjaya@gmail.com" target="_blank" style="line-height:45px; display:block; color:#ffffff; text-decoration:none;">Hubungi kami</a></td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                  <tr>
                    <td height="31" class="em_height">&nbsp;</td>
                  </tr>
                </table>
              </td>
            </tr>
            <!-- //THANK YOU SECTION -->
          </table>
        </td>
      </tr>
      <!-- //BODY  -->
      <!-- === FOOTER SECTION === -->
      <tr>
        <td align="center" valign="top"  bgcolor="#30373b" class="em_aside">
          <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="em_main_table" style="table-layout:fixed;">
            <tr>
              <td height="35" class="em_height">&nbsp;</td>
            </tr>
            <tr>
              <td height="22" class="em_height">&nbsp;</td>
            </tr>
            <!-- <tr>
              <td align="center" style="font-family:'Open Sans', Arial, sans-serif; font-size:12px; line-height:18px; color:#848789; text-transform:uppercase;">
               <span style="text-decoration:underline;"><a href="#" target="_blank" style="text-decoration:underline; color:#848789;">PRIVACY STATEMENT</a></span> &nbsp;&nbsp;|&nbsp;&nbsp; <span style="text-decoration:underline;"><a href="#" target="_blank" style="text-decoration:underline; color:#848789;">TERMS OF SERVICE</a></span><span class="em_hide"> &nbsp;&nbsp;|&nbsp;&nbsp; </span><span class="em_br"></span><span style="text-decoration:underline;"><a href="#" target="_blank" style="text-decoration:underline; color:#848789;">RETURNS</a></span>
              </td>
            </tr>
            <tr>
              <td height="10" style="font-size:1px; line-height:1px;">&nbsp;</td>
            </tr> -->
            <tr>
              <td align="center" style="font-family:'Open Sans', Arial, sans-serif; font-size:12px; line-height:18px; color:#848789;text-transform:uppercase;">
                &copy;<?php echo date('Y')?> Sukses Jaya.
              </td>
            </tr>
            <tr>
              <td height="10" style="font-size:1px; line-height:1px;">&nbsp;</td>
            </tr>
            <tr>
              <td height="35" class="em_height">&nbsp;</td>
            </tr>
          </table>
        </td>
      </tr>
      <!-- === //FOOTER SECTION === -->
    </table>
    <div style="display:none; white-space:nowrap; font:20px courier; color:#ffffff; background-color:#ffffff;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div>
  </body>
</html>
