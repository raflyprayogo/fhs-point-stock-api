<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct() {
        parent::__construct();
        $this->headers          = $this->input->request_headers();
        $this->api_version      = $this->headers['Api-Version'];
        $this->master_db        = $this->load->database('default', true)->database;
        date_default_timezone_set('UTC');
        header('Content-Type: application/json');
    }

    private function select_from_prod(){
        return "(select merk_id, cat_id, group_id, prod_no, prod_code0, prod_name0, prod_uom as Satuan1, prod_sell_price as Harga1, if(prod_sell_price4<>0, prod_uom2, '') as Satuan2, if (prod_sell_price4<>0,prod_sell_price4,'') as Harga2 from ".$this->master_db.".tproduct where is_delete = 0)";
    }

    private function select_master_variant(){
        return "(select * from ".$this->master_db.".tgroup where is_delete = 0)";
    }

    public function index(){   
        echo "FH Home Rest API";
    }

    function dashboard(){
        if($this->api_version == '1'){
            $user_id  = @$this->headers['User-Id'] ?: '';
            $res_data = [];

            if($user_id != ''){
                $res_data['total_cart']     = (Int) $this->m_global->count_data_all('cart/mobile', null, ['cart_user_id' => $user_id]);
                $res_data['total_point']    = (Int) $this->m_global->get_data_all('tperson', null, ['person_no' => $user_id], 'total_point')[0]->total_point;
                $res_data['total_new_notification'] = 0;
            }else{
                $res_data['total_cart']             = 0;
                $res_data['total_point']            = 0;
                $res_data['total_new_notification'] = 0;
            }

            $data_config                        = $this->m_global->get_data_all('config/mobile')[0];
            $res_data['show_price']             = $data_config->config_show_price == '1' ? true : false;
            $res_data['show_postal_code']       = $data_config->config_show_postal_code == '1' ? true : false;
            $res_data['current_version_name']   = $data_config->config_android_current_version;
            $res_data['current_version_code']   = $data_config->config_android_current_build;
            $res_data['announcement']           = $data_config->config_announcement;

            $join = [
                ['table' => $this->select_from_prod().' tp', 'on' => 'tp.prod_no=product_id']
            ];

            $join_flashsale   = $join;
            $join_flashsale[] = ['table' => 'flashsale', 'on' => 'flashsale_product_id=product_id'];
            $data_flashsale   = $this->m_global->get_data_all('products/mobile', $join_flashsale, ['product_status' => '1', 'flashsale_status' => '1', 'flashsale_expired_date > ' => date('Y-m-d H:i:s')], '*', null, null, 0, 10);
            $res_data['flash_sale_products'] = $this->dataset_product($data_flashsale, true);

            $join_emmy   = $join;
            $join_emmy[] = ['table' => 'emmy_products', 'on' => 'prodemmy_product_id=product_id'];
            $data_emmy   = $this->m_global->get_data_all('products/mobile', $join_emmy, ['product_status' => '1', 'prodemmy_status' => '1'], '*', null, null, 0, 10);
            $res_data['emmy_products'] = $this->dataset_product($data_emmy);

            $data_newest   = $this->m_global->get_data_all('products/mobile', $join, ['product_status' => '1'], '*', null, ['product_createddate', 'DESC'], 0, 10);
            $res_data['new_products'] = $this->dataset_product($data_newest);

            $join_promo  = [['table' => $this->select_master_variant().' tv', 'on' => 'tv.group_id=promo_variant_id', 'tipe' => 'left']];
            $data_promo  = $this->m_global->get_data_all('promo/mobile', $join_promo, ['promo_status' => '1', 'promo_show_as_banner' => '1'], '*', null, ['promo_createddate', 'DESC']);
            foreach($data_promo as $value){
                $res_data['promo_banner'][] = [
                    'id' => $value->promo_id,
                    'thumbnail' => $value->promo_thumbnail,
                    'title' => $value->promo_title,
                    'payload_id' => $value->promo_product_id ?: "",
                    'payload2_id' => $value->promo_variant_id ?: "",
                    'payload2_title' => $value->nama ?: "",
                    'description' => $value->promo_description
                ];
            }
            echo response_builder(true, 200, $res_data);
        }else{
            echo response_builder(false, 900);
        }
    }

    private function dataset_product($data, $is_flashsale = false){
        $res = [];
        foreach($data as $value){
            $prices         = [];
            if($is_flashsale) {
                $price1 = (Int)$value->Harga1;
                $price2 = ($value->Harga2 != '' ? (Int)$value->Harga2 : 0 );
                $discounts = get_discount_value($value->product_id, $price1, $price2);

                $prices[]       = ['price' => $price1 - $discounts[0], 'uom' => $value->Satuan1, 'discount' => $discounts[0], 'price_before_discount' => $price1];
                if($price2 > 0){
                    $prices[]   = ['price' => $price2 - $discounts[1], 'uom' => $value->Satuan2, 'discount' => $discounts[1], 'price_before_discount' => $price2];
                }
            } else {
                $prices[]       = ['price' => $value->Harga1, 'uom' => $value->Satuan1];
                if($value->Harga2 != ''){
                    $prices[]   = ['price' => $value->Harga2, 'uom' => $value->Satuan2];
                }
            }
            $data_image = $this->m_global->get_data_all('product_thumbnails/mobile', null, ['prodthumb_product_id' => $value->product_id], 'prodthumb_url', null, null, 0, 1);
            if(count($data_image)){
                $image = $data_image[0]->prodthumb_url;
            }else{
                $image = "";
            }
            $res[] = [
                'product_id' => $value->product_id,
                'product_thumbnail' => $image,
                'product_title' => $value->prod_name0,
                'product_expired_date' => @$value->flashsale_expired_date ?: '',
                'product_prices' => $prices
            ];
            $prices = [];
        }
        return $res;
    }

    function boarding(){
        if($this->api_version == '1'){
            $res_data       = $this->m_global->get_data_all('tprofile', null, null, 'app_path1,app_path2,app_path3')[0];
            echo response_builder(true, 200, $res_data);
        }else{
            echo response_builder(false, 900);
        }
    }

    function promos(){
        if($this->api_version == '1'){
            $data_promo  = $this->m_global->get_data_all('promo/mobile', null, ['promo_status' => '1'], '*', null, ['promo_createddate', 'DESC']);
            foreach($data_promo as $value){
                $res_data[] = [
                    'id' => $value->promo_id,
                    'thumbnail' => $value->promo_thumbnail,
                    'title' => $value->promo_title,
                    'type' => 'promo',
                    'payload_id' => $value->promo_product_id ?: "",
                    'description' => $value->promo_description
                ];
            }
            echo response_builder(true, 200, $res_data);
        }else{
            echo response_builder(false, 900);
        }
    }

    function about(){
        if($this->api_version == '1'){
            $res_data['thumbnail'] = "https://suksesjayamlg.com/Images/WizardContent/Wiz-202202/215/tentang-sukses-jaya-supermarket-terlengkap-di-malang.jpg";
            $res_data['contacts'] = [
                ['title' => "Sukses Jaya Malang", 'subtitle' => "08113660150", 'description' => "sukses_jaya@hotmail.com"],
                ['title' => "Sales Roti", 'subtitle' => "081133330466", 'description' => ""],
                ['title' => "Sales Mesin Kopi", 'subtitle' => "081133399544", 'description' => ""]
            ];
            $res_data['about'] = [
                [
                    "thumbnail" => "https://suksesjayamlg.com/Images/WizardContent/Wiz-202202/232/sukses-jaya-malang-supermarket-terlengkap.jpg",
                    "title" => "Siapa Sukses Jaya?",
                    "description" => "Sukses Jaya adalah Supermarket yang menjual berbagai kebutuhan yang berlokasi di Jl. Bandahara no.7, Malang. Sukses Jaya telah lama menjadi supplier untuk berbagai kebutuhan, seperti supplier bahan makanan import, supplier perlengkapan dan peralatan dapur, hingga supplier mesin kopi. Mutu dan kualitas barang yang dijual serta pelayanan terbaik untuk semua pelanggannya adalah hal-hal yang menjadi prioritas utama supermarket ini. Tidak heran jika Sukses Jaya ini menjadi pilihan berbagai pengusaha di bidang kuliner."
                ],
                [
                    "thumbnail" => "",
                    "title" => "Mengapa Sukses Jaya?",
                    "description" => "Sukses Jaya selalu memberikan pelayanan yang terbaik untuk semua pelanggannya tanpa terkecuali. Semua barang dan produk yang dijual oleh Sukses Jaya adalah produk yang memiliki kualitas terbaik, terjangkau, dan higienis (untuk makanan). Selain itu Sukses Jaya juga telah dipercaya untuk menjadi supplier berbagai kebutuhan mulai dari bahan makanan, peralatan dapur, mesin kopi, dan perlengkapan lainnya bagi berbagai pelaku bisnis."
                ]
            ];
            echo response_builder(true, 200, $res_data);
        }else{
            echo response_builder(false, 900);
        }
    }

    function track_promo_tap(){
        if($this->api_version == '1'){
            $data['promotap_promo_id']      = $this->input->post('id');
            $data['promotap_user_id']       = @$this->headers['User-Id'] ?: '-';
            $result = $this->m_global->insert('promo_tap_log/mobile', $data);
            if($result['status']){
                echo response_builder(true, 201);
            }else{
                echo response_builder(false, 406, null, 'failed insert data');
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    function track_adsnotif_tap(){
        if($this->api_version == '1'){
            $data['adsnotiftap_adsnotif_id']    = $this->input->post('id');
            $data['adsnotiftap_user_id']        = @$this->headers['User-Id'] ?: '-';
            $result = $this->m_global->insert('adsnotif_tap_log/mobile', $data);
            if($result['status']){
                echo response_builder(true, 201);
            }else{
                echo response_builder(false, 406, null, 'failed insert data');
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    function cashier_scan(){
        if($this->api_version == '1'){
            $user_id            = @$this->headers['User-Id'] ?: '';
            $cashier_code       = $this->input->post('cashier_code');
            $data['person_no']  = $user_id;
            $result = $this->m_global->update('tkasir/bandahara', $data, ['kasir_no' => $cashier_code]);
            if($result){
                echo response_builder(true, 201, null, 'Data anda berhasil terkirim ke kasir');
            }else{
                echo response_builder(false, 406, null, 'Kode kasir tidak ditemukan');
            }
        }else{
            echo response_builder(false, 900);
        }
    }
}

/* End of file config.php */
/* banner: ./application/modules/config/controllers/config.php */