<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Point extends CI_Controller {

	function __construct() {
        parent::__construct();
        $this->headers          = $this->input->request_headers();
        $this->api_version      = $this->headers['Api-Version'];
        // date_default_timezone_set('UTC');
        header('Content-Type: application/json');
        acc_token();
    }

    public function index(){   
        echo "FH Point Rest API";
    }

    function history(){
        if($this->api_version == '1'){
            $person_no     = $this->input->post('person_no');
            if($person_no == ''){
                $person_no      = $this->headers['User-Id'];
            }
            $date_start     = $this->input->post('date_start');
            $date_end       = $this->input->post('date_end');

            $select         = 'tt.id tp_id, tt.person_no, tt.kredit, tt.debet, tt.tgl date, tp.total_point, tt.out_no, tt.in_no';
            $join           = [
                                ['table' => 'tperson tp', 'on' => 'tp.person_no=tt.person_no']
                            ];

            $where_e        = null;
            if($date_start != '' && $date_end != ''){
                $where_e    = "tt.tgl BETWEEN '".$date_start." 00:00:00' AND '".$date_end." 00:00:00'";
            }

            $res_data       = $this->m_global->get_data_all('ttrans_point tt', $join, ['tt.person_no' => $person_no], $select, $where_e, ['tt.tgl', 'DESC']);
            if(count($res_data) > 0){
                echo response_builder(true, 200, $res_data);
            }else{
                echo response_builder(true, 200, []);
            }
        }else if($this->api_version == '2'){
            $person_no      = $this->headers['User-Id'];

            $select         = 'tt.id tp_id, tt.person_no, tt.kredit, tt.debet, tt.tgl date, tp.total_point, tt.out_no, tt.in_no';
            $join           = [
                                ['table' => 'tperson tp', 'on' => 'tp.person_no=tt.person_no']
                            ];

            $date_end       = date("Y-m-d");
            $date_start     = date("Y-m-d", strtotime("-7 days"));
            $where_e        = "tt.tgl BETWEEN '".$date_start." 00:00:00' AND '".$date_end." 00:00:00'";

            $data_history       = $this->m_global->get_data_all('ttrans_point tt', $join, ['tt.person_no' => $person_no], $select, null, ['tt.tgl', 'DESC']);
            $data_last_week     = $this->m_global->get_data_all('ttrans_point tt', $join, ['tt.person_no' => $person_no], $select, $where_e, ['tt.tgl', 'DESC']);

            $res_data['total']      = (Int) $this->m_global->get_data_all('tperson', null, ['person_no' => $person_no], 'total_point')[0]->total_point;
            $res_data['last_week']  = $data_last_week;
            $res_data['history']    = $data_history;

            for ($i=0; $i < count($res_data['history']); $i++) { 
                $res_data['history'][$i]->kredit = (String)((Int) $res_data['history'][$i]->kredit);
                $res_data['history'][$i]->debet  = (String)((Int) $res_data['history'][$i]->debet);
            }

            echo response_builder(true, 200, $res_data);
        }else{
            echo response_builder(false, 900);
        }
    }

    function total_point(){
        if($this->api_version == '1'){
            $person_no      = $this->headers['User-Id'];
            $select         = 'total_point';
            $data           = $this->m_global->get_data_all('tperson', null, ['person_no' => $person_no], $select);
            if(count($data) > 0){
                $data[0]->total_point = round($data[0]->total_point);
                echo response_builder(true, 200, $data[0]);
            }else{
                echo response_builder(false, 412);
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    function person_with_point(){
        if($this->api_version == '1'){
            $select         = 'tt.person_no, tp.person_name';
            $join           = [
                                ['table' => 'tperson tp', 'on' => 'tp.person_no=tt.person_no']
                            ];
            $res_data       = $this->m_global->get_data_all('ttrans_point tt', $join, null, $select, null, null, 0, null, ['tp.person_no']);
            echo response_builder(true, 200, $res_data);
        }else if($this->api_version == '2'){
            $search         = $this->input->post('search');
            $select         = 'tt.person_no, tp.person_name';
            $join           = [
                                ['table' => 'tperson tp', 'on' => 'tp.person_no=tt.person_no']
                            ];
            $where          = "tp.person_name LIKE '%".$search."%'";
            $res_data       = $this->m_global->get_data_all('ttrans_point tt', $join, null, $select, ($search != '' ? $where : null), null, 0, null, ['tp.person_no']);
            echo response_builder(true, 200, $res_data);
        }else{
            echo response_builder(false, 900);
        }
    }

    function boarding(){
        if($this->api_version == '1'){
            $res_data       = $this->m_global->get_data_all('tprofile', null, null, 'app_path1,app_path2,app_path3')[0];
            echo response_builder(true, 200, $res_data);
        }else{
            echo response_builder(false, 900);
        }
    }

    function update_ads(){
        if($this->api_version == '1'){
            $column_name        = $this->input->post('column_name');
            $url                = $this->input->post('url');
            $data[$column_name] = $url;
            $result   = $this->m_global->update('tprofile', $data, ['pro_Id' => '1']);
            if($result){
                echo response_builder(true, 201);
            }else{
                echo response_builder(false, 406, null, 'failed update data');
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    function send_notification(){
        $post = $this->input->post();
        $result = send_notification($post['receiver'], $post['title'], $post['message'], $post['type'], $post['payload'], false);
        if($result){
            echo response_builder(true, 201);
        }else{
            echo response_builder(false, 406, null, 'failed send notification');
        }
    }

}

/* End of file config.php */
/* banner: ./application/modules/config/controllers/config.php */