<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Products extends CI_Controller {

	function __construct() {
        parent::__construct();
        $this->headers          = $this->input->request_headers();
        $this->api_version      = $this->headers['Api-Version'];
        $this->master_db        = $this->load->database('default', true)->database;
        date_default_timezone_set('UTC');
        header('Content-Type: application/json');
    }

    private function select_from_prod(){
        return "(select merk_id, cat_id, group_id, prod_no, prod_code0, prod_name0, prod_uom as Satuan1, prod_sell_price as Harga1, if(prod_sell_price4<>0, prod_uom2, '') as Satuan2, if (prod_sell_price4<>0,prod_sell_price4,'') as Harga2 from ".$this->master_db.".tproduct where is_delete = 0)";
    }

    public function index(){   
        if($this->api_version == '1'){
            $search         = $this->input->post('search');
            $storefront     = $this->input->post('storefront') ?: '';
            $type           = $this->input->post('type');
            $offset         = 0;
            $limit          = 50;
            $order          = null;
            $join = [
                ['table' => $this->select_from_prod().' tp', 'on' => 'tp.prod_no=product_id']
            ];
            $where['product_status'] = '1';
            if($storefront != ''){
                $where['group_id'] = $storefront;
            }

            if($type == 'flashsale'){
                $join[]     = ['table' => 'flashsale', 'on' => 'flashsale_product_id=product_id'];
                $where['flashsale_status'] = '1';
                $where['flashsale_expired_date > '] = date('Y-m-d H:i:s');
            } else if($type == 'emmy') {
                $join[]     = ['table' => 'emmy_products', 'on' => 'prodemmy_product_id=product_id'];
                $where['prodemmy_status'] = '1';
            } else if($type == 'newest'){
                $order      = ['product_createddate', 'DESC'];
            }

            $where_e        = "prod_name0 LIKE '%".$search."%'";
            $data           = $this->m_global->get_data_all('products/mobile', $join, $where, '*', $where_e, $order, $offset, $limit);
            $res_data       = [];
            foreach($data as $value){
                $prices[]       = ['price' => (Int)$value->Harga1, 'uom' => $value->Satuan1];
                if($value->Harga2 != ''){
                    $prices[]   = ['price' => (Int)$value->Harga2, 'uom' => $value->Satuan2];
                }
                $data_image = $this->m_global->get_data_all('product_thumbnails/mobile', null, ['prodthumb_product_id' => $value->product_id], 'prodthumb_url', null, null, 0, 1);
                if(count($data_image)){
                    $image = $data_image[0]->prodthumb_url;
                }else{
                    $image = "";
                }
                $res_data[] = [
                    'product_id' => $value->product_id,
                    'product_thumbnail' => $image,
                    'product_title' => $value->prod_name0,
                    'product_expired_date' => @$value->flashsale_expired_date ?: '',
                    'product_prices' => $prices
                ];
                $prices = [];
            }
            echo response_builder(true, 200, $res_data);
        }else if($this->api_version == '2'){
            $search         = $this->input->post('search');
            $storefront     = $this->input->post('storefront') ?: '';
            $type           = $this->input->post('type');
            $page           = $this->input->post('page') ?: '0';
            $limit          = 30;
            $offset         = $limit*$page;
            $order          = null;
            $join           = [
                                ['table' => $this->select_from_prod().' tp', 'on' => 'tp.prod_no=product_id']
                            ];
            $where['product_status'] = '1';
            if($storefront != ''){
                $where['group_id'] = $storefront;
            }

            $where_e        = "prod_name0 LIKE '%".$search."%'";
            if($type == 'flashsale'){
                $join[]     = ['table' => 'flashsale', 'on' => 'flashsale_product_id=product_id'];
                $where['flashsale_status'] = '1';
                $where['flashsale_expired_date > '] = date('Y-m-d H:i:s');
            } else if($type == 'emmy') {
                $join[]     = ['table' => 'emmy_products', 'on' => 'prodemmy_product_id=product_id'];
                $where['prodemmy_status'] = '1';
            } else if($type == 'newest'){
                $order      = ['product_createddate', 'DESC'];
                $where_e    .= " and (DATE(product_createddate) BETWEEN DATE_SUB(NOW(), INTERVAL 1 WEEK) AND NOW())";
            }

            $data           = $this->m_global->get_data_all('products/mobile', $join, $where, '*', $where_e, $order, $offset, $limit);
            $res_data       = [];
            foreach($data as $value){
                if($type == 'flashsale') {
                    $price1 = (Int)$value->Harga1;
                    $price2 = ($value->Harga2 != '' ? (Int)$value->Harga2 : 0 );
                    $discounts = get_discount_value($value->product_id, $price1, $price2);

                    $prices[]       = ['price' => $price1 - $discounts[0], 'uom' => $value->Satuan1, 'discount' => $discounts[0], 'price_before_discount' => $price1];
                    if($price2 > 0){
                        $prices[]   = ['price' => $price2 - $discounts[1], 'uom' => $value->Satuan2, 'discount' => $discounts[1], 'price_before_discount' => $price2];
                    }
                } else {
                    $prices[]       = ['price' => (Int)$value->Harga1, 'uom' => $value->Satuan1];
                    if($value->Harga2 != ''){
                        $prices[]   = ['price' => (Int)$value->Harga2, 'uom' => $value->Satuan2];
                    }
                }
                $data_image = $this->m_global->get_data_all('product_thumbnails/mobile', null, ['prodthumb_product_id' => $value->product_id], 'prodthumb_url', null, null, 0, 1);
                if(count($data_image)){
                    $image = $data_image[0]->prodthumb_url;
                }else{
                    $image = "";
                }
                $res_data[] = [
                    'product_id' => $value->product_id,
                    'product_thumbnail' => $image,
                    'product_title' => $value->prod_name0,
                    'product_expired_date' => @$value->flashsale_expired_date ?: '',
                    'product_prices' => $prices
                ];
                $prices = [];
            }
            echo response_builder(true, 200, $res_data);
        }else{
            echo response_builder(false, 900);
        }
    }

    public function detail(){   
        if($this->api_version == '1'){
            $product_id         = $this->input->post('product_id');
            $select_group       = "(select group_id, nama group_name from ".$this->master_db.".tgroup)";
            $select_category    = "(select cat_id, nama cat_name from ".$this->master_db.".tcat)";
            $select_brand       = "(select merk_id, nama brand_name from ".$this->master_db.".tm_merk)";
            $join = [
                ['table' => $this->select_from_prod().' tp', 'on' => 'tp.prod_no=product_id'],
                ['table' => $select_group.' tg', 'on' => 'tp.group_id=tg.group_id', 'tipe' => 'left'],
                ['table' => $select_category.' tc', 'on' => 'tp.cat_id=tc.cat_id', 'tipe' => 'left'],
                ['table' => $select_brand.' tm', 'on' => 'tp.merk_id=tm.merk_id', 'tipe' => 'left'],
            ];
            $data           = $this->m_global->get_data_all('products/mobile', $join, ['product_id' => $product_id])[0];
            $data_image     = $this->m_global->get_data_all('product_thumbnails/mobile', null, ['prodthumb_product_id' => $data->product_id], '*', null, ['prodthumb_id', 'ASC']);
            $data_flashsale = $this->m_global->get_data_all('flashsale/mobile', null, ['flashsale_product_id' => $data->product_id, 'flashsale_status <>' => '99']);

            foreach($data_image as $value){
                $images[] = $value->prodthumb_url;
            }

            $price1 = (Int)$data->Harga1;
            $price2 = ($data->Harga2 != '' ? (Int)$data->Harga2 : 0 );

            $discounts = get_discount_value($product_id, $price1, $price2);

            $prices[]       = ['price' => $price1 - $discounts[0], 'uom' => $data->Satuan1, 'discount' => $discounts[0], 'price_before_discount' => $price1];
            if($price2 > 0){
                $prices[]   = ['price' => $price2 - $discounts[1], 'uom' => $data->Satuan2, 'discount' => $discounts[1], 'price_before_discount' => $price2];
            }

            $res_data = [
                'product_id' => $data->product_id,
                'product_title' => $data->prod_name0,
                'product_category' => $data->cat_name ?: "-",
                'product_variant' => $data->group_name ?: "-",
                'product_brand' => $data->brand_name ?: "-",
                'product_description' => $data->product_desc,
                'product_thumbnails' => $images,
                'product_flashsale_id' => (count($data_flashsale) > 0 ? $data_flashsale[0]->flashsale_id : ""),
                'product_prices' => $prices
            ];

            $join = [
                ['table' => $this->select_from_prod().' tp', 'on' => 'tp.prod_no=product_id']
            ];
            $data_related   = $this->m_global->get_data_all('products/mobile', $join, ['product_status' => '1', 'group_id' => $data->group_id], '*', null, null, 0, 10);
            $res_data_related   = [];
            foreach($data_related as $value){
                $prices         = [];
                $prices[]       = ['price' => (Int)$value->Harga1, 'uom' => $value->Satuan1];
                if($value->Harga2 != ''){
                    $prices[]   = ['price' => (Int)$value->Harga2, 'uom' => $value->Satuan2];
                }
                $data_image = $this->m_global->get_data_all('product_thumbnails/mobile', null, ['prodthumb_product_id' => $value->prod_no], 'prodthumb_url', null, null, 0, 1);
                if(count($data_image) > 0){
                    $image = $data_image[0]->prodthumb_url;
                }else{
                    $image = "";
                }
                $res_data_related[] = [
                    'product_id' => $value->product_id,
                    'product_thumbnail' => $image,
                    'product_title' => $value->prod_name0,
                    'product_prices' => $prices
                ];
                $prices = [];
            }
            $res_data['related_products'] = $res_data_related;

            echo response_builder(true, 200, $res_data);
        }else{
            echo response_builder(false, 900);
        }
    }

    public function cart(){   
        if($this->api_version == '1'){
            $user_id                = $this->headers['User-Id'];
            $estimate_price         = $this->input->post('estimate_price') ?: 'false';
            $selected_address_id    = $this->input->post('selected_address_id') ?: '';
            $total_item             = 0.0;
            $total_price            = 0;

            if($selected_address_id != ''){
                $data_address       = $this->m_global->get_data_all('user_address/mobile', null, ['address_id' => $selected_address_id]);
            }else{
                $data_address       = $this->m_global->get_data_all('user_address/mobile', null, ['address_user_id' => $user_id, 'address_default' => '1', 'address_status' => '1']);
            }

            if(count($data_address) > 0){
                $value = $data_address[0];
                $res_data['address'] = [
                    'id' => $value->address_id,
                    'is_default' => $value->address_default == '1' ? true : false,
                    'title' => $value->address_title,
                    'label' => $value->address_label,
                    'full' => $value->address_full,
                    'detail' => $value->address_detail,
                    'receiver_name' => $value->address_receiver_name,
                    'latitude' => $value->address_latitude,
                    'longitude' => $value->address_longitude
                ];
            }else{
                $res_data['address'] = null;
            }

            $data_config                    = $this->m_global->get_data_all('config/mobile')[0];
            $res_data['tnc_title']          = $data_config->config_tnc_title;
            $res_data['tnc']                = $data_config->config_tnc;
            $res_data['show_sj_courier']    = $data_config->config_sj_courier == '1' ? true : false;

            $join = [
                ['table' => $this->select_from_prod().' tp', 'on' => 'tp.prod_no=cart_product_id']
            ];
            $data = $this->m_global->get_data_all('cart/mobile', $join, ['cart_user_id' => $user_id]);

            foreach($data as $value){
                $subtotal = $value->cart_qty * $value->cart_price;
                $data_image = $this->m_global->get_data_all('product_thumbnails/mobile', null, ['prodthumb_product_id' => $value->prod_no], 'prodthumb_url', null, null, 0, 1);
                if(count($data_image)){
                    $image = $data_image[0]->prodthumb_url;
                }else{
                    $image = "";
                }
                $res_data['products'][] = [
                    'product_id' => $value->cart_product_id,
                    'product_thumbnail' => $image,
                    'product_title' => $value->prod_name0,
                    'product_note' => $value->cart_note ?: "",
                    'product_qty' => (double)$value->cart_qty,
                    'product_sub_total' => $subtotal,
                    'product_purchased_price' => ['price' => $value->cart_price, 'uom' => $value->cart_uom]
                ];
                $total_item = $total_item + $value->cart_qty;
                $total_price = $total_price + $subtotal;
            }

            $res_data['total_item']     = (double)$total_item;
            $res_data['total_price']    = $total_price;
            if(count($data_address) > 0 && $estimate_price == 'true'){
                $gosend_price           = $this->estimate_gosend_price($data_address[0]->address_latitude.','.$data_address[0]->address_longitude);
                $res_data['gosend_fee']         = $gosend_price['price'];
                $res_data['gosend_distance']    = (String) $gosend_price['distance'] . " KM";
            }else{
                $res_data['gosend_fee']         = 0;
            }

            echo response_builder(true, 200, $res_data);
        }else{
            echo response_builder(false, 900);
        }
    }

    function add_to_cart(){
        if($this->api_version == '1'){
            $user_id        = $this->headers['User-Id'];
            $product_id     = $this->input->post('product_id');
            $qty            = $this->input->post('qty');
            $base_price     = $this->input->post('base_price');
            $uom            = $this->input->post('uom');
            $unit           = $this->input->post('unit');

            $check_data = $this->m_global->get_data_all('cart/mobile', null, ['cart_user_id' => $user_id,'cart_product_id' => $product_id]);
            if(count($check_data) == 0){
                $data['cart_user_id']       = $user_id;
                $data['cart_product_id']    = $product_id;
                $data['cart_qty']           = $qty;
                $data['cart_uom']           = $uom;
                $data['cart_unit']          = $unit;
                $data['cart_price']         = $base_price;
                $data['cart_createddate']   = date('Y-m-d H:i:s');

                $result = $this->m_global->insert('cart/mobile', $data);
                if($result['status']){
                    echo response_builder(true, 201);
                }else{
                    echo response_builder(false, 406, null, 'failed create data');
                }
            }else{
                $data['cart_qty']           = $check_data[0]->cart_qty + $qty;
                $result = $this->m_global->update('cart/mobile', $data, ['cart_user_id' => $user_id,'cart_product_id' => $product_id]);
                if($result){
                    echo response_builder(true, 201);
                }else{
                    echo response_builder(false, 406, null, 'failed update data');
                }
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    function update_cart(){
        if($this->api_version == '1'){
            $user_id        = $this->headers['User-Id'];
            $product_id     = $this->input->post('product_id');
            $qty            = $this->input->post('qty');
            $note           = $this->input->post('note');
            $type           = $this->input->post('type');

            if($type == 'update'){
                $data['cart_qty']    = $qty;
                $data['cart_note']   = $note;
                $result = $this->m_global->update('cart/mobile', $data, ['cart_user_id' => $user_id,'cart_product_id' => $product_id]);
                if($result){
                    echo response_builder(true, 201);
                }else{
                    echo response_builder(false, 406, null, 'failed update data');
                }
            }else{
                $result = $this->m_global->delete('cart/mobile', ['cart_user_id' => $user_id,'cart_product_id' => $product_id]);
                if($result){
                    echo response_builder(true, 201);
                }else{
                    echo response_builder(false, 406, null, 'failed delete data');
                }
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    function track_product_tap(){
        if($this->api_version == '1'){
            $data['producttap_product_id']    = $this->input->post('id');
            $data['producttap_user_id']       = @$this->headers['User-Id'] ?: '-';
            $result = $this->m_global->insert('products_tap_log/mobile', $data);
            if($result['status']){
                echo response_builder(true, 201);
            }else{
                echo response_builder(false, 406, null, 'failed insert data');
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    function track_flashsale_tap(){
        if($this->api_version == '1'){
            $data['flashtap_flashsale_id']  = $this->input->post('id');
            $data['flashtap_user_id']       = @$this->headers['User-Id'] ?: '-';
            $result = $this->m_global->insert('flashsale_tap_log/mobile', $data);
            if($result['status']){
                echo response_builder(true, 201);
            }else{
                echo response_builder(false, 406, null, 'failed insert data');
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    function storefront(){
        if($this->api_version == '1'){
            $search     = $this->input->post('search');
            $res_data   = [];
            $where_e    = "nama LIKE '%".$search."%'";
            $data       = $this->m_global->get_data_all('tgroup', null, ['is_delete' => '0'], '*', $where_e, ['nama', 'ASC']);

            $res_data[] = [
                'id' => "",
                'title' => "SEMUA ETALASE",
                'value' => ""
            ];

            foreach($data as $value){
                if(strlen($value->nama) > 1){
                    $res_data[] = [
                        'id' => $value->group_id,
                        'title' => $value->nama,
                        'value' => $value->kode
                    ];
                }
            }
            echo response_builder(true, 200, $res_data);
        }else{
            echo response_builder(false, 900);
        }
    }

    function estimate_gosend_price_test(){
        echo '<pre>';
        print_r($this->estimate_gosend_price(""));
    }

    function estimate_gosend_price($destination){
        $user_id        = $this->headers['User-Id'];
        $data_config    = $this->m_global->get_data_all('config/mobile', null, null, 'config_store_latitude,config_store_longitude')[0];
        $origin         = $data_config->config_store_latitude.','.$data_config->config_store_longitude;
        $ch             = curl_init();
        curl_setopt( $ch, CURLOPT_URL, GOSEND_ESTIMATE_URL.'?origin='.$origin.'&destination='.$destination.'&paymentType=3');
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
            'Client-ID: '.GOSEND_CLIENT_ID,
            'Pass-Key: '.GOSEND_PASS_KEY
        ));
        $result = curl_exec($ch);
        $result = json_decode($result, true);
        curl_close($ch);

        $service_type = 'Instant';
        // $service_type = 'SameDay';
        if(array_key_exists($service_type, $result)){
            if($result[$service_type]['serviceable'] == 1 && $result[$service_type]['active'] == 1){
                return ['price' => $result[$service_type]['price']['total_price'], 'distance' => $result[$service_type]['distance']];
            }else{
                return ['price' => 0, 'distance' => $result[$service_type]['distance']];
            }
        }else{
            return ['price' => 0, 'distance' => 0];
        }
    }

    function address_add(){
        if($this->api_version == '1'){
            $user_id                        = $this->headers['User-Id'];

            $check_available = $this->m_global->count_data_all('user_address/mobile', null, ['address_user_id' => $user_id, 'address_status' => '1']);

            $data['address_user_id']        = $user_id;
            $data['address_title']          = $this->input->post('title');
            $data['address_label']          = $this->input->post('label');
            $data['address_receiver_name']  = $this->input->post('receiver_name');
            $data['address_full']           = $this->input->post('full');
            $data['address_detail']         = $this->input->post('detail');
            $data['address_latitude']       = $this->input->post('latitude');
            $data['address_longitude']      = $this->input->post('longitude');
            $data['address_default']        = $check_available == 0 ? '1' : '0';
            $data['address_createddate']    = date('Y-m-d H:i:s');

            $result = $this->m_global->insert('user_address/mobile', $data);
            if($result['status']){
                echo response_builder(true, 201, null, 'Berhasil menambahkan alamat');
            }else{
                echo response_builder(false, 406, null, 'Gagal menambahkan alamat');
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    function address_update(){
        if($this->api_version == '1'){
            $user_id                        = $this->headers['User-Id'];
            $is_default                     = $this->input->post('is_default') ?: '0';
            $status                         = $this->input->post('status') ?: '1';
            $id                             = $this->input->post('id');
            $data['address_title']          = $this->input->post('title');
            $data['address_label']          = $this->input->post('label');
            $data['address_receiver_name']  = $this->input->post('receiver_name');
            $data['address_full']           = $this->input->post('full');
            $data['address_detail']         = $this->input->post('detail');
            $data['address_latitude']       = $this->input->post('latitude');
            $data['address_longitude']      = $this->input->post('longitude');
            $data['address_default']        = $is_default;
            $data['address_status']         = $status;
             
            if($is_default == '1'){
                $update['address_default']  = '0';
                $this->m_global->update('user_address/mobile', $update, ['address_user_id' => $user_id]);
            }

            $result = $this->m_global->update('user_address/mobile', $data, ['address_id' => $id]);
            if($result){
                echo response_builder(true, 201, null, 'Berhasil mengubah alamat');
            }else{
                echo response_builder(false, 406, null, 'Gagal mengubah alamat');
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    function address_list(){
        if($this->api_version == '1'){
            $user_id    = $this->headers['User-Id'];
            $res_data   = [];
            $data       = $this->m_global->get_data_all('user_address/mobile', null, ['address_user_id' => $user_id, 'address_status' => '1'], '*', null, ['address_createddate', 'DESC']);

            foreach($data as $value){
                $res_data[] = [
                    'id' => $value->address_id,
                    'is_default' => $value->address_default == '1' ? true : false,
                    'title' => $value->address_title,
                    'label' => $value->address_label,
                    'full' => $value->address_full,
                    'detail' => $value->address_detail,
                    'receiver_name' => $value->address_receiver_name,
                    'latitude' => $value->address_latitude,
                    'longitude' => $value->address_longitude
                ];
            }
            echo response_builder(true, 200, $res_data);
        }else{
            echo response_builder(false, 900);
        }
    }

}

/* End of file config.php */
/* banner: ./application/modules/config/controllers/config.php */