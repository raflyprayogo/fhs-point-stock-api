<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Stock extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->headers          = $this->input->request_headers();
        $this->api_version      = $this->headers['Api-Version'];
        $this->mobile_db        = $this->load->database('mobile', true)->database;
        $this->load->model('m_stock');
        $this->const_warehouse_id = [
            'all' => '',
            'madura' => 'ID2',
            'bandahara' => 'ID-190121-105451-0003'
        ];
        $this->reused_enum = [
            '0' => 'NONE',
            '1' => 'R',
            '2' => 'NR',
            '3' => 'T' 
        ];
        $this->retur_day_enum = [
            '0' => 'N',
            '1' => 'D',
            '2' => 'M',
            '3' => 'Y' 
        ];
        $this->basic_product_select = "tp.prod_no,
            tp.prod_code0,
            tp.prod_name0,
            tp.prod_uom uom1, tp.prod_uom2 uom2, tp.prod_uom3 uom3,
            tp.prod_sell_price price1, tp.prod_sell_price4 price2, tp.prod_sell_price7 price3,
            tp.konversi1, tp.konversi2, tp.konversi3,
            tp.prod_buy_price,
            tp.prod_last_ppn,
            tp.prod_last_biaya,
            tp.is_reused, 
            tp.is_retur_day,
            tp.is_retur_value, ";
        // date_default_timezone_set('UTC');
        header('Content-Type: application/json');
        acc_token();
    }

    public function index(){   
        echo "FH Stock Rest API";
    }

    private function product_thumbnail($id){
        $product_thumbnail = "";
        $thumbnails = $this->m_global->get_data_all('product_thumbnails/mobile', null, ['prodthumb_product_id' => $id], 'prodthumb_url', null, null, 0, 1);
        if(count($thumbnails) > 0) {
            $product_thumbnail = $thumbnails[0]->prodthumb_url;
        }
        return $product_thumbnail;
    }

    private function product_prices($data){
        $prices[] = [
            'price' => (Int) $data->price1,
            'uom' => $data->uom1,
            'uom_qty' => $data->konversi1
        ];
        if($data->uom2 != ''){
            $prices[] = [
                'price' => (Int) $data->price2,
                'uom' => $data->uom2,
                'uom_qty' => $data->konversi2
            ];
        }
        if($data->uom3 != ''){
            $prices[] = [
                'price' => (Int) $data->price3,
                'uom' => $data->uom3,
                'uom_qty' => $data->konversi3
            ];
        }
        return $prices;
    }

    private function product_quantities_csw($id){
        $wh_id              = $this->headers['Warehouse-Id'];
        $database           = ($wh_id == DEFAULT_WAREHOUSE ? 'tdgproduct_wh' : 'tdgproduct_wh/bandahara');
        $value_bandahara    = '0';
        $value_madura       = '0';
        $data = $this->m_global->get_data_all($database, null, ['prod_no' => $id], 'gud_no, prod_on_hand');
        foreach ($data as $value) {
            if($value->gud_no == 'ID-190121-105451-0003'){
                $value_bandahara = $value->prod_on_hand;
            }
            if($value->gud_no == 'ID2'){
                $value_madura = $value->prod_on_hand;
            }
        } 
        return [
            'title' => 'csw',
            'value' => (String) ($value_bandahara + $value_madura)
        ];
    }

    private function product_quantities($id, $show_csw = false, $is_global_qty_accumulation = false){
        $wh_id      = $this->headers['Warehouse-Id'];
        $database   = ($wh_id == DEFAULT_WAREHOUSE ? 'tdgproduct' : 'tdgproduct/bandahara');
        $value_bandahara = '0';
        $value_madura = '0';
        $data = $this->m_global->get_data_all($database, null, ['prod_no' => $id], 'gud_no, prod_on_hand');
        foreach ($data as $value) {
            if($value->gud_no == 'ID-190121-105451-0003'){
                $value_bandahara = $value->prod_on_hand;
            }
            if($value->gud_no == 'ID2'){
                $value_madura = $value->prod_on_hand;
            }
        }
        $quantities[] = [
            'title' => 'bandahara',
            'value' => $value_bandahara
        ];
        $quantities[] = [
            'title' => 'madura',
            'value' => $value_madura
        ];
        if($is_global_qty_accumulation) {
            $quantities[] = [
                'title' => 'global',
                'value' => (String) ($value_madura + $value_bandahara)
            ];
        } else {
            $quantities[] = [
                'title' => 'global',
                'value' => (String) ($wh_id == DEFAULT_WAREHOUSE ? $value_madura : $value_bandahara )
            ];
        }
        if($show_csw) {
            $quantities[] = $this->product_quantities_csw($id);
        }
        return $quantities;
    }

    function detail_product(){
        if($this->api_version == '1'){
            $prod_code      = $this->input->post('code');
            $select         = 'prod_code0 code, prod_no no, prod_name0 name, prod_uom uom, prod_sell_price price';
            $data           = $this->m_global->get_data_all('tproduct', null, ['prod_code0' => $prod_code], $select);
            if(count($data) > 0){
                echo response_builder(true, 200, $data[0]);
            }else{
                echo response_builder(false, 412);
            }
        }else if($this->api_version == '2'){
            $search         = $this->input->post('search');
            $gud_no         = $this->m_global->get_data_all('tusers', null, ['User_id' => $this->headers['User-Id']])[0]->gud_no;
            $select         = 'a.prod_code0 code, a.prod_no no, a.prod_name0 name, a.prod_uom uom, IF( a.prod_sell_type = 0, a.prod_sell_price, a.prod_sell_qty1 ) price, b.prod_on_hand stock, a.prod_uom uom';
            $where          = "prod_name0 = '".$search."' OR prod_code0 = '".$search."'";
            $join           = [
                                ['table' => "( SELECT tproduct.prod_no, tdgproduct.prod_on_hand FROM tdgproduct LEFT JOIN tproduct ON tdgproduct.prod_no = tproduct.prod_no WHERE gud_no = '".$gud_no."' ) b", 'on' => 'a.prod_no = b.prod_no', 'tipe' => 'LEFT']
                            ];
            $data           = $this->m_global->get_data_all('tproduct a', $join, null, $select, $where);
            if(count($data) > 0){
                $data[0]->stock = ($data[0]->stock == null ? '0' : $data[0]->stock);
                echo response_builder(true, 200, $data[0]);
            }else{
                echo response_builder(false, 412);
            }
        }else if($this->api_version == '3'){
            $wh_id          = $this->headers['Warehouse-Id'];
            $hide_qty       = $this->input->post('hide_qty');
            $search         = $this->input->post('search');
            if(trim($search) == '') {
                echo response_builder(false, 412);
                exit;
            }
            $where          = "(prod_code0 = '".$search."' or prod_code1 = '".$search."')";
            $select         = $this->basic_product_select."tg.nama as variant_name,
                                tmrk.nama as brand_name,
                                trck.nama as rack_name
                                ";
            $join           = [
                                ['table' => 'tgroup tg', 'on' => 'tg.group_id=tp.group_id', 'tipe' => 'LEFT'],
                                ['table' => 'tm_merk tmrk', 'on' => 'tmrk.merk_id=tp.merk_id', 'tipe' => 'LEFT'],
                                ['table' => 'tm_rak trck', 'on' => 'trck.rak_id=tp.rak_id', 'tipe' => 'LEFT']
                            ];

            $data           = $this->m_global->get_data_all('tproduct tp', $join, ['tp.is_delete' => '0'], $select, $where);
            if(count($data) > 0){
                $response = [
                    'product_id' => $data[0]->prod_no,
                    'product_code' => $data[0]->prod_code0,
                    'product_name' => $data[0]->prod_name0,
                    'product_variant' => $data[0]->variant_name ?: '',
                    'product_brand' => $data[0]->brand_name ?: '',
                    'product_rack' =>  $data[0]->rack_name ?: '',
                    'product_reuse_status' =>  $this->reused_enum[$data[0]->is_reused].' '.$this->retur_day_enum[$data[0]->is_retur_day].' '.$data[0]->is_retur_value,
                    'product_thumbnail' => $this->product_thumbnail($data[0]->prod_no),
                    'product_prices' => $this->product_prices($data[0])
                ];
                if($hide_qty == '') {
                    $response['product_quantities'] = $this->product_quantities($data[0]->prod_no, true);
                }
                echo response_builder(true, 200, $response);
            }else{
                echo response_builder(false, 412);
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    function search_product(){
        if($this->api_version == '1'){
            $search         = $this->input->post('search');
            $select         = 'prod_code0 code, prod_no no, prod_name0 name, prod_sell_price price, prod_min_stock stock';
            $where          = "prod_name0 LIKE '%".$search."%' OR prod_code0 LIKE '%".$search."%'";
            $data           = $this->m_global->get_data_all('tproduct', null, null, $select, $where, null, 0, 20);
            echo response_builder(true, 200, $data);
        }else{
            echo response_builder(false, 900);
        }
    }

    function add_stock(){
        if($this->api_version == '1'){
            $user_id        = $this->headers['User-Id'];
            $gud_no         = $this->input->post('gud_no');
            $cab_no         = $this->input->post('cat_gud_no');
            $note           = $this->input->post('note');
            $adj_no         = $this->input->post('adj_no');
            $products       = json_decode($this->input->post('products'));
            $date           = date('Y-m-d H:i:s');
            $is_edit        = true;

            if($adj_no == ''){
                $adj_no                 = strtoupper(strEncrypt($user_id.$date));
                $data['adj_no']         = $adj_no;
                $data['adj_date']       = $date;
                $data['gud_no']         = $gud_no;
                $data['cab_no']         = $cab_no;
                $data['user_id']        = $user_id;
                $data['create_date']    = $date;
                $is_edit                = false;
            }
            
            $data['adj_desc']       = $note;
            $data['user_edit']      = $user_id;
            $data['edit_date']      = $date;
            $data['is_delete']      = '0';
            $data['iUpload']        = '1';

            if(!$is_edit){
                $result = $this->m_global->insert('tadjust', $data);
                if(!$result['status']){
                    echo response_builder(false, 406, null, 'failed create data');
                    exit;
                }
            }else{
                $result = $this->m_global->update('tadjust', $data, ['adj_no' => $adj_no]);
                if(!$result){
                    echo response_builder(false, 406, null, 'failed update data');
                    exit;
                }
            }

            $this->m_global->delete('td_adjust', ['adj_no' => $adj_no]);
            foreach ($products as $value) {
                $data_prod['adj_det_no']    = strtoupper(strEncrypt($value->no.$user_id.$date));
                $data_prod['adj_no']        = $adj_no;
                $data_prod['prod_no']       = $value->no;
                $data_prod['gud_no']        = $gud_no;
                $data_prod['qty_satuan']    = $value->qty;
                $data_prod['satuan']        = '1';
                $data_prod['konversi']      = '1';
                $this->m_global->insert('td_adjust', $data_prod);
            }

            echo response_builder(true, 201, ['id' => $adj_no]);
        }else{
            echo response_builder(false, 900);
        }
    }

    function history(){
        if($this->api_version == '1'){
            $user_id        = $this->headers['User-Id'];
            $select         = 'adj_no no, create_date date, adj_desc note';
            $data           = $this->m_global->get_data_all('tadjust', null, ['user_id' => $user_id, 'is_delete' => '0'], $select, null, ['create_date', 'DESC']);
            $res_data       = [];
            for ($i=0; $i < count($data); $i++) { 
                $counter    = 0;
                $detail     = $this->m_global->get_data_all('td_adjust', null, ['adj_no' => $data[$i]->no]);
                for ($d=0; $d < count($detail); $d++) { 
                    if($detail[$d]->det_qty_adjust == 0){
                        $counter++;
                    }
                }
                if(count($detail) == $counter){
                    $data[$i]->total = $counter;
                    $res_data[] = $data[$i];
                }
            }
            echo response_builder(true, 200, $res_data);
        }else{
            echo response_builder(false, 900);
        }
    }

    function history_detail(){
        if($this->api_version == '1'){
            $no             = $this->input->post('no');
            $select         = 'tp.prod_name0 name, tp.prod_no no, tp.prod_code0 code, ta.qty_satuan qty, prod_sell_price price, tp.prod_uom uom';
            $join           = [
                                ['table' => 'tproduct tp', 'on' => 'tp.prod_no=ta.prod_no']
                            ];
            $res_data       = $this->m_global->get_data_all('td_adjust ta', $join, ['ta.adj_no' => $no], $select);
            echo response_builder(true, 200, $res_data);
        }else{
            echo response_builder(false, 900);
        }
    }

    function delete_history(){
        if($this->api_version == '1'){
            $no             = $this->input->post('no');
            $data['is_delete'] = '1';
            $result   = $this->m_global->update('tadjust', $data, ['adj_no' => $no]);
            if($result){
                echo response_builder(true, 201);
            }else{
                echo response_builder(false, 406, null, 'failed update data');
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    function supplier_list(){
        if($this->api_version == '1'){
            $search         = $this->input->post('search');
            $select         = 'person_no id, person_name title';
            $where          = "(person_name LIKE '%".$search."%' OR person_no LIKE '%".$search."%')";
            $data           = $this->m_global->get_data_all('tperson', null, ['person_type' => '1', 'is_delete' => '0'], $select, $where, null, 0, 20);
            echo response_builder(true, 200, $data);
        }else{
            echo response_builder(false, 900);
        }
    }

    function warehouse_list(){
        if($this->api_version == '1'){
            $response = [];
            $data = $this->m_global->get_data_all('tgudang', null, ['is_delete' => '0']);
            foreach ($data as $value) {
                $response[] = [
                    'id' => $value->gud_no,
                    'title' => $value->gud_code.' '.$value->gud_name
                ];
            }
            echo response_builder(true, 200, $response);
        }else{
            echo response_builder(false, 900);
        }
    }

    function list_category(){
        if($this->api_version == '1'){
            $response   = [];
            $search     = $this->input->post('search');
            $limit      = $this->input->post('limit') ?: '20';
            $where      = "(kode LIKE '%".$search."%' OR nama LIKE '%".$search."%')";
            $data       = $this->m_global->get_data_all('tcat', null, ['is_delete' => '0'], 'cat_id, kode, nama', $where, ['nama', 'ASC'], 0, $limit);
            foreach ($data as $value) {
                $response[] = [
                    'id' => $value->cat_id,
                    'title' => $value->kode.' - '.$value->nama
                ];
            }
            echo response_builder(true, 200, $response);
        }else{
            echo response_builder(false, 900);
        }
    }

    function list_variant(){
        if($this->api_version == '1'){
            $response   = [];
            $search     = $this->input->post('search');
            $limit      = $this->input->post('limit') ?: '20';
            $where      = "(kode LIKE '%".$search."%' OR nama LIKE '%".$search."%')";
            $data       = $this->m_global->get_data_all('tgroup', null, ['is_delete' => '0'], 'group_id, kode, nama', $where, ['nama', 'ASC'], 0, $limit);
            foreach ($data as $value) {
                $response[] = [
                    'id' => $value->group_id,
                    'title' => $value->kode.' - '.$value->nama
                ];
            }
            echo response_builder(true, 200, $response);
        }else{
            echo response_builder(false, 900);
        }
    }

    function list_subvariant(){
        if($this->api_version == '1'){
            $response   = [];
            $search     = $this->input->post('search');
            $limit      = $this->input->post('limit') ?: '20';
            $where      = "(kode LIKE '%".$search."%' OR nama LIKE '%".$search."%')";
            $data       = $this->m_global->get_data_all('tvarian', null, ['is_delete' => '0'], 'varian_id, kode, nama', $where, ['nama', 'ASC'], 0, $limit);
            foreach ($data as $value) {
                $response[] = [
                    'id' => $value->varian_id,
                    'title' => $value->kode.' - '.$value->nama
                ];
            }
            echo response_builder(true, 200, $response);
        }else{
            echo response_builder(false, 900);
        }
    }

    function list_brand(){
        if($this->api_version == '1'){
            $response   = [];
            $search     = $this->input->post('search');
            $limit      = $this->input->post('limit') ?: '20';
            $where      = "(kode LIKE '%".$search."%' OR nama LIKE '%".$search."%')";
            $data       = $this->m_global->get_data_all('tm_merk', null, ['is_delete' => '0'], 'merk_id, kode, nama', $where, ['nama', 'ASC'], 0, $limit);
            foreach ($data as $value) {
                $response[] = [
                    'id' => $value->merk_id,
                    'title' => $value->kode.' - '.$value->nama
                ];
            }
            echo response_builder(true, 200, $response);
        }else{
            echo response_builder(false, 900);
        }
    }

    function list_rack(){
        if($this->api_version == '1'){
            $response   = [];
            $search     = $this->input->post('search');
            $limit      = $this->input->post('limit') ?: '20';
            $where      = "(kode LIKE '%".$search."%' OR nama LIKE '%".$search."%')";
            $data       = $this->m_global->get_data_all('tm_rak', null, ['is_delete' => '0'], 'rak_id, kode, nama', $where, ['nama', 'ASC'], 0, $limit);
            foreach ($data as $value) {
                $response[] = [
                    'id' => $value->rak_id,
                    'title' => $value->kode.' - '.$value->nama
                ];
            }
            echo response_builder(true, 200, $response);
        }else{
            echo response_builder(false, 900);
        }
    }

    function list_rack_warehouse(){
        if($this->api_version == '1'){
            $response   = [];
            $search     = $this->input->post('search');
            $limit      = $this->input->post('limit') ?: '20';
            $where      = "(kode LIKE '%".$search."%' OR nama LIKE '%".$search."%')";
            $data       = $this->m_global->get_data_all('tm_rak_gudang', null, ['is_delete' => '0'], 'rak_gudang, kode, nama', $where, ['nama', 'ASC'], 0, $limit);
            foreach ($data as $value) {
                $response[] = [
                    'id' => $value->rak_gudang,
                    'title' => $value->kode.' - '.$value->nama
                ];
            }
            echo response_builder(true, 200, $response);
        }else{
            echo response_builder(false, 900);
        }
    }

    function product_list(){
        if($this->api_version == '1'){
            $response   = [];
            $search     = $this->input->post('search');
            $show_csw   = $this->input->post('show_csw');
            $is_selection = $this->input->post('is_selection');
            $page       = $this->input->post('page') ?: '0';
            $limit      = 30;
            $offset     = $limit*$page;
            $where      = "(tp.prod_code0 LIKE '%".$search."%' OR tp.prod_name0 LIKE '%".$search."%')";
            if($is_selection != '') {
                $select     = $this->basic_product_select;
                $data       = $this->m_global->get_data_all('tproduct tp', null, ['tp.is_delete' => '0'], $select, $where, null, $offset, $limit, ['tp.prod_no']);
                foreach ($data as $value) {
                    // $quantities = [];
                    // if($show_csw != '') {
                    //     $quantities = [$this->product_quantities_csw($value->prod_no)];
                    // }
                    $response[] = [
                        'product_id' => $value->prod_no,
                        'product_code' => $value->prod_code0,
                        'product_name' => $value->prod_name0,
                        'product_prices' => $this->product_prices($value),
                        'product_quantities' => $this->product_quantities($value->prod_no, ($show_csw != '' ? true : false), false)
                    ];
                }
            } else {
                $select     = $this->basic_product_select."tg.nama as variant_name,
                                tmrk.nama as brand_name,
                                trck.nama as rack_name
                                ";
                $join       = [
                                ['table' => 'tgroup tg', 'on' => 'tg.group_id=tp.group_id', 'tipe' => 'LEFT'],
                                ['table' => 'tm_merk tmrk', 'on' => 'tmrk.merk_id=tp.merk_id', 'tipe' => 'LEFT'],
                                ['table' => 'tm_rak trck', 'on' => 'trck.rak_id=tp.rak_id', 'tipe' => 'LEFT']
                            ];
                $data       = $this->m_global->get_data_all('tproduct tp', $join, ['tp.is_delete' => '0'], $select, $where, null, $offset, $limit, ['tp.prod_no']);
                foreach ($data as $value) {
                    $response[] = [
                        'product_id' => $value->prod_no,
                        'product_code' => $value->prod_code0,
                        'product_name' => $value->prod_name0,
                        'product_variant' => $value->variant_name ?: '',
                        'product_brand' => $value->brand_name ?: '',
                        'product_rack' =>  $value->rack_name ?: '',
                        'product_prices' => $this->product_prices($value),
                        'product_quantities' => $this->product_quantities($value->prod_no, ($show_csw != '' ? true : false), true),
                        'product_cost' => [
                            'buy' => $value->prod_buy_price,
                            'ppn' => $value->prod_last_ppn,
                            'ppn_cost' => $value->prod_last_biaya
                        ]
                    ];
                }
            }
            echo response_builder(true, 200, $response);
        }else{
            echo response_builder(false, 900);
        }
    }

    function stock_card(){
        if($this->api_version == '1'){
            $response       = [];
            $prod_no        = $this->input->post('product_code');
            $date_start     = $this->input->post('date_start');
            $date_end       = $this->input->post('date_end');
            $type           = $this->input->post('type');
            $tran_type      = $this->input->post('tran_type') ?: '';
            $init_balance   = $this->m_stock->stock_card_init_balance($this->api_version, $prod_no, $date_start, 'ttrans', DEFAULT_WAREHOUSE, $tran_type, $this->const_warehouse_id[$type]);
            $data           = $this->m_stock->stock_card($this->api_version, $prod_no, $date_start, $date_end, $tran_type, $this->const_warehouse_id[$type]);
            $balance        = $init_balance[0]->OnHand;
            if(count($init_balance) > 0) {
                $response[] = [
                    'id' => '',
                    'qty_in' => '',
                    'qty_out' => '',
                    'balance' => $init_balance[0]->OnHand,
                    'date' => 'Saldo Awal'
                ];
            }
            foreach ($data as $value) {
                $debet = ($value->kredit < 0 ? ($value->kredit * -1) : $value->debet );
                $kredit = ($value->kredit > 0 ? $value->kredit : 0 );
                $balance = ($balance + $debet) - $kredit;
                $response[] = [
                    'id' => $value->Transaksi,
                    'qty_in' => $debet,
                    'qty_out' => $kredit,
                    'balance' => (String) $balance,
                    'date' => $value->tgl
                ];
            }
            echo response_builder(true, 200, $response);
        }else{
            echo response_builder(false, 900);
        }
    }

    function stock_card_csw(){
        if($this->api_version == '1'){
            $response       = [];
            $wh_id          = $this->headers['Warehouse-Id'];
            $prod_no        = $this->input->post('product_code');
            $date_start     = $this->input->post('date_start');
            $date_end       = $this->input->post('date_end');
            $type           = $this->input->post('type');
            $init_balance   = $this->m_stock->stock_card_init_balance($this->api_version, $prod_no, $date_start, 'ttrans_wh', $wh_id, '', $this->const_warehouse_id[$type]);
            $data           = $this->m_stock->stock_card_csw($this->api_version, $prod_no, $date_start, $date_end, $wh_id, $this->const_warehouse_id[$type]);
            $balance        = $init_balance[0]->OnHand;
            if(count($init_balance) > 0) {
                $response[] = [
                    'id' => '',
                    'qty_in' => '',
                    'qty_out' => '',
                    'balance' => $init_balance[0]->OnHand,
                    'date' => 'Saldo Awal'
                ];
            }
            foreach ($data as $value) {
                $debet = ($value->kredit < 0 ? ($value->kredit * -1) : $value->debet );
                $kredit = ($value->kredit > 0 ? $value->kredit : 0 );
                $balance = ($balance + $debet) - $kredit;
                $response[] = [
                    'id' => $value->Transaksi,
                    'qty_in' => $debet,
                    'qty_out' => $kredit,
                    'balance' => (String) $balance,
                    'date' => $value->tgl
                ];
            }
            echo response_builder(true, 200, $response);
        }else{
            echo response_builder(false, 900);
        }
    }

    function price_changes(){
        if($this->api_version == '1'){
            $response       = [];
            $prod_no        = $this->input->post('product_code');
            $date_start     = $this->input->post('date_start');
            $date_end       = $this->input->post('date_end');
            $data           = $this->m_stock->price_changes($this->api_version, $prod_no, $date_start, $date_end);
            foreach ($data as $value) {
                $response[] = [
                    'product_code' => $value->prod_code0,
                    'product_name' => $value->prod_name0,
                    'product_date' => $value->price_date ?: EMPTY_DATE,
                    'product_prices' => [
                        [
                            'price' => (Int) $value->hj_new_1 ?: 0,
                            'uom' => $value->Satuan
                        ],
                        [
                            'price' => (Int) $value->hj_old_1 ?: 0,
                            'uom' => $value->Satuan
                        ]
                    ]
                ];
            }
            echo response_builder(true, 200, $response);
        }else{
            echo response_builder(false, 900);
        }
    }

    function graph_report_sell(){
        if($this->api_version == '1'){
            $response       = [];
            $prod_no        = $this->input->post('product_code');
            $date_start     = $this->input->post('date_start');
            $date_end       = $this->input->post('date_end');
            $type           = $this->input->post('type');
            $data           = $this->m_stock->graph_report_sell($this->api_version, $prod_no, $date_start, $date_end, $type);
            foreach ($data as $value) {
                $response[] = [
                    'title' => $value->Harian,
                    'value' => (String) $value->value ?: '0'
                ];
            }
            echo response_builder(true, 200, $response);
        }else{
            echo response_builder(false, 900);
        }
    }

    function graph_report_buy(){
        if($this->api_version == '1'){
            $response       = [];
            $data_type      = $this->input->post('data_type');
            $data_code      = $this->input->post('data_code');
            $date_start     = $this->input->post('date_start');
            $date_end       = $this->input->post('date_end');
            $type           = $this->input->post('type');
            $data           = $this->m_stock->graph_report_buy($this->api_version, $data_type, $data_code, $date_start, $date_end, $type);
            foreach ($data as $value) {
                $response[] = [
                    'title' => $value->Harian,
                    'value' => (String) $value->value ?: '0'
                ];
            }
            echo response_builder(true, 200, $response);
        }else{
            echo response_builder(false, 900);
        }
    }

    function submit_planogram(){
        if($this->api_version == '1'){
            $wh_id      = $this->headers['Warehouse-Id'];
            $plan_id    = $this->input->post('plan_id');
            $rack_id    = $this->input->post('id');
            $cab_no     = $this->input->post('cab_no');
            $note       = $this->input->post('note');
            $products   = base64_decode($this->input->post('products'));
            $products   = json_decode($products);
            $database   = ($wh_id == DEFAULT_WAREHOUSE ? 'tplanogram' : 'tplanogram/bandahara');
            $date       = date('Y-m-d H:i:s');

            if($plan_id != '') {
                $this->submit_detail_planogram($wh_id, $plan_id, $products);
                exit;
            }

            if($wh_id == DEFAULT_WAREHOUSE) {
                $plan_no_prefix  = 'PT-P'.date('ymd');
            } else {
                $plan_no_prefix  = 'CB-P'.date('ymd');
            }

            $where_e        = "plan_no LIKE '%".$plan_no_prefix."%'";
            $check_queue    = $this->m_global->get_data_all($database, null, null, 'plan_no', $where_e, ['plan_no', 'DESC'], 0, 1);
            $queue          = 1;
            if(count($check_queue) > 0) {
                $queue = str_replace($plan_no_prefix, '', $check_queue[0]->plan_no);
                $queue = ((Int) $queue) + 1;
            }

            $plan_no                = $plan_no_prefix.str_pad($queue, 5, '0', STR_PAD_LEFT);
            $data['plan_no']        = $plan_no;
            $data['plan_date']      = $date;
            $data['plan_type']      = '0';
            $data['plan_desc']      = $note;
            $data['gud_no']         = $wh_id;
            $data['rak_id']         = $rack_id;
            $data['is_delete']      = '0';
            $data['create_date']    = $date;
            $data['user_id']        = $this->headers['User-Id'];
            $data['cab_no']         = $cab_no;
            $data['iUpload']        = '0';
            $result                 = $this->m_global->insert($database, $data);

            if($result['status']) {
                $this->submit_detail_planogram($wh_id, $plan_no, $products);
                $this->m_global->update($database, ['iUpload' => '1'], ['plan_no' => $plan_no]);
            } else {
                echo response_builder(false, 406, null, 'Gagal menambah data Planogram');
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    function submit_detail_planogram($wh_id, $plan_no, $products) {
        $database = ($wh_id == DEFAULT_WAREHOUSE ? 'td_planogram' : 'td_planogram/bandahara');
        $i = 1;
        $plan_detail_prefix = 'PT-'.date('ymd').'-'.date('His').'-';
        $this->m_global->delete($database, ['plan_no' => $plan_no]);
        foreach ($products as $value) {
            $data_prod['plan_det_no']       = $plan_detail_prefix.str_pad($i, 4, '0', STR_PAD_LEFT);
            $data_prod['plan_no']           = $plan_no;
            $data_prod['prod_no']           = $value->id;
            $data_prod['det_qty_satuan']    = $value->int_value;
            $data_prod['qty_satuan']        = $value->int_value;
            $data_prod['satuan']            = '1';
            $data_prod['konversi']          = '1';
            $data_prod['expired_date']      = $value->value;
            $result_prod                    = $this->m_global->insert($database, $data_prod);
            $i++;
        }
        echo response_builder(true, 201, null, 'Berhasil menambahkan data Planogram');
    }

    function list_planogram(){
        if($this->api_version == '1'){
            $wh_id          = $this->headers['Warehouse-Id'];
            $response       = [];
            $plan_history   = [];
            $rack_id        = $this->input->post('rack_id') ?: '';
            $type           = $this->input->post('type');
            $plan_id        = $this->input->post('plan_id') ?: '';
            $show_only_checked = $this->input->post('show_only_checked');
            $database       = ($wh_id == DEFAULT_WAREHOUSE ? 'td_planogram' : 'td_planogram/bandahara');
            $data           = $this->m_stock->list_planogram($this->api_version, $rack_id, $wh_id);

            if($type == 'history' && $plan_id != '') {
                $data_history   = $this->m_global->get_data_all($database, null, ['plan_no' => $plan_id]);
                foreach ($data_history as $value) {
                    $plan_history[$value->prod_no] = $value->expired_date;
                }
            }
            
            foreach ($data as $value) {
                $expired_date   = '';
                $is_check       = false;
                if(array_key_exists($value->prod_no, $plan_history)) {
                    $expired_date = $plan_history[$value->prod_no];
                    $is_check     = true;
                }
                if($show_only_checked == '1' && !$is_check) {} else {
                    $response[] = [
                        'product_id' => $value->prod_no,
                        'product_code' => $value->prod_code0,
                        'product_name' => $value->prod_name0,
                        'product_date' => $expired_date,
                        'product_is_check' => $is_check,
                        'product_prices' => [['uom' => $value->prod_uom]],
                        'product_quantities' => [[
                            'title' => 'regular',
                            'value' => $value->QtyPersediaan
                        ]]
                    ];
                }
            }
            echo response_builder(true, 200, $response);
        }else{
            echo response_builder(false, 900);
        }
    }

    function list_planogram_history(){
        if($this->api_version == '1'){
            $response       = [];
            $search         = $this->input->post('search');
            $page           = $this->input->post('page') ?: '0';
            $date           = $this->input->post('date') ?: date('Y-m-d');
            $warehouse      = $this->input->post('warehouse') ?: DEFAULT_WAREHOUSE;
            $limit          = 50;
            $offset         = $limit*$page;
            $data           = $this->m_stock->list_planogram_history($this->api_version, $warehouse, $search, $date, $limit, $offset);
            foreach ($data as $value) {
                $response[] = [
                    'id' => $value->No_Planogram,
                    'title' => $value->Rak_Name,
                    'value' => $value->rak_id,
                    'subtitle' => $value->Tgl_Plan,
                    'description' => 'User: '.$value->user_create.PHP_EOL.'Note: '.$value->Keterangan
                ];
            }
            echo response_builder(true, 200, $response);
        } else if($this->api_version == '2'){
            $response       = [];
            $search         = $this->input->post('search');
            $page           = $this->input->post('page') ?: '0';
            $date           = $this->input->post('date') ?: date('Y-m-d');
            $date_type      = $this->input->post('date_type');
            $show_all       = $this->input->post('show_all');
            $rack_id        = $this->input->post('rack_id');
            $warehouse      = $this->input->post('warehouse') ?: DEFAULT_WAREHOUSE;
            $user_id        = ($show_all == '1' ? '' : $this->headers['User-Id']);
            $limit          = 1000;
            $offset         = $limit*$page;
            if($date_type == 'exp_date') {
                $plan_data  = [];
                $data       = $this->m_stock->list_planogram_history_by_expired_date('1', $warehouse, $user_id, $rack_id, $date, $limit, $offset);
                foreach ($data as $value) {
                    $plan_data[$value->No_Planogram]['order_code'] = $value->No_Planogram;
                    $plan_data[$value->No_Planogram]['rack_id'] = $value->rak_id;
                    $plan_data[$value->No_Planogram]['date'] = $value->Tgl_Plan;
                    $plan_data[$value->No_Planogram]['description'] = 'Rak   : '.$value->Rak_Name.'  User: '.$value->user_create.PHP_EOL.'Note : '.$value->Keterangan;
                    $plan_data[$value->No_Planogram]['products'][] = [
                        'product_id' => $value->prod_no,
                        'product_code' => $value->prod_code0,
                        'product_name' => $value->prod_name0,
                        'product_date' => $value->expired_date
                    ];
                }
                foreach ($plan_data as $value) {
                    $response[] = $value;
                }
            } else {
                $data       = $this->m_stock->list_planogram_history('1', $warehouse, $user_id, $rack_id, $search, $date, $limit, $offset);
                foreach ($data as $value) {
                    $response[] = [
                        'order_code' => $value->No_Planogram,
                        'rack_id' => $value->rak_id,
                        'date' => $value->Tgl_Plan,
                        'description' => 'Rak   : '.$value->Rak_Name.'  User: '.$value->user_create.PHP_EOL.'Note : '.$value->Keterangan
                    ];
                }
            }
            echo response_builder(true, 200, $response);
        } else {
            echo response_builder(false, 900);
        }
    }

    function list_stock_opname(){
        if($this->api_version == '1'){
            $response           = [];
            $method             = $this->input->post('method') ?: '';
            $type               = $this->input->post('type') ?: '';
            $date               = $this->input->post('date') ?: '';
            $category_id        = $this->input->post('category_id') ?: '';
            $variant_id         = $this->input->post('variant_id') ?: '';
            $subvariant_id      = $this->input->post('subvariant_id') ?: '';
            $brand_id           = $this->input->post('brand_id') ?: '';
            $rack_id            = $this->input->post('rack_id') ?: '';
            $rack_warehouse_id  = $this->input->post('rack_warehouse_id') ?: '';
            $warehouse          = $this->input->post('warehouse') ?: DEFAULT_WAREHOUSE;
            $product_name       = $this->input->post('product_name') ?: '';

            if($type == 'history' || $type == 'report') {
                $data           = $this->m_stock->list_stock_opname_detail($this->api_version, $warehouse, $date, $rack_id);
                foreach ($data as $value) {
                    $response[] = [
                        'product_id' => $value->prod_no,
                        'product_code' => $value->prod_code0,
                        'product_name' => $value->prod_name0,
                        'product_prices' => [['uom' => $value->prod_uom]],
                        'product_quantities' => [
                            [
                                'title' => 'regular',
                                'value' => (Int) $value->QtyData
                            ],
                            [
                                'title' => 'csw',
                                'value' => (Int) $value->QtyCSW
                            ],
                            [
                                'title' => 'actual',
                                'value' => (Int) $value->QtyFisik
                            ]
                        ]
                    ];
                }
            } else {
                $data           = $this->m_stock->list_stock_opname($this->api_version, $warehouse, $date, $category_id, $variant_id, $subvariant_id, $brand_id, $rack_id, $rack_warehouse_id, $product_name);
                if($method == 'FILTER') {
                    foreach ($data as $value) {
                        $response[] = [
                            'product_id' => $value->prod_no,
                            'product_code' => $value->prod_code0,
                            'product_name' => $value->prod_name0,
                            'product_prices' => [['uom' => $value->prod_uom]],
                            'product_quantities' => [
                                [
                                    'title' => 'regular',
                                    'value' => (Int) $value->QtyData
                                ],
                                [
                                    'title' => 'csw',
                                    'value' => (Int) $value->QtyCSW
                                ],
                                [
                                    'title' => 'actual',
                                    'value' => 0
                                ]
                            ]
                        ];
                    }
                }
            }

            echo response_builder(true, 200, $response);
        }else{
            echo response_builder(false, 900);
        }
    }

    function get_single_product_stock_opname(){
        if($this->api_version == '1'){
            $response       = [];
            $product_code   = $this->input->post('product_code') ?: '';
            $warehouse      = $this->input->post('warehouse') ?: DEFAULT_WAREHOUSE;
            $date           = $this->input->post('date') ?: '';

            if($product_code == '') {
                echo response_builder(false, 406, null, 'Kode barang tidak ditemukan');
                exit;
            }

            $data           = $this->m_stock->list_stock_opname($this->api_version, $warehouse, $date, '', '', '', '', '', '', '', $product_code);
            if(count($data) == 0) {
                echo response_builder(false, 406, null, 'Kode barang tidak ditemukan');
                exit;
            }
            $value = $data[0];
            $response[] = [
                'product_id' => $value->prod_no,
                'product_code' => $value->prod_code0,
                'product_name' => $value->prod_name0,
                'product_prices' => [['uom' => $value->prod_uom]],
                'product_quantities' => [
                    [
                        'title' => 'regular',
                        'value' => (Int) $value->QtyData
                    ],
                    [
                        'title' => 'csw',
                        'value' => (Int) $value->QtyCSW
                    ],
                    [
                        'title' => 'actual',
                        'value' => ''
                    ]
                ]
            ];
            echo response_builder(true, 200, $response);
        }else{
            echo response_builder(false, 900);
        }
    }

    function submit_stock_opname(){
        if($this->api_version == '1'){
            $rack_id        = $this->input->post('id');
            $cab_no         = $this->input->post('cab_no');
            $warehouse      = $this->input->post('warehouse') ?: DEFAULT_WAREHOUSE;
            $products       = base64_decode($this->input->post('products'));
            $products       = json_decode($products, true);

            if($warehouse == DEFAULT_WAREHOUSE) {
                $order_code_prefix  = 'PT-SOP'.date('ymd');
            } else {
                $order_code_prefix  = 'CB-SOP'.date('ymd');
            }
            $where_e            = "out_no LIKE '%".$order_code_prefix."%'";
            $check_queue        = $this->m_global->get_data_all('tout_mutasi/mobile', null, null, 'out_no', $where_e, ['out_no', 'DESC'], 0, 1);
            $queue              = 1;
            if(count($check_queue) > 0) {
                $queue = str_replace($order_code_prefix, '', $check_queue[0]->out_no);
                $queue = ((Int) $queue) + 1;
            }

            $order_code = $order_code_prefix.str_pad($queue, 5, '0', STR_PAD_LEFT);

            $check_exist = $this->m_global->get_data_all('tout_mutasi/mobile', null, ['out_no' => $order_code]);
            if(count($check_exist) > 0) {
                $this->m_global->delete('tdetail_out_mutasi/mobile', ['out_no' => $order_code]);
                $this->m_global->delete('tout_mutasi/mobile', ['out_no' => $order_code]);
            }

            $data['iUpload']        = '1';
            $data['out_no']         = $order_code;
            $data['out_date']       = date('Y-m-d H:i:s');
            $data['out_type']       = '17';
            $data['gud_no1']        = $warehouse;
            $data['rak_id']         = ( $rack_id == '' ? '0' : $rack_id );
            $data['is_delete']      = '0';
            $data['user_id']        = $this->headers['User-Id'];
            $data['create_date']    = date('Y-m-d H:i:s');
            $data['user_edit']      = $this->headers['User-Id'];
            $data['edit_date']      = date('Y-m-d H:i:s');
            $data['user_delete']    = '0';
            $data['cab_no']         = $cab_no;
            $result                 = $this->m_global->insert('tout_mutasi/mobile', $data);

            if($result['status']) {
                $this->submit_detail_stock_opname($order_code, $warehouse, $products);
            } else {
                echo response_builder(false, 406, null, 'Gagal menambah data Stock Opname');
            }
        }else if($this->api_version == '2'){
            $rack_id        = $this->input->post('id');
            $cab_no         = $this->input->post('cab_no');
            $note           = $this->input->post('note');
            $checked_by     = $this->input->post('checked_by');
            $warehouse      = $this->input->post('warehouse') ?: DEFAULT_WAREHOUSE;
            $database       = ($warehouse == DEFAULT_WAREHOUSE ? 'tadjust' : 'tadjust/bandahara');
            $products       = base64_decode($this->input->post('products'));
            $products       = json_decode($products, true);

            if($warehouse == DEFAULT_WAREHOUSE) {
                $order_code_prefix  = 'PT-SOP'.date('ymd');
            } else {
                $order_code_prefix  = 'CB-SOP'.date('ymd');
            }
            $where_e            = "adj_no LIKE '%".$order_code_prefix."%'";
            $check_queue        = $this->m_global->get_data_all($database, null, null, 'adj_no', $where_e, ['adj_no', 'DESC'], 0, 1);
            $queue              = 1;
            if(count($check_queue) > 0) {
                $queue = str_replace($order_code_prefix, '', $check_queue[0]->adj_no);
                $queue = ((Int) $queue) + 1;
            }

            $order_code = $order_code_prefix.str_pad($queue, 5, '0', STR_PAD_LEFT);

            $check_exist = $this->m_global->get_data_all($database, null, ['adj_no' => $order_code]);
            if(count($check_exist) > 0) {
                $this->m_global->delete(($warehouse == DEFAULT_WAREHOUSE ? 'td_adjust' : 'td_adjust/bandahara'), ['adj_no' => $order_code]);
                $this->m_global->delete($database, ['adj_no' => $order_code]);
            }

            $data['iUpload']        = '0';
            $data['adj_no']         = $order_code;
            $data['adj_date']       = date('Y-m-d H:i:s');
            $data['adj_type']       = '17';
            $data['adj_desc']       = $note;
            $data['adj_terima']     = $checked_by;
            $data['gud_no']         = $warehouse;
            $data['is_delete']      = '0';
            $data['user_id']        = $this->headers['User-Id'];
            $data['create_date']    = date('Y-m-d H:i:s');
            $data['user_edit']      = $this->headers['User-Id'];
            $data['edit_date']      = date('Y-m-d H:i:s');
            $data['cab_no']         = $cab_no;
            $result                 = $this->m_global->insert($database, $data);

            if($result['status']) {
                $this->submit_detail_stock_opname($order_code, $warehouse, $products);
                $this->m_global->update($database, ['iUpload' => '1'], ['adj_no' => $order_code]);
            } else {
                echo response_builder(false, 406, null, 'Gagal menambah data Stock Opname');
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    function submit_detail_stock_opname($order_code, $warehouse, $products) {
        $database = ($warehouse == DEFAULT_WAREHOUSE ? 'td_adjust' : 'td_adjust/bandahara');
        $i = 1;
        if($warehouse == DEFAULT_WAREHOUSE) {
            $plan_detail_prefix = 'PT-'.date('ymd').'-'.date('His').'-';
        } else {
            $plan_detail_prefix = 'CB-'.date('ymd').'-'.date('His').'-';
        }
        foreach ($products as $value) {
            if($value['value'] != '') {
                $data_prod['adj_det_no']    = $plan_detail_prefix.str_pad($i, 4, '0', STR_PAD_LEFT);
                $data_prod['adj_no']        = $order_code;
                $data_prod['prod_no']       = $value['id'];
                $data_prod['gud_no']        = $warehouse;
                $data_prod['qty_satuan']    = $value['value'];
                $data_prod['satuan']        = '1';
                $data_prod['konversi']      = '1'; 

                $result_prod                = $this->m_global->insert($database, $data_prod);
                $i++;
            }
        }
        echo response_builder(true, 201, null, 'Berhasil menambahkan data Stock Opname');
    }

    function submit_stock_opname_csw(){
        if($this->api_version == '1'){
            $rack_id        = $this->input->post('id');
            $cab_no         = $this->input->post('cab_no');
            $note           = $this->input->post('note');
            $checked_by     = $this->input->post('checked_by');
            $warehouse      = $this->input->post('warehouse') ?: DEFAULT_WAREHOUSE;
            $database       = 'tadjust_wh_mutasi/mobile';
            $products       = base64_decode($this->input->post('products'));
            $products       = json_decode($products, true);

            if($warehouse == DEFAULT_WAREHOUSE) {
                $order_code_prefix  = 'PT-SOP'.date('ymd');
            } else {
                $order_code_prefix  = 'CB-SOP'.date('ymd');
            }
            $where_e            = "adj_no LIKE '%".$order_code_prefix."%'";
            $check_queue        = $this->m_global->get_data_all($database, null, null, 'adj_no', $where_e, ['adj_no', 'DESC'], 0, 1);
            $queue              = 1;
            if(count($check_queue) > 0) {
                $queue = str_replace($order_code_prefix, '', $check_queue[0]->adj_no);
                $queue = ((Int) $queue) + 1;
            }

            $order_code = $order_code_prefix.str_pad($queue, 5, '0', STR_PAD_LEFT);

            $check_exist = $this->m_global->get_data_all($database, null, ['adj_no' => $order_code]);
            if(count($check_exist) > 0) {
                $this->m_global->delete('td_adjust_wh_mutasi/mobile', ['adj_no' => $order_code]);
                $this->m_global->delete($database, ['adj_no' => $order_code]);
            }

            $data['iUpload']        = '0';
            $data['adj_no']         = $order_code;
            $data['adj_date']       = date('Y-m-d H:i:s');
            $data['adj_type']       = '17';
            $data['adj_terima']     = $checked_by;
            $data['adj_desc']       = $note;
            $data['gud_no']         = $warehouse;
            $data['is_delete']      = '0';
            $data['user_id']        = $this->headers['User-Id'];
            $data['create_date']    = date('Y-m-d H:i:s');
            $data['user_edit']      = $this->headers['User-Id'];
            $data['edit_date']      = date('Y-m-d H:i:s');
            $data['cab_no']         = $cab_no;
            $result                 = $this->m_global->insert($database, $data);

            if($result['status']) {
                $this->submit_detail_stock_opname_csw($order_code, $warehouse, $products);
                $this->m_global->update($database, ['iUpload' => '1'], ['adj_no' => $order_code]);
            } else {
                echo response_builder(false, 406, null, 'Gagal menambah data Stock Opname CSW');
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    function submit_detail_stock_opname_csw($order_code, $warehouse, $products) {
        $database = 'td_adjust_wh_mutasi/mobile';
        $i = 1;
        if($warehouse == DEFAULT_WAREHOUSE) {
            $plan_detail_prefix = 'PT-'.date('ymd').'-'.date('His').'-';
        } else {
            $plan_detail_prefix = 'CB-'.date('ymd').'-'.date('His').'-';
        }
        foreach ($products as $value) {
            if($value['value'] != '') {
                $data_prod['adj_det_no']    = $plan_detail_prefix.str_pad($i, 4, '0', STR_PAD_LEFT);
                $data_prod['adj_no']        = $order_code;
                $data_prod['prod_no']       = $value['id'];
                $data_prod['gud_no']        = $warehouse;
                if ($value['value'] != '0') {
                    $data_prod['qty_satuan'] = $value['value'];
                }
                if ($value['value2'] != '0') {
                    $data_prod['qty_satuan'] = $value['value2'];
                }
                $data_prod['det_qty_in']    = $value['value'];
                $data_prod['det_qty_out']   = $value['value2'];
                $data_prod['satuan']        = '1';
                $data_prod['konversi']      = '1'; 

                $result_prod                = $this->m_global->insert($database, $data_prod);
                $i++;
            }
        }
        echo response_builder(true, 201, null, 'Berhasil menambahkan data Stock Opname CSW');
    }

    function list_stock_opname_history(){
        if($this->api_version == '1'){
            $response       = [];
            $search         = $this->input->post('search');
            $page           = $this->input->post('page') ?: '0';
            $rack_id        = $this->input->post('rack_id');
            $brand_id       = $this->input->post('brand_id');
            $flag           = $this->input->post('flag') ?: 'reg';
            $date           = $this->input->post('date') ?: date('Y-m-d');
            $warehouse      = $this->input->post('warehouse') ?: BANDAHARA_WAREHOUSE;
            $limit          = 50;
            $offset         = $limit*$page;
            $data           = $this->m_stock->list_stock_opname_history($this->api_version, $flag, $warehouse, $rack_id, $brand_id, $search, $date, $limit, $offset);
            foreach ($data as $value) {
                $response[] = [
                    'id' => $value->No_SO,
                    'subtitle' => $value->Tgl_SO,
                    'payload_id' => $warehouse,
                    'payload_title' => ($warehouse == DEFAULT_WAREHOUSE ? "Gudang Madura" : "Toko Bandahara"),
                    'description' => $value->Keterangan
                ];
            }
            echo response_builder(true, 200, $response);
        }else{
            echo response_builder(false, 900);
        }
    }

    function item_request_history(){
        if($this->api_version == '1'){
            $response       = [];
            $product_data   = [];
            $search         = $this->input->post('search');
            $is_selection   = $this->input->post('is_selection');
            $page           = $this->input->post('page') ?: '0';
            $date           = $this->input->post('date') ?: '';
            $warehouse      = $this->input->post('warehouse') ?: BANDAHARA_WAREHOUSE;
            $limit          = 1000;
            $offset         = $limit*$page;

            if($search == '' && $date == '') {
                $date = date('Y-m-d');
            }

            $data           = $this->m_stock->item_request_history($this->api_version, $warehouse, $search, $date, $limit, $offset, ($is_selection == '1' ? true : false));
            foreach ($data as $value) {
                $product_data[$value->req_no]['date'] = $value->req_tgl; 
                $product_data[$value->req_no]['desc'] = $value->req_desc; 
                $product_data[$value->req_no]['products'][] = [
                    'product_code' => $value->prod_code0,
                    'product_name' => $value->prod_name0,
                    'product_prices' => [['uom' => $value->nm_satuan]],
                    'product_quantities' => [['value' => (String) $value->qty_satuan]]
                ];
            }
            foreach ($product_data as $key => $value) {
                $response[] = [
                    'order_code' => $key,
                    'date' => $value['date'],
                    'description' => $value['desc'],
                    'products' => $value['products']
                ];
            }
            echo response_builder(true, 200, $response);
        }else{
            echo response_builder(false, 900);
        }
    }

    function item_request(){
        if($this->api_version == '1'){
            $response       = [];
            $search         = $this->input->post('search');
            $page           = $this->input->post('page') ?: '0';
            $date           = $this->input->post('date') ?: date('Y-m-d');
            $status         = $this->input->post('status') ?: '0';
            $status_comparison = $this->input->post('status_comparison') ?: '=';
            $warehouse      = $this->input->post('warehouse') ?: BANDAHARA_WAREHOUSE;
            $limit          = 50;
            $offset         = $limit*$page;
            $data           = $this->m_stock->item_request($this->api_version, $warehouse, $search, $date, $status, $status_comparison, $limit, $offset);
            foreach ($data as $value) {
                $response[] = [
                    'order_code' => $value->req_no,
                    'date' => $value->req_tgl,
                    'status' => $value->Status,
                    'is_highlighted' => ($value->Status == 'REQUEST' ? true : false),
                    'description' => $value->req_desc,
                    'user' => $value->user_create
                ];
            }
            echo response_builder(true, 200, $response);
        }else{
            echo response_builder(false, 900);
        }
    }

    function item_request_detail(){
        if($this->api_version == '1'){
            $response       = [];
            $order_code     = $this->input->post('order_code');

            if($order_code == '') {
                $order_code_prefix      = 'CB-MREQ'.date('ymd');
                $response['order_code'] = $order_code_prefix.UNDEFINED_QUEUE;
                $response['status']     = "on_process";
                $response['date']       = date('Y-m-d H:i:s');
            } else {
                $detail = $this->m_global->get_data_all('ttransfer_req/bandahara', null, ['req_no' => $order_code]);
                if(count($detail) == 0) {
                    echo response_builder(false, 406, null, 'Kode request tidak ditemukan');
                    exit;
                }
                $detail = $detail[0];

                $response['order_code']     = $order_code;
                $response['date']           = $detail->req_tgl;
                $response['description']    = $detail->req_desc;

                $join_prod      = [['table' => 'tproduct tp', 'on' => 'tp.prod_no=ttr.prod_no']];
                $select_prod    = '*,'.$this->basic_product_select;
                $products       = $this->m_global->get_data_all('td_transfer_req ttr/bandahara', $join_prod, ['ttr.req_no' => $order_code], $select_prod, null, ['det_req_no', 'ASC']);
                foreach ($products as $value) {
                    $response['products'][] = [
                        'product_id' => $value->prod_no,
                        'product_name' => $value->prod_name0,
                        'product_code' => $value->prod_code0,
                        'product_note' => $value->det_req_desc,
                        'product_uom_input' => $value->satuan,
                        'product_qty_input' => $value->qty_satuan,
                        'product_prices' => $this->product_prices($value),
                    ];
                }
            }
            $response['wh_request'] = [
                'id' => "ID2",
                'title' => "GUDANG MADURA"
            ];
            $response['wh_receive'] = [
                'id' => "ID-190121-105451-0003",
                'title' => "TOKO BANDAHARA"
            ];

            echo response_builder(true, 200, $response);
        }else{
            echo response_builder(false, 900);
        }
    }

    function item_request_export() {
        if($this->api_version == '1'){
            $response       = [];
            $order_code     = $this->input->post('order_code');

            $detail = $this->m_global->get_data_all('ttransfer_req/bandahara', null, ['req_no' => $order_code]);
            if(count($detail) == 0) {
                echo response_builder(false, 406, null, 'Kode request tidak ditemukan');
                exit;
            }
            $detail = $detail[0];

            $response['order_code']     = $order_code;
            $response['date']           = $detail->req_tgl;
            $response['description']    = $detail->req_desc;

            $join_prod      = [['table' => 'tproduct tp', 'on' => 'tp.prod_no=ttr.prod_no']];
            $select_prod    = '*,'.$this->basic_product_select;
            $products       = $this->m_global->get_data_all('td_transfer_req ttr/bandahara', $join_prod, ['ttr.req_no' => $order_code], $select_prod);
            foreach ($products as $value) {
                $response['products'][] = [
                    'product_id' => $value->prod_no,
                    'product_name' => $value->prod_name0,
                    'product_code' => $value->prod_code0,
                    'product_note' => $value->det_req_desc,
                    'product_uom_input' => $value->satuan,
                    'product_qty_input' => $value->qty_satuan,
                    'product_prices' => $this->product_prices($value),
                ];
            }
            $response['wh_request'] = [
                'id' => "ID2",
                'title' => "MADURA"
            ];
            $response['wh_receive'] = [
                'id' => "ID-190121-105451-0003",
                'title' => "BANDAHARA"
            ];

            $this->load->library('ItemRequestReportPDF');
            $pdf = new ItemRequestReportPDF("L","mm", [210, 148]);
            $pdf->AddPage();
            $pdf->SetAutoPageBreak(true);
            $pdf->header_report();
            $pdf->body($response, $this->headers['Username']);

            $namefile = "item_request_".$order_code."_report_".date('Ymdhis').".pdf";
            $pdf->Output("./assets/pdf/".$namefile, "F");

            echo response_builder(true, 200, ['title' => $namefile]);
        }else{
            echo response_builder(false, 900);
        }
    }

    function submit_item_request(){
        if($this->api_version == '1'){
            $order_code     = $this->input->post('order_code');
            $date           = $this->input->post('date');
            $note           = $this->input->post('note');
            $products       = base64_decode($this->input->post('products'));
            $products       = json_decode($products, true);

            if(strpos($order_code, UNDEFINED_QUEUE)) {
                $prefix     = str_replace(UNDEFINED_QUEUE, '', $order_code);
                $order_code = $prefix.get_item_request_queue($prefix);
            }

            $check_exist = $this->m_global->get_data_all('ttransfer_req/bandahara', null, ['req_no' => $order_code]);
            if(count($check_exist) > 0) {
                $this->m_global->delete('ttransfer_req/bandahara', ['req_no' => $order_code]);
            }

            $data['iUpload']        = '0';
            $data['req_no']         = $order_code;
            $data['req_tgl']        = $date;
            $data['gud_no1']        = DEFAULT_WAREHOUSE;
            $data['gud_no2']        = BANDAHARA_WAREHOUSE;
            $data['req_desc']       = $note;
            $data['user_id']        = $this->headers['User-Id'];
            $data['create_date']    = date('Y-m-d H:i:s');
            $data['user_edit']      = $this->headers['User-Id'];
            $data['edit_date']      = date('Y-m-d H:i:s');
            $data['is_delete']      = '0';
            $data['is_android']     = '1';
            $data['nopol']          = '';
            $data['nm_supir']       = '';
            $data['nm_setuju']      = '';
            $data['nm_terima']      = '';
            $data['req_no_reff']    = '';
            $data['no_reff2']       = '';
            $data['req_type']       = '0';
            $result                 = $this->m_global->insert('ttransfer_req/bandahara', $data);

            if($result['status']) {
                $this->submit_detail_item_request($order_code, $products);
                $this->m_global->update('ttransfer_req/bandahara', ['iUpload' => '1'], ['req_no' => $order_code]);
            } else {
                echo response_builder(false, 406, null, 'Gagal menambah data Request');
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    function submit_detail_item_request($order_code, $products) {
        $i = 1;
        $plan_detail_prefix = 'CB-'.date('ymd').'-'.date('His').'-';
        $this->m_global->delete('td_transfer_req/bandahara', ['req_no' => $order_code]);
        foreach ($products as $value) {
            $uom                        = $value['product_uom_input'];
            $data_prod['det_req_no']    = $plan_detail_prefix.str_pad($i, 4, '0', STR_PAD_LEFT);
            $data_prod['req_no']        = $order_code;
            $data_prod['prod_no']       = $value['product_id'];
            $data_prod['gud_no1']       = DEFAULT_WAREHOUSE;
            $data_prod['gud_no2']       = BANDAHARA_WAREHOUSE;
            $data_prod['qty_satuan']    = $value['product_qty_input'];
            $data_prod['satuan']        = $uom;
            $data_prod['konversi']      = $value['product_prices'][$uom-1]['uom_qty'];
            $data_prod['det_req_desc']  = $value['product_note'];
            // $data_prod['qty_transfer']  = $data_prod['qty_satuan'] * $data_prod['konversi'];   
            $data_prod['qty_transfer']  = '0';   

            $result_prod                = $this->m_global->insert('td_transfer_req/bandahara', $data_prod);
            $i++;
        }
        echo response_builder(true, 201, null, 'Berhasil menambahkan data Request');
    }

    function mutation(){
        if($this->api_version == '1'){
            $response       = [];
            $search         = $this->input->post('search');
            $page           = $this->input->post('page') ?: '0';
            $date           = $this->input->post('date') ?: date('Y-m-d');
            $warehouse      = $this->input->post('warehouse') ?: DEFAULT_WAREHOUSE;
            $limit          = 50;
            $offset         = $limit*$page;
            $data           = $this->m_stock->mutation($this->api_version, $warehouse, $search, $date, $limit, $offset);
            foreach ($data as $value) {
                $response[] = [
                    'parent_code' => $value->No_Request,
                    'order_code' => $value->No_Mutasi,
                    'date' => $value->Tgl_Mutasi,
                    'description' => $value->Keterangan,
                    'user' => $value->user_create
                ];
            }
            echo response_builder(true, 200, $response);
        }else{
            echo response_builder(false, 900);
        }
    }

    function mutation_detail(){
        if($this->api_version == '1'){
            $response       = [];
            $req_code       = $this->input->post('request_code');
            $order_code     = $this->input->post('mutation_code');
            $wh_id          = $this->headers['Warehouse-Id'];

            $response['parent_code'] = $req_code;
            if($order_code == '') {
                $order_code_prefix      = 'CB-MMS'.date('ymd');
                $response['order_code'] = $order_code_prefix.UNDEFINED_QUEUE;
                $response['status']     = "on_process";
                $response['date']       = date('Y-m-d H:i:s');

                $join_prod      = [['table' => 'tproduct tp', 'on' => 'tp.prod_no=ttr.prod_no']];
                $select_prod    = '*,'.$this->basic_product_select;
                $products       = $this->m_global->get_data_all(($wh_id == DEFAULT_WAREHOUSE ? 'td_transfer_req ttr' : 'td_transfer_req ttr/bandahara'), $join_prod, ['ttr.req_no' => $req_code], $select_prod, null, ['det_req_no', 'ASC']);
                foreach ($products as $value) {
                    $whpp       = "tgl < '".$response['date']."'";
                    $get_hpp    = $this->m_global->get_data_all('ttrans_hpp', null, ['prod_no' => $value->prod_no], 'last_prod_rata_price', $whpp, ['tgl', 'DESC'], 0, 1);
                    if(count($get_hpp) > 0) {
                        $hpp    = $get_hpp[0]->last_prod_rata_price;
                    } else {
                        $hpp    = 0;
                    }
                    $response['products'][] = [
                        'product_id' => $value->prod_no,
                        'product_name' => $value->prod_name0,
                        'product_code' => $value->prod_code0,
                        'product_note' => $value->det_req_desc,
                        'product_uom_input' => $value->satuan,
                        'product_qty_input' => $value->qty_satuan,
                        'product_qty_input2' => $value->qty_satuan,
                        'product_prices' => $this->product_prices($value),
                        'product_hpp' => (double) $hpp,
                        'product_hpp_total' => (double) ($value->qty_satuan * $hpp)
                    ];
                }
            } else {
                $detail = $this->m_global->get_data_all('tout', null, ['out_no' => $order_code]);
                if(count($detail) == 0) {
                    echo response_builder(false, 406, null, 'Kode mutasi tidak ditemukan');
                    exit;
                }
                $detail = $detail[0];

                $response['order_code']     = $order_code;
                $response['date']           = $detail->out_date;
                $response['description']    = $detail->out_desc;
                $response['vehicle_no']     = $detail->no_pol;
                $response['packer_name']    = $detail->nm_supir;
                $response['checker_name']   = $detail->disetujui;
                $response['receiver_name']  = $detail->diterima;
                $response['status']         = "on_process";

                $join_prod      = [['table' => 'tproduct tp', 'on' => 'tp.prod_no=ttr.prod_no']];
                $select_prod    = '*,'.$this->basic_product_select;
                $products       = $this->m_global->get_data_all(($wh_id == DEFAULT_WAREHOUSE ? 'tdetail_out ttr' : 'tdetail_out ttr/bandahara'), $join_prod, ['ttr.out_no' => $order_code], $select_prod, null, ['out_det_no', 'ASC']);
                foreach ($products as $value) {
                    $whpp       = "tgl < '".$response['date']."'";
                    $get_hpp    = $this->m_global->get_data_all('ttrans_hpp', null, ['prod_no' => $value->prod_no], 'last_prod_rata_price', $whpp, ['tgl', 'DESC'], 0, 1);
                    if(count($get_hpp) > 0) {
                        $hpp    = $get_hpp[0]->last_prod_rata_price;
                    } else {
                        $hpp    = 0;
                    }
                    $response['products'][] = [
                        'product_id' => $value->prod_no,
                        'product_name' => $value->prod_name0,
                        'product_code' => $value->prod_code0,
                        'product_note' => $value->out_det_desc,
                        'product_uom_input' => $value->satuan,
                        'product_qty_input' => $value->qty_satuan,
                        'product_qty_input2' => $value->qty_satuan,
                        'product_prices' => $this->product_prices($value),
                        'product_hpp' => (double) $hpp,
                        'product_hpp_total' => (double) ($value->qty_satuan * $hpp)
                    ];
                }
            }

            $response['wh_request'] = [
                'id' => "ID2",
                'title' => "GUDANG MADURA"
            ];
            $response['wh_receive'] = [
                'id' => "ID-190121-105451-0003",
                'title' => "TOKO BANDAHARA"
            ];

            echo response_builder(true, 200, $response);
        }else{
            echo response_builder(false, 900);
        }
    }

    function mutation_detail_export() {
        if($this->api_version == '1'){
            $response       = [];
            $req_code       = $this->input->post('request_code');
            $order_code     = $this->input->post('mutation_code');

            $response['parent_code'] = $req_code;
            $detail = $this->m_global->get_data_all('tout', null, ['out_no' => $order_code]);
            if(count($detail) == 0) {
                echo response_builder(false, 406, null, 'Kode mutasi tidak ditemukan');
                exit;
            }
            $detail = $detail[0];

            $response['order_code']     = $order_code;
            $response['date']           = $detail->out_date;
            $response['description']    = $detail->out_desc;
            $response['vehicle_no']     = $detail->no_pol;
            $response['packer_name']    = $detail->nm_supir;
            $response['checker_name']   = $detail->disetujui;
            $response['receiver_name']  = $detail->diterima;
            $response['status']         = "on_process";

            $join_prod      = [['table' => 'tproduct tp', 'on' => 'tp.prod_no=ttr.prod_no']];
            $select_prod    = '*,'.$this->basic_product_select;
            $products       = $this->m_global->get_data_all('tdetail_out ttr', $join_prod, ['ttr.out_no' => $order_code], $select_prod, null, ['out_det_no', 'ASC']);
            foreach ($products as $value) {
                $whpp       = "tgl < '".$response['date']."'";
                $get_hpp    = $this->m_global->get_data_all('ttrans_hpp', null, ['prod_no' => $value->prod_no], 'last_prod_rata_price', $whpp, ['tgl', 'DESC'], 0, 1);
                if(count($get_hpp) > 0) {
                    $hpp    = $get_hpp[0]->last_prod_rata_price;
                } else {
                    $hpp    = 0;
                }
                $response['products'][] = [
                    'product_id' => $value->prod_no,
                    'product_name' => $value->prod_name0,
                    'product_code' => $value->prod_code0,
                    'product_note' => $value->out_det_desc,
                    'product_uom_input' => $value->satuan,
                    'product_qty_input' => $value->qty_satuan,
                    'product_prices' => $this->product_prices($value),
                    'product_hpp' => (double) $hpp,
                    'product_hpp_total' => (double) ($value->qty_satuan * $hpp)
                ];
            }
            $response['wh_request'] = [
                'id' => "ID2",
                'title' => "GUDANG"
            ];
            $response['wh_receive'] = [
                'id' => "ID-190121-105451-0003",
                'title' => "TOKO"
            ];

            $this->load->library('ItemMutationReportPDF');
            $pdf = new ItemMutationReportPDF("L","mm", [210, 148]);
            $pdf->AddPage();
            $pdf->SetAutoPageBreak(true);
            $pdf->header_report();
            $pdf->body($response, $this->headers['Username']);

            $namefile = "item_mutation_".$order_code."_report_".date('Ymdhis').".pdf";
            $pdf->Output("./assets/pdf/".$namefile, "F");

            echo response_builder(true, 200, ['title' => $namefile]);
        }else{
            echo response_builder(false, 900);
        }
    }

    function submit_mutation(){
        if($this->api_version == '1'){
            $wh_id          = $this->headers['Warehouse-Id'];
            $req_code       = $this->input->post('request_code');
            $order_code     = $this->input->post('mutation_code');
            $date           = $this->input->post('date');
            $note           = $this->input->post('note');
            $vehicle_no     = $this->input->post('vehicle_no');
            $packer_name    = $this->input->post('packer_name');
            $checker_name   = $this->input->post('checker_name');
            $receiver_name  = $this->input->post('receiver_name');
            $cab_no         = $this->input->post('cab_no');
            $products       = base64_decode($this->input->post('products'));
            $products       = json_decode($products, true);

            if($req_code == '' || $req_code == null) {
                echo response_builder(false, 406, null, 'Kode request tidak boleh kosong');
                exit;
            }

            $this->mutation_check_stock($products, $wh_id, $date);

            if(strpos($order_code, UNDEFINED_QUEUE)) {
                $prefix     = str_replace(UNDEFINED_QUEUE, '', $order_code);
                $order_code = $prefix.get_mutation_queue($prefix);
            }

            $check_exist = $this->m_global->get_data_all('tout_mutasi/mobile', null, ['out_no' => $order_code]);
            if(count($check_exist) > 0) {
                $this->m_global->delete('tdetail_out_mutasi/mobile', ['out_no' => $order_code]);
                $this->m_global->delete('tout_mutasi/mobile', ['out_no' => $order_code]);
            }

            $data['iUpload']        = '0';
            $data['out_no']         = $order_code;
            $data['out_date']       = $date;
            $data['out_type']       = '11';
            $data['gud_no1']        = DEFAULT_WAREHOUSE;
            $data['gud_no2']        = BANDAHARA_WAREHOUSE;
            $data['out_desc']       = $note;
            $data['no_pol']         = $vehicle_no;
            $data['nm_supir']       = $packer_name;
            $data['diterima']       = $receiver_name;
            $data['disetujui']      = $checker_name;
            $data['req_no']         = $req_code;
            $data['is_delete']      = '0';
            $data['user_id']        = $this->headers['User-Id'];
            $data['create_date']    = date('Y-m-d H:i:s');
            $data['user_edit']      = $this->headers['User-Id'];
            $data['edit_date']      = date('Y-m-d H:i:s');
            $data['user_delete']    = '0';
            $data['cab_no']         = $cab_no;
            $result                 = $this->m_global->insert('tout_mutasi/mobile', $data);

            if($result['status']) {
                $this->submit_detail_mutation($order_code, $req_code, $products);
                $this->m_global->update('tout_mutasi/mobile', ['iUpload' => '1'], ['out_no' => $order_code]);
            } else {
                echo response_builder(false, 406, null, 'Gagal menambah data Mutasi');
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    function mutation_check_stock($products, $warehouse, $date) {
        foreach ($products as $value) {
            $check_stock = $this->m_stock->list_stock_opname($this->api_version, $warehouse, $date, '', '', '', '', '', '', '', $value['product_code']);
            if(count($check_stock) > 0) {
                $actual_qty = (Int) $check_stock[0]->QtyData;
                $input_qty = (Int) $value['product_qty_input'];
                if($input_qty > $actual_qty) {
                    echo response_builder(false, 406, null, 'Stock persediaan barang '. $value['product_name'] .' tidak mencukupi');
                    exit;
                }
            }
        }
    }

    function submit_detail_mutation($order_code, $req_code, $products) {
        $i = 1;
        $plan_detail_prefix = 'CB-'.date('ymd').'-'.date('His').'-';
        foreach ($products as $value) {
            $uom                        = $value['product_uom_input'];
            $data_prod['out_det_no']    = $plan_detail_prefix.str_pad($i, 4, '0', STR_PAD_LEFT);
            $data_prod['out_no']        = $order_code;
            $data_prod['det_req_no']    = $req_code;
            $data_prod['prod_no']       = $value['product_id'];
            $data_prod['gud_no1']       = DEFAULT_WAREHOUSE;
            $data_prod['gud_no2']       = BANDAHARA_WAREHOUSE;
            $data_prod['qty_satuan']    = $value['product_qty_input'];
            $data_prod['satuan']        = $uom;
            $data_prod['konversi']      = $value['product_prices'][$uom-1]['uom_qty'];
            $data_prod['out_det_desc']  = $value['product_note'];
            $data_prod['out_det_qty']   = $data_prod['qty_satuan'] * $data_prod['konversi'];   

            $result_prod                = $this->m_global->insert('tdetail_out_mutasi/mobile', $data_prod);
            $i++;
        }
        echo response_builder(true, 201, null, 'Berhasil menambahkan data Mutasi');
    }

    function mutation_history(){
        if($this->api_version == '1'){
            $response       = [];
            $product_data   = [];
            $search         = $this->input->post('search');
            $page           = $this->input->post('page') ?: '0';
            $date           = $this->input->post('date') ?: date('Y-m-d');
            $warehouse      = $this->input->post('warehouse') ?: DEFAULT_WAREHOUSE;
            $limit          = 1000;
            $offset         = $limit*$page;
            $data           = $this->m_stock->mutation_history($this->api_version, $warehouse, $search, $date, $limit, $offset);
            foreach ($data as $value) {
                $product_data[$value->out_no]['date'] = $value->out_date; 
                $product_data[$value->out_no]['description'] = $value->out_desc; 
                $product_data[$value->out_no]['products'][] = [
                    'product_code' => $value->prod_code0,
                    'product_name' => $value->prod_name0,
                    'product_prices' => [['uom' => $value->nm_satuan]],
                    'product_quantities' => [['value' => (String) $value->qty_satuan]]
                ];
            }
            foreach ($product_data as $key => $value) {
                $response[] = [
                    'order_code' => $key,
                    'date' => $value['date'],
                    'description' => $value['description'],
                    'products' => $value['products']
                ];
            }
            echo response_builder(true, 200, $response);
        }else{
            echo response_builder(false, 900);
        }
    }

    function item_receive(){
        if($this->api_version == '1'){
            $response       = [];
            $search         = $this->input->post('search');
            $page           = $this->input->post('page') ?: '0';
            $date           = $this->input->post('date') ?: date('Y-m-d');
            $warehouse      = $this->input->post('warehouse') ?: BANDAHARA_WAREHOUSE;
            $cab_no         = $this->input->post('cab_no');
            $limit          = 50;
            $offset         = $limit*$page;
            $data           = $this->m_stock->item_receive($this->api_version, $warehouse, $search, $date, $cab_no, $limit, $offset);
            foreach ($data as $value) {
                $response[] = [
                    'parent_code' => $value->pur_ord,
                    'order_code' => $value->No_Penerimaan,
                    'date' => $value->Tgl_Penerimaan,
                    'description' => $value->Keterangan,
                    'user' => $value->user_create
                ];
            }
            echo response_builder(true, 200, $response);
        }else{
            echo response_builder(false, 900);
        }
    }

    function item_receive_detail(){
        if($this->api_version == '1'){
            $response       = [];
            $req_code       = $this->input->post('request_code');
            $order_code     = $this->input->post('receive_code');

            $response['parent_code'] = $req_code;
            if($order_code == '') {
                $order_code_prefix      = 'CB-MPN'.date('ymd');
                $response['order_code'] = $order_code_prefix.UNDEFINED_QUEUE;
                $response['status']     = "on_process";
                $response['date']       = date('Y-m-d H:i:s');

                $join_prod      = [
                    ['table' => 'tproduct tp', 'on' => 'tp.prod_no=ttr.prod_no'],
                    ['table' => 'tgudang tg', 'on' => 'tg.gud_no=ttr.gud_no2'],
                ];
                $select_prod    = '*,'.$this->basic_product_select;
                $products       = $this->m_global->get_data_all('td_transfer_req ttr/bandahara', $join_prod, ['ttr.req_no' => $req_code], $select_prod, null, ['det_req_no', 'ASC']);
                foreach ($products as $value) {
                    $data_prices    = $this->product_prices($value);
                    $qty_minus      = $value->satuan - 1;
                    $selected_price = [];
                    if(count($data_prices) > $qty_minus) {
                        $selected_price = [$data_prices[$qty_minus]];
                    }
                    //hpp
                    $whpp       = "tgl < '".$response['date']."'";
                    $get_hpp    = $this->m_global->get_data_all('ttrans_hpp', null, ['prod_no' => $value->prod_no], 'last_prod_rata_price', $whpp, ['tgl', 'DESC'], 0, 1);
                    if(count($get_hpp) > 0) {
                        $hpp    = $get_hpp[0]->last_prod_rata_price;
                    } else {
                        $hpp    = 0;
                    }
                    //
                    $response['products'][] = [
                        'product_id' => $value->prod_no,
                        'product_name' => $value->prod_name0,
                        'product_code' => $value->prod_code0,
                        'product_location_code' => $value->gud_code,
                        'product_prices' => $selected_price,
                        'product_uom_input' => $value->satuan,
                        'product_note' => $value->det_req_desc,
                        'product_quantities' => [
                            ['value' => $value->qty_satuan]
                        ],
                        'product_hpp' => (double) $hpp,
                        'product_hpp_total' => (double) ($value->qty_satuan * $hpp)
                    ];
                }
            } else {
                $detail = $this->m_global->get_data_all('tpurchase/bandahara', null, ['pur_no' => $order_code]);
                if(count($detail) == 0) {
                    echo response_builder(false, 406, null, 'Kode penerimaan tidak ditemukan');
                    exit;
                }
                $detail = $detail[0];

                $response['order_code']     = $order_code;
                $response['date']           = $detail->pur_date;
                $response['description']    = $detail->pur_ket;
                $response['invoice_date']   = $detail->pur_inv_date;
                $response['invoice_code']   = $detail->pur_inv;
                $response['supplier']       = '';
                $response['status']         = "on_process";

                $join_prod      = [
                    ['table' => 'tproduct tp', 'on' => 'tp.prod_no=ttr.prod_no', 'tipe' => 'LEFT'],
                    ['table' => 'tdetail_in td', 'on' => 'ttr.in_det_no=td.in_det_no', 'tipe' => 'LEFT'],
                    ['table' => 'tgudang tg', 'on' => 'tg.gud_no=td.gud_no', 'tipe' => 'LEFT'],
                ];
                $select_prod    = '*,'.$this->basic_product_select;
                $products       = $this->m_global->get_data_all('td_purchase ttr/bandahara', $join_prod, ['ttr.pur_no' => $order_code], $select_prod, null, ['ttr.pur_det_no', 'ASC']);
                foreach ($products as $value) {
                    $data_prices    = $this->product_prices($value);
                    $qty_minus      = $value->satuan - 1;
                    $selected_price = [];
                    if(count($data_prices) > $qty_minus) {
                        $selected_price = [$data_prices[$qty_minus]];
                    }
                    //hpp
                    $whpp       = "tgl < '".$response['date']."'";
                    $get_hpp    = $this->m_global->get_data_all('ttrans_hpp', null, ['prod_no' => $value->prod_no], 'last_prod_rata_price', $whpp, ['tgl', 'DESC'], 0, 1);
                    if(count($get_hpp) > 0) {
                        $hpp    = $get_hpp[0]->last_prod_rata_price;
                    } else {
                        $hpp    = 0;
                    }
                    //
                    $response['products'][] = [
                        'product_id' => $value->prod_no,
                        'product_name' => $value->prod_name0,
                        'product_code' => $value->prod_code0,
                        'product_location_code' => $value->gud_code,
                        'product_prices' => $selected_price,
                        'product_uom_input' => $value->satuan,
                        'product_qty_input' => $value->qty_satuan,
                        'product_note' => $value->pur_det_keterangan ?: '',
                        'product_quantities' => [
                            ['value' => $value->qty_satuan]
                        ],
                        'product_hpp' => (double) $hpp,
                        'product_hpp_total' => (double) ($value->qty_satuan * $hpp)
                    ];
                }
            }

            $response['wh_request'] = [
                'id' => "ID2",
                'title' => "GUDANG MADURA"
            ];
            $response['wh_receive'] = [
                'id' => "ID-190121-105451-0003",
                'title' => "TOKO BANDAHARA"
            ];

            echo response_builder(true, 200, $response);
        }else{
            echo response_builder(false, 900);
        }
    }

    function item_receive_export() {
        if($this->api_version == '1'){
            $response       = [];
            $req_code       = $this->input->post('request_code');
            $order_code     = $this->input->post('receive_code');

            $response['parent_code'] = $req_code;
            $detail = $this->m_global->get_data_all('tpurchase/bandahara', null, ['pur_no' => $order_code]);
            if(count($detail) == 0) {
                echo response_builder(false, 406, null, 'Kode penerimaan tidak ditemukan');
                exit;
            }
            $detail = $detail[0];

            $response['order_code']     = $order_code;
            $response['date']           = $detail->pur_date;
            $response['description']    = $detail->pur_ket;
            $response['invoice_date']   = $detail->pur_inv_date;
            $response['invoice_code']   = $detail->pur_inv;
            $response['supplier']       = '';
            $response['status']         = "on_process";

            $join_prod      = [
                ['table' => 'tproduct tp', 'on' => 'tp.prod_no=ttr.prod_no', 'tipe' => 'LEFT'],
                ['table' => 'tdetail_in td', 'on' => 'ttr.in_det_no=td.in_det_no', 'tipe' => 'LEFT'],
                ['table' => 'tgudang tg', 'on' => 'tg.gud_no=ttr.gud_no', 'tipe' => 'LEFT'],
            ];
            $select_prod    = '*,'.$this->basic_product_select;
            $products       = $this->m_global->get_data_all('td_purchase ttr/bandahara', $join_prod, ['ttr.pur_no' => $order_code], $select_prod, null, ['ttr.pur_det_no', 'ASC']);
            foreach ($products as $value) {
                $data_prices    = $this->product_prices($value);
                $qty_minus      = $value->satuan - 1;
                $selected_price = [];
                if(count($data_prices) > $qty_minus) {
                    $selected_price = [$data_prices[$qty_minus]];
                }
                $response['products'][] = [
                    'product_id' => $value->prod_no,
                    'product_name' => $value->prod_name0,
                    'product_code' => $value->prod_code0,
                    'product_location_code' => $value->gud_code,
                    'product_prices' => $selected_price,
                    'product_uom_input' => $value->satuan,
                    'product_qty_input' => $value->qty_satuan,
                    'product_note' => $value->pur_det_keterangan ?: '',
                    'product_quantities' => [
                        ['value' => $value->qty_satuan]
                    ]
                ];
            }
            $response['wh_request'] = [
                'id' => "ID2",
                'title' => "GUDANG"
            ];
            $response['wh_receive'] = [
                'id' => "ID-190121-105451-0003",
                'title' => "TOKO"
            ];

            $this->load->library('ItemReceiveReportPDF');
            $pdf = new ItemReceiveReportPDF("L","mm", [210, 148]);
            $pdf->AddPage();
            $pdf->SetAutoPageBreak(true);
            $pdf->header_report();
            $pdf->body($response, $this->headers['Username']);

            $namefile = "item_receive_".$order_code."_report_".date('Ymdhis').".pdf";
            $pdf->Output("./assets/pdf/".$namefile, "F");

            echo response_builder(true, 200, ['title' => $namefile]);
        }else{
            echo response_builder(false, 900);
        }
    }

    function submit_item_receive(){
        if($this->api_version == '1'){
            $req_code       = $this->input->post('request_code');
            $order_code     = $this->input->post('receive_code');
            $date           = $this->input->post('date');
            $note           = $this->input->post('note');
            $invoice_date   = $this->input->post('invoice_date');
            $invoice_code   = $this->input->post('invoice_code');
            $cab_no         = $this->input->post('cab_no');
            $products       = base64_decode($this->input->post('products'));
            $products       = json_decode($products, true);

            if($req_code == '' || $req_code == null) {
                echo response_builder(false, 406, null, 'Kode request tidak boleh kosong');
                exit;
            }

            if(strpos($order_code, UNDEFINED_QUEUE)) {
                $prefix     = str_replace(UNDEFINED_QUEUE, '', $order_code);
                $order_code = $prefix.get_item_receive_queue($prefix);
            }

            $check_exist = $this->m_global->get_data_all('tpurchase_mutasi/mobile', null, ['pur_no' => $order_code]);
            if(count($check_exist) > 0) {
                $this->m_global->delete('td_purchase_mutasi/mobile', ['pur_no' => $order_code]);
                $this->m_global->delete('tpurchase_mutasi/mobile', ['pur_no' => $order_code]);
            }

            $data['iUpload']        = '0';
            $data['pur_no']         = $order_code;
            $data['pur_date']       = $date;
            $data['person_no']      = 'ID-120207-131532-0001';
            $data['pur_ket']        = $note;
            $data['pur_type']       = '4';
            $data['pur_ord']        = $req_code;
            $data['uang_id']        = 'IDR01';
            $data['kurs_cur']       = '1';
            $data['is_delete']      = '0';
            $data['user_id']        = $this->headers['User-Id'];
            $data['create_date']    = date('Y-m-d H:i:s');
            $data['user_edit']      = $this->headers['User-Id'];
            $data['edit_date']      = date('Y-m-d H:i:s');
            $data['user_delete']    = '0';
            $data['cab_no']         = $cab_no;
            $data['pur_inv']        = $invoice_code;
            $data['pur_inv_date']   = $invoice_date;
            $result                 = $this->m_global->insert('tpurchase_mutasi/mobile', $data);

            if($result['status']) {
                $this->submit_detail_item_receive($order_code, $products);
                $this->m_global->update('tpurchase_mutasi/mobile', ['iUpload' => '1'], ['pur_no' => $order_code]);
            } else {
                echo response_builder(false, 406, null, 'Gagal menambah data Penerimaan');
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    function submit_detail_item_receive($order_code, $products) {
        $i = 1;
        $plan_detail_prefix = 'CB-'.date('ymd').'-'.date('His').'-';
        foreach ($products as $value) {
            $uom                        = $value['product_uom_input'];
            $data_prod['pur_det_no']    = $plan_detail_prefix.str_pad($i, 4, '0', STR_PAD_LEFT);
            $data_prod['pur_no']        = $order_code;
            $data_prod['prod_no']       = $value['product_id'];
            $data_prod['gud_no']        = BANDAHARA_WAREHOUSE;
            $data_prod['qty_satuan']    = $value['product_qty_input'];
            $data_prod['satuan']        = $uom;
            $data_prod['konversi']      = $value['product_prices'][0]['uom_qty'];
            $data_prod['uang_id']       = 'IDR01';
            $data_prod['kurs_cur']      = '1';
            $data_prod['pur_det_keterangan']  = $value['product_note'];
            $data_prod['pur_det_qty']   = $data_prod['qty_satuan'] * $data_prod['konversi'];   

            $result_prod                = $this->m_global->insert('td_purchase_mutasi/mobile', $data_prod);
            $i++;
        }
        echo response_builder(true, 201, null, 'Berhasil menambahkan data Penerimaan');
    }

    function item_receive_history(){
        if($this->api_version == '1'){
            $response       = [];
            $product_data   = [];
            $search         = $this->input->post('search');
            $page           = $this->input->post('page') ?: '0';
            $date           = $this->input->post('date') ?: date('Y-m-d');
            $warehouse      = $this->input->post('warehouse') ?: BANDAHARA_WAREHOUSE;
            $limit          = 1000;
            $offset         = $limit*$page;
            $data           = $this->m_stock->item_receive_history($this->api_version, $warehouse, $search, $date, $limit, $offset);
            foreach ($data as $value) {
                $product_data[$value->pur_no]['date'] = $value->pur_date; 
                $product_data[$value->pur_no]['description'] = $value->pur_ket; 
                $product_data[$value->pur_no]['products'][] = [
                    'product_code' => $value->prod_code0,
                    'product_name' => $value->prod_name0,
                    'product_prices' => [['uom' => $value->nm_satuan]],
                    'product_quantities' => [['value' => (String) $value->qty_satuan]]
                ];
            }
            foreach ($product_data as $key => $value) {
                $response[] = [
                    'order_code' => $key,
                    'date' => $value['date'],
                    'description' => $value['description'],
                    'products' => $value['products']
                ];
            }
            echo response_builder(true, 200, $response);
        }else{
            echo response_builder(false, 900);
        }
    }

    function new_product_list(){
        if($this->api_version == '1'){
            $response       = [];
            $search         = $this->input->post('search');
            $page           = $this->input->post('page') ?: '0';
            $warehouse      = $this->input->post('warehouse') ?: DEFAULT_WAREHOUSE;
            $limit          = 50;
            $offset         = $limit*$page;
            $data           = $this->m_stock->new_product_list($this->api_version, $warehouse, $search, $limit, $offset);
            foreach ($data as $value) {
                $response[] = [
                    'product_id' => $value->prod_no,
                    'product_code' => $value->prod_code0,
                    'product_name' => $value->prod_name0,
                    'product_prices' => [['uom' => $value->prod_uom]],
                    'product_quantities' => [['value' => (String) $value->OnHand]]
                ];
            }
            echo response_builder(true, 200, $response);
        }else{
            echo response_builder(false, 900);
        }
    }

    function product_come_list(){
        if($this->api_version == '1'){
            $response       = [];
            $product_data   = [];
            $search         = $this->input->post('search');
            $page           = $this->input->post('page') ?: '0';
            $date           = $this->input->post('date') ?: date('Y-m-d');
            $warehouse      = $this->input->post('warehouse') ?: DEFAULT_WAREHOUSE;
            $limit          = 50;
            $offset         = $limit*$page;
            $data           = $this->m_stock->product_come_list($this->api_version, $warehouse, $search, $date, $limit, $offset);
            foreach ($data as $value) {
                $product_data[$value->pur_no]['date'] = $value->pur_date; 
                $product_data[$value->pur_no]['description'] = $value->pur_ket; 
                $product_data[$value->pur_no]['products'][] = [
                    'product_code' => $value->prod_code0,
                    'product_name' => $value->prod_name0,
                    'product_prices' => [['uom' => $value->nm_satuan]],
                    'product_quantities' => [['value' => (String) $value->qty_satuan]]
                ];
            }
            foreach ($product_data as $key => $value) {
                $response[] = [
                    'order_code' => $key,
                    'date' => $value['date'],
                    'description' => $value['description'],
                    'products' => $value['products']
                ];
            }
            echo response_builder(true, 200, $response);
        }else{
            echo response_builder(false, 900);
        }
    }

    function madura_product_list(){
        if($this->api_version == '1'){
            $response       = [];
            $search         = $this->input->post('search');
            $page           = $this->input->post('page') ?: '0';
            $warehouse      = $this->input->post('warehouse') ?: DEFAULT_WAREHOUSE;
            $limit          = 50;
            $offset         = $limit*$page;
            $data           = $this->m_stock->madura_product_list($this->api_version, $warehouse, $search, $limit, $offset);
            foreach ($data as $value) {
                $response[] = [
                    'product_id' => $value->prod_no,
                    'product_code' => $value->prod_code0,
                    'product_name' => $value->prod_name0,
                    'product_prices' => [['uom' => $value->prod_uom]],
                    'product_quantities' => [['value' => (String) $value->OnHand]]
                ];
            }
            echo response_builder(true, 200, $response);
        }else{
            echo response_builder(false, 900);
        }
    }

    function list_csw_opname(){
        if($this->api_version == '1'){
            $response       = [];
            $search         = $this->input->post('search');
            $page           = $this->input->post('page') ?: '0';
            $warehouse      = $this->input->post('warehouse') ?: BANDAHARA_WAREHOUSE;
            $limit          = 50;
            $offset         = $limit*$page;
            $data           = $this->m_stock->list_csw_opname($this->api_version, $warehouse, $search, $limit, $offset);
            foreach ($data as $value) {
                $product = [
                    'product_id' => $value->prod_no,
                    'product_code' => $value->prod_code0,
                    'product_name' => $value->prod_name0,
                    'product_quantities' => [
                        ['title' => 'csw', 'value' => (String) $value->OnHand],
                        ['title' => 'branch', 'value' => (String) $value->OnBranch]
                    ]
                ];
                if($value->Unit1 != ''){
                    $unit = explode(' ', $value->Unit1);
                    $product['product_prices'][] = [
                        'uom' => $unit[1],
                        'uom_qty' => $unit[0]
                    ];
                }
                if($value->Unit2 != ''){
                    $unit = explode(' ', $value->Unit2);
                    $product['product_prices'][] = [
                        'uom' => $unit[1],
                        'uom_qty' => $unit[0]
                    ];
                }
                if($value->Unit3 != ''){
                    $unit = explode(' ', $value->Unit3);
                    $product['product_prices'][] = [
                        'uom' => $unit[1],
                        'uom_qty' => $unit[0]
                    ];
                }
                $response[] = $product;
            }
            echo response_builder(true, 200, $response);
        }else{
            echo response_builder(false, 900);
        }
    }

    function list_csw_item(){
        if($this->api_version == '1'){
            $response       = [];
            $search         = $this->input->post('search');
            $date_start     = $this->input->post('date_start');
            $date_end       = $this->input->post('date_end');
            $data           = $this->m_stock->list_csw_item($this->api_version, BANDAHARA_WAREHOUSE, $search, $date_start, $date_end);
            foreach ($data as $value) {
                $response[] = [
                    'order_code' => $value->adj_no,
                    'date' => $value->adj_date,
                    'description' => $value->adj_desc,
                    'checker_name' => $value->user_create,
                    'wh_request' => [
                        'id' => $value->gud_no,
                        'title' => $value->gud_code.' - '.$value->gud_name,
                    ]
                ];
            }
            echo response_builder(true, 200, $response);
        }else{
            echo response_builder(false, 900);
        }
    }

    function csw_item_detail(){
        if($this->api_version == '1'){
            $response       = [];
            $order_code     = $this->input->post('order_code');

            if($order_code == '') {
                $order_code_prefix      = 'CB-MCSW'.date('ymd');
                $response['order_code'] = $order_code_prefix.UNDEFINED_QUEUE;
                $response['status']     = "on_process";
                $response['date']       = date('Y-m-d H:i:s');
            } else {
                $detail = $this->m_global->get_data_all('tadjust_wh/bandahara', null, ['adj_no' => $order_code]);
                if(count($detail) == 0) {
                    echo response_builder(false, 406, null, 'Kode csw tidak ditemukan');
                    exit;
                }
                $detail = $detail[0];

                $response['order_code']     = $order_code;
                $response['date']           = $detail->adj_date;
                $response['description']    = $detail->adj_desc;
                $response['checker_name']   = $detail->adj_terima;

                $join_prod      = [['table' => 'tproduct tp', 'on' => 'tp.prod_no=ttr.prod_no']];
                $select_prod    = '*,'.$this->basic_product_select;
                $products       = $this->m_global->get_data_all('td_adjust_wh ttr/bandahara', $join_prod, ['ttr.adj_no' => $order_code], $select_prod);
                foreach ($products as $value) {
                    $response['products'][] = [
                        'product_id' => $value->prod_no,
                        'product_name' => $value->prod_name0,
                        'product_code' => $value->prod_code0,
                        'product_note' => $value->det_adj_desc,
                        'product_qty_input' => $value->det_qty_in,
                        'product_qty_input2' => $value->det_qty_out,
                        'product_quantities' => [$this->product_quantities_csw($value->prod_no)]
                    ];
                }
            }

            echo response_builder(true, 200, $response);
        }else{
            echo response_builder(false, 900);
        }
    }

    function submit_csw_item(){
        if($this->api_version == '1'){
            $wh_id          = $this->headers['Warehouse-Id'];
            $order_code     = $this->input->post('order_code');
            $date           = $this->input->post('date');
            $checker_name   = $this->input->post('checker_name');
            $cab_no         = $this->input->post('cab_no');
            $note           = $this->input->post('note');
            $products       = base64_decode($this->input->post('products'));
            $products       = json_decode($products, true);

            if(strpos($order_code, UNDEFINED_QUEUE)) {
                $prefix     = str_replace(UNDEFINED_QUEUE, '', $order_code);
                $order_code = $prefix.get_csw_item_queue($prefix);
            }

            $check_exist = $this->m_global->get_data_all('tadjust_wh_mutasi/mobile', null, ['adj_no' => $order_code]);
            if(count($check_exist) > 0) {
                $this->m_global->delete('td_adjust_wh_mutasi/mobile', ['adj_no' => $order_code]);
                $this->m_global->delete('tadjust_wh_mutasi/mobile', ['adj_no' => $order_code]);
            }

            $data['iUpload']        = '0';
            $data['adj_no']         = $order_code;
            $data['adj_date']       = $date;
            $data['adj_desc']       = $note;
            $data['adj_type']       = '0';
            $data['adj_terima']     = $checker_name;
            $data['gud_no']         = $wh_id;
            $data['cab_no']         = $cab_no;
            $data['is_delete']      = '0';
            $data['is_delete']      = '0';
            $data['user_id']        = $this->headers['User-Id'];
            $data['create_date']    = date('Y-m-d H:i:s');
            $data['user_edit']      = $this->headers['User-Id'];
            $data['edit_date']      = date('Y-m-d H:i:s');
            $data['user_delete']    = '0';
            $result                 = $this->m_global->insert('tadjust_wh_mutasi/mobile', $data);

            if($result['status']) {
                $this->submit_detail_csw_item($order_code, $wh_id, $products);
                $this->m_global->update('tadjust_wh_mutasi/mobile', ['iUpload' => '1'], ['adj_no' => $order_code]);
            } else {
                echo response_builder(false, 406, null, 'Gagal menambah data CSW Item');
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    function submit_detail_csw_item($order_code, $wh_id, $products) {
        $i = 1;
        $plan_detail_prefix = 'CB-'.date('ymd').'-'.date('His').'-';
        foreach ($products as $value) {
            $data_prod['adj_det_no']    = $plan_detail_prefix.str_pad($i, 4, '0', STR_PAD_LEFT);
            $data_prod['adj_no']        = $order_code;
            $data_prod['prod_no']       = $value['product_id'];
            $data_prod['gud_no']        = $wh_id;
            $data_prod['qty_satuan']    = $value['product_qty_input'];
            $data_prod['satuan']        = '1';
            $data_prod['konversi']      = '1';
            $data_prod['det_adj_desc']  = $value['product_note'];
            $data_prod['det_qty_in']    = $value['product_qty_input'] * $data_prod['konversi'];   
            $data_prod['det_qty_out']   = $value['product_qty_input2'] * $data_prod['konversi'];   

            $result_prod                = $this->m_global->insert('td_adjust_wh_mutasi/mobile', $data_prod);
            $i++;
        }
        echo response_builder(true, 201, null, 'Berhasil menambahkan data CSW Item');
    }

    function po_purchase_report(){
        if($this->api_version == '1'){
            $response       = [];
            $product_data   = [];
            $search         = $this->input->post('search');
            $page           = $this->input->post('page') ?: '0';
            $date           = $this->input->post('date') ?: date('Y-m-d');
            $warehouse      = $this->input->post('warehouse') ?: DEFAULT_WAREHOUSE;
            $limit          = 1000;
            $offset         = $limit*$page;
            $data           = $this->m_stock->po_purchase_report($this->api_version, $warehouse, $search, $date, $limit, $offset);
            foreach ($data as $value) {
                $qty_order = (String) $value->Qty_Order;
                $qty_shipment = (String) $value->Qty_Kirim;
                $qty_remain = (String) $value->Qty_Sisa;
                $product_data[$value->no_faktur]['date'] = $value->pur_ord_date; 
                $product_data[$value->no_faktur]['description'] = $value->nm_supplier; 
                $product_data[$value->no_faktur]['products'][] = [
                    'product_code' => $value->prod_code0,
                    'product_name' => $value->prod_name0,
                    'product_prices' => [['uom' => $value->nm_satuan]],
                    'product_quantities' => [[
                        'value' => 'Order:   '.$qty_order.PHP_EOL.'Kirim:   '.$qty_shipment.PHP_EOL.'Sisa:   '.$qty_remain
                    ]]
                ];
            }
            foreach ($product_data as $key => $value) {
                $response[] = [
                    'order_code' => $key,
                    'date' => $value['date'],
                    'description' => $value['description'],
                    'products' => $value['products']
                ];
            }
            echo response_builder(true, 200, $response);
        }else{
            echo response_builder(false, 900);
        }
    }

    function discount_period_report(){
        if($this->api_version == '1'){
            $response       = [];
            $product_data   = [];
            $search         = $this->input->post('search');
            $page           = $this->input->post('page') ?: '0';
            $date           = $this->input->post('date') ?: '';
            $warehouse      = $this->input->post('warehouse') ?: DEFAULT_WAREHOUSE;
            $limit          = 30;
            $offset         = $limit*$page;
            $data           = $this->m_stock->discount_period_report($this->api_version, $warehouse, $search, $date, $limit, $offset);
            foreach ($data as $value) {
                $response[] = [
                    'product_code'      => $value->prod_code0,
                    'product_name'      => $value->prod_name0,
                    'product_discount'  => [
                        'start_date'        => $value->disc_tgl1,
                        'end_date'          => $value->disc_tgl2,
                        'status'            => $value->Status,
                        'status_discount'   => $value->StatusDisc,
                        'qty_limit'         => $value->qty_limit,
                        'discount'          => [
                            ['title' => str_replace('Rp. ', '', uang($value->disc_value)), 'value' => $value->disc_prefix],
                            ['title' => str_replace('Rp. ', '', uang($value->disc_value_1)), 'value' => $value->disc_prefix_1],
                            ['title' => str_replace('Rp. ', '', uang($value->disc_value_2)), 'value' => $value->disc_prefix_2],
                            ['title' => str_replace('Rp. ', '', uang($value->disc_value_3)), 'value' => $value->disc_prefix_3],
                        ]
                    ]
                ];
            }
            echo response_builder(true, 200, $response);
        }else{
            echo response_builder(false, 900);
        }
    }

    function promotion_setting_report(){
        if($this->api_version == '1'){
            $response       = [];
            $search         = $this->input->post('search');
            $page           = $this->input->post('page') ?: '0';
            $warehouse      = $this->input->post('warehouse') ?: DEFAULT_WAREHOUSE;
            $limit          = 50;
            $offset         = $limit*$page;
            $data           = $this->m_stock->promotion_setting_report($this->api_version, $warehouse, $search, $limit, $offset);
            foreach ($data as $value) {
                $response[] = [
                    'product_code' => $value->prod_code0,
                    'product_name' => $value->prod_name0,
                    'product_promotion' => [
                        'start_date'        => $value->pro_tgl1,
                        'end_date'          => $value->pro_tgl2,
                        'qty'               => $value->pro_qty,
                        'code'              => $value->code_alias,
                        'name'              => $value->name_alias
                    ]
                ];
            }
            echo response_builder(true, 200, $response);
        }else{
            echo response_builder(false, 900);
        }
    }

    function csw_report(){
        if($this->api_version == '1'){
            $response       = [];
            $product_data   = [];
            $search         = $this->input->post('search');
            $qty_in         = $this->input->post('qty_in');
            $qty_out        = $this->input->post('qty_out');
            $page           = $this->input->post('page') ?: '0';
            $date           = $this->input->post('date') ?: date('Y-m-d');
            $warehouse      = $this->input->post('warehouse') ?: DEFAULT_WAREHOUSE;
            $limit          = 1000;
            $offset         = $limit*$page;
            $data           = $this->m_stock->csw_report($this->api_version, $warehouse, $search, $date, $limit, $offset, $qty_in, $qty_out);
            foreach ($data as $value) {
                $qty_all    = (String) $value->Qty;
                $qty_in     = (String) $value->det_qty_in;
                $qty_out    = (String) $value->det_qty_out;
                $product_data[$value->adj_no]['date'] = $value->adj_date; 
                $product_data[$value->adj_no]['description'] = $value->adj_desc; 
                $product_data[$value->adj_no]['products'][] = [
                    'product_code' => $value->prod_code0,
                    'product_name' => $value->prod_name0,
                    'product_prices' => [['uom' => $value->nm_satuan]],
                    'product_quantities' => [[
                        'value' => 'Qty:   '.$qty_all.PHP_EOL.'In:   '.$qty_in.PHP_EOL.'Out:   '.$qty_out
                    ]]
                ];
            }
            foreach ($product_data as $key => $value) {
                $response[] = [
                    'order_code' => $key,
                    'date' => $value['date'],
                    'description' => $value['description'],
                    'products' => $value['products']
                ];
            }
            echo response_builder(true, 200, $response);
        }else{
            echo response_builder(false, 900);
        }
    }
}

/* End of file config.php */
/* banner: ./application/modules/config/controllers/config.php */