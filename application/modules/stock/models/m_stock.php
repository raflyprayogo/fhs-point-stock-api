<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_stock extends CI_Model {

    public function __construct(){
        parent::__construct();
    }

    private function get_naming($warehouse){
        $db_name = '-';
        if($warehouse == DEFAULT_WAREHOUSE){
            $db_name = 'default';
        }
        if($warehouse == BANDAHARA_WAREHOUSE){
            $db_name = 'bandahara';
        }
        return $db_name;
    }

    private function get_date($date){
        $date = explode(' - ', $date);
        $start_date = $date[0];
        if(count($date) > 1){
            $end_date = $date[1];
        } else {
            $end_date = $date[0];
        }
        return (Object) ['start' => $start_date. ' 00:00:00', 'end' => $end_date. ' 23:59:59'];
    }

    function stock_card_init_balance($version = '1', $prod_no, $date_start, $table, $db_wh, $tran_type, $warehouse_id = ''){
        $this->db = $this->load->database($this->get_naming($db_wh), true);
        if($version == '1') {
            $where_wh = '';
            if($warehouse_id != '') {
                $where_wh = "and xa.gud_no = '$warehouse_id'";
            }
            $where_tran_type = '';
            if($tran_type == 'purchase') {
                $where_tran_type = " and (xa.tran_type = 1 or xa.tran_type = 2) ";
            }
            if($tran_type == 'sell') {
                $where_tran_type = " and (xa.tran_type = 3 or xa.tran_type = 4) ";
            }
            if($tran_type == 'correction') {
                $where_tran_type = " and xa.tran_type = 17 ";
            }
            if($tran_type == 'mutation') {
                $where_tran_type = " and xa.tran_type = 13 ";
            }
            $sql = "select 'SALDO AWAL' as No_Faktur, sum(xa.debet - xa.kredit) as OnHand
                    from $table xa
                    where xa.prod_no = '$prod_no'
                    and xa.tgl < '$date_start 00:00:00' $where_wh $where_tran_type";
            $query  = $this->db->query($sql);
            $result = $query->result();
            return $result;
        }
    }

    function stock_card($version = '1', $prod_no, $date_start, $date_end, $tran_type, $warehouse_id = ''){
        $this->db = $this->load->database($this->get_naming(DEFAULT_WAREHOUSE), true);
        if($version == '1') {
            $where_wh = '';
            if($warehouse_id != '') {
                $where_wh = "and xa.gud_no = '$warehouse_id'";
            }
            $where_tran_type = '';
            if($tran_type == 'purchase') {
                $where_tran_type = " and (xa.tran_type = 1 or xa.tran_type = 2) ";
            }
            if($tran_type == 'sell') {
                $where_tran_type = " and (xa.tran_type = 3 or xa.tran_type = 4) ";
            }
            if($tran_type == 'correction') {
                $where_tran_type = " and xa.tran_type = 17 ";
            }
            if($tran_type == 'mutation') {
                $where_tran_type = " and xa.tran_type = 13 ";
            }
            $sql = "select 
                      xa.tgl, 
                      if(
                        xa.tran_type = 17, 
                        xe.out_no, 
                        if(
                          xa.tran_type = 13, 
                          if(
                            xe.out_type = 11, 
                            ifnull(xc.out_no, xe.out_no), 
                            if(
                              in_type = 4, 
                              ifnull(xf.out_no, xe.out_no), 
                              ifnull(xc.req_no, xe.req_no)
                            )
                          ), 
                          if(
                            xa.tran_type = 7, 
                            concat(
                              xe.jual_no, ' - ', xxe.person_name
                            ), 
                            if(
                              xa.tran_type = 4, 
                              xc.sal_ret_no, 
                              if(
                                xa.tran_type = 3, 
                                concat(
                                  xe.jual_no, ' - ', xxe.person_name
                                ), 
                                if(
                                  xa.tran_type = 2, 
                                  concat(
                                    xe.ret_pur_no, ' - ', xxe.person_name
                                  ), 
                                  if(
                                    xa.tran_type = 1, 
                                    if(
                                      xc.in_type = 1, xc.in_no, xc.pur_no
                                    ), 
                                    ''
                                  )
                                )
                              )
                            )
                          )
                        )
                      ) as Transaksi, 
                      xa.debet, 
                      xa.kredit 
                    from 
                      ttrans xa 
                      left join tdetail_in xb on xa.in_det_no = xb.in_det_no 
                      left join tin xc on xb.in_no = xc.in_no 
                      left join tdetail_out xd on xa.out_det_no = xd.out_det_no 
                      left join tout xe on xd.out_no = xe.out_no 
                      left join tperson xxe on xe.person_no = xxe.person_no 
                      left join tout xf on xc.in_no = xf.in_no 
                    where 
                      xa.prod_no = '$prod_no' 
                      and xa.tgl >= '$date_start 00:00:00' 
                      and xa.tgl <= '$date_end 23:59:59' 
                      and (
                        xa.debet <> 0 
                        or xa.kredit <> 0
                      ) 
                      $where_wh
                      $where_tran_type 
                    order by 
                      tgl asc";
            $query  = $this->db->query($sql);
            // echo $this->db->last_query(); exit;
            $result = $query->result();
            return $result;
        }
    }

    function stock_card_csw($version = '1', $prod_no, $date_start, $date_end, $db_wh, $warehouse_id = ''){
        $this->db = $this->load->database($this->get_naming($db_wh), true);
        if($version == '1') {
            $where_wh = '';
            if($warehouse_id != '') {
                $where_wh = "and xa.gud_no = '$warehouse_id'";
            }
            $sql = "select 
                      xa.tgl, 
                      if(xa.tran_type = 17, xe.adj_no, '') as Transaksi, 
                      if(
                        xa.kredit < 0, xa.kredit * -1, xa.debet
                      ) as debet, 
                      if(xa.kredit > 0, xa.kredit, 0) as kredit 
                    from 
                      ttrans_wh xa 
                      left join td_adjust_wh xd on xa.out_det_no = xd.adj_det_no 
                      left join tadjust_wh xe on xd.adj_no = xe.adj_no 
                    where 
                      xa.prod_no = '$prod_no'
                      and xa.tgl >= '$date_start 00:00:00' 
                      and xa.tgl <= '$date_end 23:59:59' 
                      and (
                        xa.debet <> 0 
                        or xa.kredit <> 0
                      ) 
                      $where_wh
                    order by 
                      tgl asc
                    ";
            $query  = $this->db->query($sql);
            // echo $this->db->last_query(); exit;
            $result = $query->result();
            return $result;
        }
    }

    function price_changes($version = '1', $prod_no, $date_start, $date_end) {
        $where_prod_no = '';
        if($prod_no != '') {
          $where_prod_no = "and a.prod_no = '$prod_no'";
        }
        $this->db = $this->load->database($this->get_naming(DEFAULT_WAREHOUSE), true);
        if($version == '1') {
            $sql = "select 
                      a.prod_no, 
                      a.price_date, 
                      b.prod_code0, 
                      b.prod_name0, 
                      if(
                        a.satuan = 3, 
                        b.prod_uom3, 
                        if(
                          a.satuan = 2, b.prod_uom2, b.prod_uom
                        )
                      ) as Satuan, 
                      a.hj_old_1, 
                      a.hj_old_2, 
                      a.hj_old_3, 
                      a.hj_new_1, 
                      a.hj_new_2, 
                      a.hj_new_3, 
                      c.user_name 
                    from 
                      tdgproduct_harga a 
                      left join tproduct b on a.prod_no = b.prod_no 
                      left join tusers c on a.user_id = c.user_id 
                    where 
                      b.is_delete = 0 
                      $where_prod_no 
                      and a.price_date >= '$date_start 00:00:00' 
                      and a.price_date <= '$date_end 23:59:59' 
                    order by 
                      price_date desc, 
                      id asc 
                    limit 
                      100;
                    ";
            $query  = $this->db->query($sql);
            $result = $query->result();
            return $result;
        }
    }

    function graph_report_sell($version = '1', $prod_no, $date_start, $date_end, $type = 'daily') {
        $this->db = $this->load->database($this->get_naming(DEFAULT_WAREHOUSE), true);
        if($version == '1') {
            $type = ($type == 'daily' ? 'day' : 'month');
            $sql = "SELECT xb.jual_date AS Harian
                      ,sum(xa.det_total_kurs) as value
                    FROM td_sales xa
                    LEFT JOIN tsales xb ON xa.jual_no = xb.jual_no
                    WHERE xb.is_delete = 0
                      AND jual_type = 1
                      AND prod_no = '$prod_no'
                      AND xb.jual_date >= '$date_start 00:00:00'
                      AND xb.jual_date <= '$date_end 23:59:59'
                    GROUP BY $type(xb.jual_date);
                    ";
            $query  = $this->db->query($sql);
            $result = $query->result();
            return $result;
        }
    }

    function graph_report_buy($version = '1', $data_type, $data_code, $date_start, $date_end, $type = 'daily') {
        $this->db = $this->load->database($this->get_naming(DEFAULT_WAREHOUSE), true);
        if($version == '1') {
            $type = ($type == 'daily' ? 'day' : 'month');
            $data_type = ($data_type == 'supplier' ? 'xb.person_no' : 'xa.prod_no');
            $sql = "SELECT xb.pur_date AS Harian
                      ,sum(xa.pur_det_sub_total_kurs) as value
                    FROM td_purchase xa
                    LEFT JOIN tpurchase xb ON xa.pur_no = xb.pur_no
                    WHERE xb.is_delete = 0
                      AND pur_type = 1
                      AND $data_type = '$data_code'
                      AND xb.pur_date >= '$date_start 00:00:00'
                      AND xb.pur_date <= '$date_end 23:59:59'
                    GROUP BY $type(xb.pur_date);
                    ";
            $query  = $this->db->query($sql);
            $result = $query->result();
            return $result;
        }
    }

    function list_planogram($version = '1', $rack_id, $wh_id) {
        $this->db = $this->load->database($this->get_naming(DEFAULT_WAREHOUSE), true);
        if($version == '1') {
            $date = date('Y-m-d H:i:s');
            $sql = "select 
                      a.prod_no, 
                      a.prod_code0, 
                      a.prod_name0, 
                      a.prod_uom, 
                      (
                        ifnull(b.Qtot, 0) - ifnull(c.Qtrx, 0)
                      ) as QtyPersediaan 
                    from 
                      tproduct a 
                      left join (
                        select 
                          x.gud_no, 
                          x.prod_no, 
                          y.gud_name, 
                          sum(x.prod_on_hand) as Qtot 
                        from 
                          tdgproduct x 
                          left join tgudang y on x.gud_no = y.gud_no 
                        where 
                          x.gud_no = '$wh_id' 
                        group by 
                          x.prod_no
                      ) b on a.prod_no = b.prod_no 
                      left join (
                        select 
                          x.gud_no, 
                          x.prod_no, 
                          y.gud_name, 
                          sum(x.debet - x.kredit) as Qtrx 
                        from 
                          ttrans x 
                          left join tgudang y on x.gud_no = y.gud_no 
                        where 
                          x.gud_no = '$wh_id' 
                          and x.tgl > '$date' 
                        group by 
                          x.prod_no
                      ) c on a.prod_no = c.prod_no 
                    where 
                      is_delete = 0 
                      and a.rak_id = '$rack_id' 
                    order by 
                      prod_name0
                    ";
            $query  = $this->db->query($sql);
            // echo $this->db->last_query(); exit;
            $result = $query->result();
            return $result;
        }
    }

    function list_planogram_history($version = '1', $warehouse, $user_id = '', $rack_id = '', $search, $date, $limit, $offset) {
        $this->db = $this->load->database($this->get_naming($warehouse), true);
        $date = $this->get_date($date);
        if($version == '1') {
            $where_user = ($user_id != '' ? "and a.user_id = '$user_id'" : '');
            $where_rack = ($rack_id != '' ? "and c.rak_id = '$rack_id'" : '');
            $sql = "select 
                      a.plan_no as No_Planogram, 
                      a.plan_date as Tgl_Plan, 
                      b.gud_name as WareHouseName, 
                      c.nama as Rak_Name,
                      c.rak_id, 
                      ifnull(a.plan_desc, '-') as Keterangan, 
                      ifnull(t1.user_name, '-') as user_create 
                    from 
                      tplanogram a 
                      left join tgudang b on a.gud_no = b.gud_no 
                      left join tm_rak c on a.rak_id = c.rak_id 
                      left join tusers t1 on a.user_id = t1.user_id 
                    where 
                      a.plan_type = 0 
                      $where_user
                      $where_rack
                      and a.is_delete = 0 
                      and (
                        a.plan_no like '%$search%' 
                        or a.plan_desc like '%$search%'
                      ) 
                      and a.plan_date >= '$date->start' 
                      and a.plan_date <= '$date->end' 
                      and a.is_delete = 0 
                    order by 
                      a.plan_date desc 
                    limit 
                      $limit
                    offset
                      $offset
                    ";
            $query  = $this->db->query($sql);
            // echo $this->db->last_query(); exit;
            $result = $query->result();
            return $result;
        }
    }

    function list_planogram_history_by_expired_date($version = '1', $warehouse, $user_id = '', $rack_id = '', $date, $limit, $offset) {
        $this->db = $this->load->database($this->get_naming($warehouse), true);
        $date = $this->get_date($date);
        if($version == '1') {
            $where_user = ($user_id != '' ? "and b.user_id = '$user_id'" : '');
            $where_rack = ($rack_id != '' ? "and e.rak_id = '$rack_id'" : '');
            $sql = "select 
                      b.plan_no as No_Planogram, 
                      b.plan_date as Tgl_Plan, 
                      d.gud_name as WareHouseName, 
                      e.nama as Rak_Name, 
                      e.rak_id,
                      ifnull(b.plan_desc, '-') as Keterangan,
                      ifnull(t1.user_name, '-') as user_create,
                      c.prod_no, 
                      c.prod_code0, 
                      c.prod_name0, 
                      a.qty_satuan, 
                      if(
                        a.satuan = 3, 
                        c.prod_uom3, 
                        if(
                          a.satuan = 2, c.prod_uom2, c.prod_uom
                        )
                      ) as Unit, 
                      a.expired_date 
                    from 
                      td_planogram a 
                      left join tplanogram b on a.plan_no = b.plan_no 
                      left join tproduct c on a.prod_no = c.prod_no 
                      left join tgudang d on b.gud_no = d.gud_no 
                      left join tm_rak e on b.rak_id = e.rak_id
                      left join tusers t1 on b.user_id = t1.user_id  
                    where 
                      b.is_delete = 0 
                      $where_user
                      $where_rack
                      and a.expired_date >= '$date->start' 
                      and a.expired_date <= '$date->end'
                    order by 
                      b.plan_date desc
                    limit 
                      $limit
                    offset
                      $offset
                    ";
            $query  = $this->db->query($sql);
            // echo $this->db->last_query(); exit;
            $result = $query->result();
            return $result;
        }
    }

    function list_stock_opname($version = '1', $warehouse, $date, $category_id, $variant_id, $subvariant_id, $brand_id, $rack_id, $rack_warehouse_id, $product_name = '', $product_code = '') {
        $this->db = $this->load->database($this->get_naming($warehouse), true);
        if($version == '1') {
            $where_product        = ($product_name != '' ? "(xx.prod_name0 like '%$product_name%' AND xx.is_delete = 0)" : ($product_code != '' ? "(xx.prod_code0 = '$product_code' AND xx.is_delete = 0)" : "(xx.prod_name0 like '%%' AND xx.is_delete = 0)"));
            $where_category       = ($category_id != '' ? "and xb.cat_id = '$category_id'" : '');
            $where_variant        = ($variant_id != '' ? "and xb.group_id = '$variant_id'" : '');
            $where_subvariant     = ($subvariant_id != '' ? "and xb.varian_id = '$subvariant_id'" : '');
            $where_brand          = ($brand_id != '' ? "and xb.merk_id = '$brand_id'" : '');
            $where_rack           = ($rack_id != '' ? "and xb.rak_id = '$rack_id'" : '');
            $where_rack_warehouse = ($rack_warehouse_id != '' ? "and xb.rak_gudang = '$rack_warehouse_id'" : '');
            $sql = "select 
                      xx.prod_no, 
                      xx.prod_code0,
                      xx.prod_name0,
                      round(
                        ifnull(Qty1, 0) - ifnull(Qty2, 0), 
                        5
                      ) as QtyData, 
                      round(
                        ifnull(QtyWH1, 0) - ifnull(QtyWH2, 0), 
                        5
                      ) as QtyCSW, 
                      xx.prod_uom 
                    from 
                      tproduct xx 
                      left join (
                        select 
                          xa.prod_no, 
                          sum(xa.prod_on_hand) as Qty1 
                        from 
                          tdgproduct xa 
                          left join tproduct xb on xa.prod_no = xb.prod_no 
                        where 
                          xa.gud_no = '$warehouse'
                          $where_category
                          $where_variant
                          $where_subvariant
                          $where_brand
                          $where_rack
                          $where_rack_warehouse
                        group by 
                          xa.prod_no
                      ) xx1 on xx.prod_no = xx1.prod_no 
                      left join (
                        select 
                          xa.prod_no, 
                          sum(xa.debet - xa.kredit) as Qty2 
                        from 
                          ttrans xa 
                          left join tproduct xb on xa.prod_no = xb.prod_no 
                        where 
                          xa.tgl > '$date 23:59:59'
                          and xa.gud_no = '$warehouse'
                          $where_category
                          $where_variant
                          $where_subvariant
                          $where_brand
                          $where_rack
                          $where_rack_warehouse
                        group by 
                          xa.prod_no
                      ) xx2 on xx.prod_no = xx2.prod_no 
                      left join (
                        select 
                          xa.prod_no, 
                          sum(xa.prod_on_hand) as QtyWH1 
                        from 
                          tdgproduct_wh xa 
                          left join tproduct xb on xa.prod_no = xb.prod_no 
                        where 
                          xa.gud_no = '$warehouse'
                          $where_category
                          $where_variant
                          $where_subvariant
                          $where_brand
                          $where_rack
                          $where_rack_warehouse
                        group by 
                          xa.prod_no
                      ) xx3 on xx.prod_no = xx3.prod_no 
                      left join (
                        select 
                          xa.prod_no, 
                          sum(xa.debet - xa.kredit) as QtyWH2 
                        from 
                          ttrans_wh xa 
                          left join tproduct xb on xa.prod_no = xb.prod_no 
                        where 
                          xa.tgl > '$date 23:59:59'
                          and xa.gud_no = '$warehouse'
                          $where_category
                          $where_variant
                          $where_subvariant
                          $where_brand
                          $where_rack
                          $where_rack_warehouse
                        group by 
                          xa.prod_no
                      ) xx4 on xx.prod_no = xx4.prod_no 
                    where 
                      $where_product
                      ".str_replace('xb', 'xx', $where_category)."
                      ".str_replace('xb', 'xx', $where_variant)."
                      ".str_replace('xb', 'xx', $where_subvariant)."
                      ".str_replace('xb', 'xx', $where_brand)."
                      ".str_replace('xb', 'xx', $where_rack)."
                      ".str_replace('xb', 'xx', $where_rack_warehouse)."
                    order by xx.prod_name0 asc
                    limit 100
                    ";
            $query  = $this->db->query($sql);
            // echo $this->db->last_query(); exit;
            $result = $query->result();
            return $result;
        }
    }

    function list_stock_opname_detail($version = '1', $warehouse, $date, $so_no) {
        $this->db = $this->load->database($this->get_naming($warehouse), true);
        if($version == '1') {
            $sql = "select aa.qty_satuan, aa.prod_no, bb.prod_code0, bb.prod_name0,
                      (bb.QtyData / aa.konversi) as QtyData, (bb.QtyCSW / aa.konversi) as QtyCSW,
                      if(aa.satuan=3,bb.Unit3,if(aa.satuan=2,bb.Unit2,bb.Unit1)) as prod_uom,
                      (aa.qty_satuan / aa.konversi) as QtyFisik from td_adjust aa
                      left join (
                        select xx.prod_no, xx.prod_code0, xx.prod_name0,
                        xx.prod_uom as Unit1, xx.prod_uom2 as Unit2, xx.prod_uom3 as Unit3,
                        round(ifnull(Qty1,0) - ifnull(Qty2,0),5) as QtyData,
                        round(ifnull(QtyWH1,0) - ifnull(QtyWH2,0),5) as QtyCSW, xx.prod_uom
                        from tproduct xx
                        left join (
                              select xa.prod_no, sum(xa.prod_on_hand) as Qty1
                              from tdgproduct xa left join tproduct xb on xa.prod_no = xb.prod_no
                                where
                                  xa.gud_no = '$warehouse' /* Gudang */
                                  group by xa.prod_no
                        ) xx1 on xx.prod_no = xx1.prod_no
                        left join (
                              select xa.prod_no, sum(xa.debet - xa.kredit) as Qty2
                              from ttrans xa left join tproduct xb on xa.prod_no = xb.prod_no
                                where
                                  xa.tgl > '$date' /* yyyy-mm-dd HH:mm:ss */
                                  and xa.gud_no = '$warehouse' /* Gudang */
                                  group by xa.prod_no
                        ) xx2 on xx.prod_no = xx2.prod_no
                        left join (
                              select xa.prod_no, sum(xa.prod_on_hand) as QtyWH1
                              from tdgproduct_wh xa left join tproduct xb on xa.prod_no = xb.prod_no
                                where
                                  xa.gud_no = '$warehouse' /* Gudang */
                                  group by xa.prod_no
                        ) xx3 on xx.prod_no = xx3.prod_no
                        left join (
                              select xa.prod_no, sum(xa.debet - xa.kredit) as QtyWH2
                              from ttrans_wh xa left join tproduct xb on xa.prod_no = xb.prod_no
                                where
                                  xa.tgl > '$date' /* yyyy-mm-dd HH:mm:ss */
                                  and xa.gud_no = '$warehouse' /* Gudang */
                                  group by xa.prod_no
                        ) xx4 on xx.prod_no = xx4.prod_no
                      ) bb on aa.prod_no = bb.prod_no
                      where aa.adj_no = '$so_no'";
            $query  = $this->db->query($sql);
            // echo $this->db->last_query(); exit;
            $result = $query->result();
            return $result;
        }
    }

    function list_stock_opname_history($version = '1', $flag, $warehouse, $rack_id = '', $brand_id = '', $search, $date, $limit, $offset) {
        $this->db = $this->load->database($this->get_naming($warehouse), true);
        $date = $this->get_date($date);
        if($version == '1') {
            if ($flag == 'csw') {
              $table        = 'tadjust_wh';
              $table_detail = 'td_adjust_wh';
            } else {
              $table        = 'tadjust';
              $table_detail = 'td_adjust';
            }
            $where_rack = ($rack_id != '' ? "and d.rak_id = '$rack_id'" : '');
            $where_brand = ($brand_id != '' ? "and d.merk_id = '$brand_id'" : '');
            $sql = "select
                      a.adj_no as No_SO,
                      a.adj_date as Tgl_SO,
                      c.gud_name as WareHouseName,
                      a.adj_desc as Keterangan,
                      ifnull(t1.user_name, '-') as user_create
                    from $table_detail xa
                      left join $table a on xa.adj_no= a.adj_no
                      left join tusers t1 on a.user_id = t1.user_id
                      left join tgudang c on a.gud_no = c.gud_no
                      left join tproduct d on xa.prod_no = d.prod_no
                    where
                      a.is_delete = 0
                      and a.adj_type = 17
                      and a.is_delete = 0
                      and c.gud_no = '$warehouse'
                      and a.adj_date >= '$date->start'
                      and a.adj_date <= '$date->end'
                      and (
                        a.adj_no like '%$search%'
                        or a.adj_desc like '%$search%'
                      )
                      $where_rack
                      $where_brand
                    group by
                      a.adj_no
                    order by
                      a.adj_date desc
                    limit
                      $limit offset $offset";
            $query  = $this->db->query($sql);
            // echo $this->db->last_query(); exit;
            $result = $query->result();
            return $result;
        }
    }

    function item_request($version = '1', $warehouse, $search, $date, $status, $status_comparison, $limit, $offset) {
        $this->db = $this->load->database($this->get_naming($warehouse), true);
        $date = $this->get_date($date);
        if($version == '1') {
            $sql = "select 
                      a.req_no, 
                      a.req_tgl, 
                      a.req_desc, 
                      ifnull(t1.user_name, '-') as user_create, 
                      if(
                        a.statusReq = 0, 
                        'REQUEST', 
                        if(
                          a.statusReq = 1, 
                          'ON PROGRESS', 
                          if(
                            a.statusReq = 2, 'COMPLETE', 'CLOSE'
                          )
                        )
                      ) as Status 
                    from 
                      ttransfer_req a 
                      left join tusers t1 on a.user_id = t1.user_id 
                    where 
                      a.is_delete = 0 
                      and a.req_type = 0 
                      and (a.req_no like '%$search%') 
                      and a.req_tgl >= '$date->start' 
                      and a.req_tgl <= '$date->end' 
                      and a.statusReq $status_comparison $status
                    order by 
                      a.req_tgl desc 
                    limit 
                      $limit
                    offset
                      $offset
                    ";
            $query  = $this->db->query($sql);
            $result = $query->result();
            return $result;
        }
    }

    function item_request_history($version = '1', $warehouse, $search, $date, $limit, $offset, $is_selection = false) {
        $this->db = $this->load->database($this->get_naming($warehouse), true);
        $where_date = "";
        if($date != '') {
          $date       = $this->get_date($date);
          $where_date = "and c.req_tgl >= '$date->start' and c.req_tgl <= '$date->end'";
        } 
        $where_is_selection = ($is_selection ? "and c.statusReq <= 1" : "");
        if($version == '1') {
            $sql = "select 
                    c.req_no,
                    c.req_desc, 
                    c.req_tgl, 
                    c.req_desc, 
                    b.prod_code0, 
                    b.prod_name0, 
                    round(a.qty_satuan - (a.qty_transfer/a.konversi),2) as qty_satuan, 
                    if(
                      a.satuan = 3, 
                      b.prod_uom3, 
                      if(
                        a.satuan = 2, b.prod_uom2, b.prod_uom
                      )
                    ) as nm_satuan 
                  from 
                    td_transfer_req a 
                    left join tproduct b on a.prod_no = b.prod_no 
                    left join ttransfer_req c on a.req_no = c.req_no 
                  where 
                    c.is_delete = 0
                    and c.req_type = 0
                    $where_is_selection
                    and (c.req_no like '%$search%')
                    $where_date
                  order by 
                    c.req_tgl desc, 
                    c.req_no, 
                    a.det_req_no
                  limit 
                    $limit
                  offset
                    $offset
                  ";
            $query  = $this->db->query($sql);
            // echo $this->db->last_query(); exit;
            $result = $query->result();
            return $result;
        }
    }

    function mutation($version = '1', $warehouse, $search, $date, $limit, $offset) {
        $this->db = $this->load->database($this->get_naming($warehouse), true);
        $date = $this->get_date($date);
        if($version == '1') {
            $sql = "select 
                      a.out_no as No_Mutasi, 
                      a.out_date as Tgl_Mutasi, 
                      a.req_no as No_Request, 
                      a.out_desc as Keterangan, 
                      ifnull(t1.user_name, '-') as user_create 
                    from 
                      tout a 
                      left join tusers t1 on a.user_id = t1.user_id 
                    where 
                      a.out_type = 11 
                      and a.is_delete = 0 
                      and (
                        a.out_no like '%$search%' 
                        or t1.user_name like '%$search%' 
                        or a.req_no like '%$search%'
                      ) 
                      and a.out_date >= '$date->start' 
                      and a.out_date <= '$date->end'
                    order by 
                      a.out_date desc
                    limit 
                      $limit
                    offset
                      $offset
                    ";
            $query  = $this->db->query($sql);
            $result = $query->result();
            return $result;
        }
    }

    function mutation_history($version = '1', $warehouse, $search, $date, $limit, $offset) {
        $this->db = $this->load->database($this->get_naming($warehouse), true);
        $date = $this->get_date($date);
        if($version == '1') {
            $sql = "select 
                      c.out_no, 
                      c.out_date, 
                      c.out_desc, 
                      b.prod_code0, 
                      b.prod_name0, 
                      a.qty_satuan, 
                      if(
                        a.satuan = 3, 
                        b.prod_uom3, 
                        if(
                          a.satuan = 2, b.prod_uom2, b.prod_uom
                        )
                      ) as nm_satuan 
                    from 
                      tdetail_out a 
                      left join tproduct b on a.prod_no = b.prod_no 
                      left join tout c on a.out_no = c.out_no 
                    where 
                      c.is_delete = 0 
                      and c.out_type = 11 
                      and (c.out_no like '%$search%')
                      and c.out_date >= '$date->start' 
                      and c.out_date <= '$date->end' 
                    order by 
                      c.out_date desc, 
                      c.out_no, 
                      a.out_det_no
                    limit 
                      $limit
                    offset
                      $offset
                    ";
            $query  = $this->db->query($sql);
            $result = $query->result();
            return $result;
        }
    }

    function item_receive($version = '1', $warehouse, $search, $date, $cab_no, $limit, $offset) {
        $this->db = $this->load->database($this->get_naming($warehouse), true);
        $date = $this->get_date($date);
        if($version == '1') {
            $sql = "select 
                      a.pur_no as No_Penerimaan, 
                      a.pur_date as Tgl_Penerimaan, 
                      a.pur_ket as Keterangan,
                      a.pur_ord,  
                      ifnull(t1.user_name, '-') as user_create 
                    from 
                      tpurchase a 
                      left join tusers t1 on a.user_id = t1.user_id 
                    where 
                      a.pur_type = 4 
                      and a.is_delete = 0 
                      and (
                        a.pur_no like '%$search%' 
                        or a.pur_ket like '%$search%'
                      ) 
                      and a.pur_date >= '$date->start' 
                      and a.pur_date <= '$date->end' 
                      and a.cab_no = '$cab_no' 
                    order by 
                      pur_date desc 
                    limit 
                      $limit
                    offset
                      $offset
                    ";
            $query  = $this->db->query($sql);
            $result = $query->result();
            return $result;
        }
    }

    function item_receive_history($version = '1', $warehouse, $search, $date, $limit, $offset) {
        $this->db = $this->load->database($this->get_naming($warehouse), true);
        $date = $this->get_date($date);
        if($version == '1') {
            $sql = "select 
                    c.pur_no, 
                    c.pur_date, 
                    c.pur_ket, 
                    b.prod_code0, 
                    b.prod_name0, 
                    a.qty_satuan, 
                    if(
                      a.satuan = 3, 
                      b.prod_uom3, 
                      if(
                        a.satuan = 2, b.prod_uom2, b.prod_uom
                      )
                    ) as nm_satuan 
                  from 
                    td_purchase a 
                    left join tproduct b on a.prod_no = b.prod_no 
                    left join tpurchase c on a.pur_no = c.pur_no 
                  where 
                    c.is_delete = 0 
                    and c.pur_type = 4 
                    and (c.pur_no like '%$search%')
                    and c.pur_date >= '$date->start' 
                    and c.pur_date <= '$date->end' 
                  order by 
                    c.pur_date desc, 
                    c.pur_no, 
                    a.pur_det_no
                  limit 
                    $limit
                  offset
                    $offset
                  ";
            $query  = $this->db->query($sql);
            $result = $query->result();
            return $result;
        }
    }

    function new_product_list($version = '1', $warehouse, $search, $limit, $offset) {
        $this->db = $this->load->database($this->get_naming($warehouse), true);
        if($version == '1') {
            $sql = "select 
                      xa.prod_no, 
                      xa.prod_code0, 
                      xa.prod_name0, 
                      ifnull(xb.Qty, 0) as OnHand, 
                      xa.prod_uom 
                    from 
                      tproduct xa 
                      left join (
                        select 
                          xx.prod_no, 
                          sum(xx.prod_on_hand) as Qty 
                        from 
                          tdgproduct xx 
                          left join tproduct xy on xx.prod_no = xy.prod_no 
                        where 
                          xy.is_delete = 0 
                          and xy.is_stok <= 1 
                          and (
                            datediff(now(), xy.create_date)
                          ) <= 7
                        group by 
                          xy.prod_no
                      ) xb on xa.prod_no = xb.prod_no 
                    where 
                      xa.is_delete = 0 
                      and xa.is_stok <= 1 
                      and (xa.prod_code0 like '%$search%')
                      and (
                        datediff(now(), xa.create_date)
                      ) <= 7
                    limit 
                      $limit
                    offset
                      $offset
                    ";
            $query  = $this->db->query($sql);
            $result = $query->result();
            return $result;
        }
    }

    function product_come_list($version = '1', $warehouse, $search, $date, $limit, $offset) {
        $this->db = $this->load->database($this->get_naming($warehouse), true);
        $date = $this->get_date($date);
        if($version == '1') {
            $sql = "select 
                      c.pur_no, 
                      c.pur_date, 
                      c.pur_ket, 
                      b.prod_code0, 
                      b.prod_name0, 
                      a.qty_satuan, 
                      if(
                        a.satuan = 3, 
                        b.prod_uom3, 
                        if(
                          a.satuan = 2, b.prod_uom2, b.prod_uom
                        )
                      ) as nm_satuan 
                    from 
                      td_purchase a 
                      left join tproduct b on a.prod_no = b.prod_no 
                      left join tpurchase c on a.pur_no = c.pur_no 
                    where 
                      c.is_delete = 0 
                      and c.pur_type = 1 
                      and (b.prod_code0 like '%$search%')
                      and c.pur_date >= '$date->start' 
                      and c.pur_date <= '$date->end' 
                    order by 
                      c.pur_date desc, 
                      c.pur_no, 
                      a.pur_det_no
                    limit 
                      $limit
                    offset
                      $offset
                    ";
            $query  = $this->db->query($sql);
            $result = $query->result();
            return $result;
        }
    }

    function madura_product_list($version = '1', $warehouse, $search, $limit, $offset) {
        $this->db = $this->load->database($this->get_naming($warehouse), true);
        if($version == '1') {
            $sql = "select 
                      xa.prod_no, 
                      xa.prod_code0, 
                      xa.prod_name0, 
                      ifnull(xb.Qty, 0) as OnHand, 
                      xa.prod_uom 
                    from 
                      tproduct xa 
                      left join (
                        select 
                          xx.prod_no, 
                          sum(xx.prod_on_hand) as Qty 
                        from 
                          tdgproduct xx 
                          left join tproduct xy on xx.prod_no = xy.prod_no 
                        where 
                          xy.is_delete = 0 
                          and xy.is_stok <= 1 
                          and xx.gud_no = 'ID2' 
                          /* Default gudang Madura */
                          and (
                            xy.prod_code0 like '%$search%' 
                            or xy.prod_name0 like '%$search%'
                          ) 
                        group by 
                          xy.prod_no
                      ) xb on xa.prod_no = xb.prod_no 
                    where 
                      xa.is_delete = 0 
                      and xa.is_stok <= 1 
                      and (
                        xa.prod_code0 like '%$search%' 
                        or xa.prod_name0 like '%$search%'
                      )
                    limit 
                      $limit
                    offset
                      $offset
                    ";
            $query  = $this->db->query($sql);
            $result = $query->result();
            return $result;
        }
    }

    function list_csw_opname($version = '1', $warehouse, $search, $limit, $offset) {
        $this->db = $this->load->database($this->get_naming($warehouse), true);
        if($version == '1') {
            $sql = "select 
                      xa.prod_no, 
                      xa.prod_code0, 
                      xa.prod_name0, 
                      ifnull(xb.Qty, 0) as OnHand, 
                      ifnull(xc.Qty, 0) as OnBranch, 
                      xa.prod_uom, 
                      concat(xa.konversi1, ' ', xa.prod_uom) as Unit1, 
                      if(
                        xa.konversi2 <> 0, 
                        concat(xa.konversi2, ' ', xa.prod_uom2), 
                        ''
                      ) as Unit2, 
                      if(
                        xa.konversi3 <> 0, 
                        concat(xa.konversi3, ' ', xa.prod_uom3), 
                        ''
                      ) as Unit3 
                    from 
                      tproduct xa 
                      left join (
                        select 
                          xx.prod_no, 
                          sum(xx.prod_on_hand) as Qty 
                        from 
                          tdgproduct_wh xx 
                          left join tproduct xy on xx.prod_no = xy.prod_no 
                        where 
                          xy.is_delete = 0 
                          and xy.is_stok <= 1 
                          and (
                            xy.prod_code0 like '%$search%' 
                            or xy.prod_name0 like '%$search%'
                          ) 
                        group by 
                          xy.prod_no
                      ) xb on xa.prod_no = xb.prod_no
                      left join (
                        select 
                          xx.prod_no, 
                          sum(xx.prod_on_hand) as Qty 
                        from 
                          tdgproduct xx 
                          left join tproduct xy on xx.prod_no = xy.prod_no 
                        where 
                          xy.is_delete = 0 
                          and xy.is_stok <= 1 
                          and (
                            xy.prod_code0 like '%$search%' 
                            or xy.prod_name0 like '%$search%'
                          ) 
                        group by 
                          xy.prod_no
                      ) xc on xa.prod_no = xc.prod_no  
                    where 
                      xa.is_delete = 0 
                      and xa.is_stok <= 1 
                      and (
                        xa.prod_code0 like '%$search%' 
                        or xa.prod_name0 like '%$search%'
                      )
                    limit 
                      $limit
                    offset
                      $offset
                    ";
            $query  = $this->db->query($sql);
            $result = $query->result();
            return $result;
        }
    }

    function list_csw_item($version = '1', $warehouse, $search, $date_start, $date_end) {
        $this->db = $this->load->database($this->get_naming($warehouse), true);
        if($version == '1') {
            $sql = "select 
                      a.adj_no, 
                      a.adj_date, 
                      adj_desc, 
                      ifnull(t1.user_name, '-') as user_create, 
                      b.gud_name,
                      b.gud_no,
                      b.gud_code 
                    from 
                      tadjust_wh a 
                      left join tusers t1 on a.user_id = t1.user_id 
                      left join tgudang b on a.gud_no = b.gud_no 
                    where 
                      a.is_delete = 0 
                      and a.is_adjust = 0 
                      and a.adj_date >= '$date_start 00:00:00' 
                      and a.adj_date <= '$date_end 23:59:59' 
                      and (
                        a.adj_no like '%$search%' 
                        or a.adj_desc like '%$search%'
                      ) 
                    order by 
                      a.adj_date desc 
                    limit 
                      100;
                    ";
            $query  = $this->db->query($sql);
            $result = $query->result();
            return $result;
        }
    }

    function po_purchase_report($version = '1', $warehouse, $search, $date, $limit, $offset) {
        $this->db = $this->load->database($this->get_naming($warehouse), true);
        $date = $this->get_date($date);
        if($version == '1') {
            $sql = "select 
                      b.prod_code0, 
                      b.prod_name0, 
                      a.qty_satuan as Qty_Order,
                      c.pur_ord_date, 
                      if(
                        a.satuan = 1, 
                        b.prod_uom, 
                        if(
                          a.satuan = 2, 
                          b.prod_uom2, 
                          if(
                            a.satuan = 3, b.prod_uom3, b.prod_uom
                          )
                        )
                      ) as nm_satuan, 
                      a.qty_datang / a.konversi as Qty_Kirim, 
                      (a.qty_order - a.qty_Datang)/ a.konversi as Qty_Sisa, 
                      concat(
                        d.person_code, ' - ', d.person_name
                      ) as nm_supplier, 
                      concat(
                        a.pur_ord, 
                        ' - ', 
                        date_format(c.pur_ord_date, '%d-%m-%Y')
                      ) as no_faktur 
                    from 
                      td_purchase_order a 
                      left join tproduct b on a.prod_no = b.prod_no 
                      left join tpurchase_order c on a.pur_ord = c.pur_ord 
                      left join tperson d on c.person_no = d.person_no 
                    where 
                      c.is_delete = 0
                      and c.pur_ord_date >= '$date->start'
                      and c.pur_ord_date <= '$date->end'
                      and (a.pur_ord like '%$search%')
                    order by 
                      d.person_code, 
                      c.pur_ord, 
                      a.det_pur_ord
                    limit 
                      $limit
                    offset
                      $offset";
            $query  = $this->db->query($sql);
            // echo $this->db->last_query(); exit;
            $result = $query->result();
            return $result;
        }
    }

    function discount_period_report($version = '1', $warehouse, $search, $date_init, $limit, $offset) {
        $this->db = $this->load->database($this->get_naming($warehouse), true);
        $where_date = "";
        if($date_init != '') {
            $date       = $this->get_date($date_init);
            $where_date = "and xa.disc_tgl1 >= '$date->start' and xa.disc_tgl1 <= '$date->end'";
        }

        if($version == '1') {
            $sql = "select 
                      xb.prod_code0, 
                      xb.prod_name0, 
                      if(
                        xa.is_member = 0, '*Non Member', '*Member'
                      ) as Status, 
                      xa.disc_tgl1, 
                      xa.disc_tgl2, 
                      if(
                        xa.status_disc = 0, 
                        'Disc. All Unit', 
                        if(
                          xa.status_disc = 1, 'Disc. Unit', ''
                        )
                      ) as StatusDisc, 
                      if(disc_type = 0, 'Rp', '%') as disc_prefix, 
                      disc_value, 
                      if(disc_type_1 = 0, 'Rp', '%') as disc_prefix_1, 
                      disc_value_1, 
                      if(disc_type_2 = 0, 'Rp', '%') as disc_prefix_2, 
                      disc_value_2, 
                      if(disc_type_3 = 0, 'Rp', '%') as disc_prefix_3, 
                      disc_value_3, 
                      qty_limit 
                    from 
                      tm_diskon xa 
                      left join tproduct xb on xa.prod_no = xb.prod_no 
                    where 
                      xa.is_delete = 0
                      $where_date
                      and (
                        xb.prod_code0 like '%$search%' 
                        or xb.prod_name0 like '%$search%'
                      )
                    limit 
                      $limit
                    offset
                      $offset
                    ";
            $query  = $this->db->query($sql);
            // echo $this->db->last_query(); exit;
            $result = $query->result();
            return $result;
        }
    }

    function promotion_setting_report($version = '1', $warehouse, $search, $limit, $offset) {
        $this->db = $this->load->database($this->get_naming($warehouse), true);
        if($version == '1') {
            $sql = "select 
                      xb.prod_code0, 
                      xb.prod_name0, 
                      xa.pro_tgl1, 
                      xa.pro_tgl2, 
                      xa.pro_qty, 
                      xc.prod_code0 as code_alias, 
                      xc.prod_name0 as name_alias
                    from 
                      tm_program xa 
                      left join tproduct xb on xa.prod_no = xb.prod_no 
                      left join tproduct xc on xa.prod_no2 = xc.prod_no 
                    where 
                      xa.is_delete = 0
                    limit 
                      $limit
                    offset
                      $offset
                    ";
            $query  = $this->db->query($sql);
            $result = $query->result();
            return $result;
        }
    }

    function csw_report($version = '1', $warehouse, $search, $date, $limit, $offset, $qty_in, $qty_out) {
        $this->db = $this->load->database($this->get_naming($warehouse), true);
        $date = $this->get_date($date);
        $where_qty_in   = '';
        $where_qty_out  = '';
        if($qty_in != '') {
          $where_qty_in = 'and a.det_qty_in '.$qty_in;
        }
        if($qty_out != '') {
          $where_qty_out = 'and a.det_qty_out '.$qty_out;
        }
        if($version == '1') {
            $sql = "select 
                      a.adj_no, 
                      c.adj_date, 
                      d.gud_name, 
                      b.prod_code0, 
                      b.prod_name0, 
                      0 as Qty, 
                      if(
                        a.satuan = 3, 
                        b.prod_uom3, 
                        if(
                          a.satuan = 2, b.prod_uom2, b.prod_uom
                        )
                      ) as nm_satuan, 
                      a.det_qty_in, 
                      a.det_qty_out, 
                      a.det_adj_desc, 
                      c.adj_desc 
                    from 
                      td_adjust_wh a 
                      left join tproduct b on a.prod_no = b.prod_no 
                      left join tadjust_wh c on a.adj_no = c.adj_no 
                      left join tgudang d on c.gud_no = d.gud_no 
                    where 
                      c.is_delete = 0 
                      and c.adj_date >= '$date->start' 
                      and c.adj_date <= '$date->end' 
                      $where_qty_in
                      $where_qty_out
                      and (a.adj_no like '%$search%')
                    order by 
                      c.adj_no, 
                      a.adj_det_no
                    limit 
                      $limit
                    offset
                      $offset
                    ";
            $query  = $this->db->query($sql);
            // echo $this->db->last_query(); exit;
            $result = $query->result();
            return $result;
        }
    }
}

/* End of file m_global.php */
/* Location: ./application/modules/global/models/m_global.php */