<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Transaction extends CI_Controller {

	function __construct() {
        parent::__construct();
        $this->headers          = $this->input->request_headers();
        $this->api_version      = $this->headers['Api-Version'];
        $this->master_db        = $this->load->database('default', true)->database;
        $this->status_gosend    = [
            'confirmed'             => 'Melakukan pencarian driver, harap menunggu',
            'allocated'             => 'Driver sedang menuju lokasi Sukses Jaya untuk mengambil pesanan',
            'out_for_pickup'        => 'Driver sedang menuju lokasi Sukses Jaya untuk mengambil pesanan',
            'picked'                => 'Driver sedang mengantar pesanan ke tujuan',
            'out_for_delivery'      => 'Driver sedang mengantar pesanan ke tujuan',
            'cancelled'             => 'Pesanan dibatalkan oleh Sukses Jaya',
            'delivered'             => 'Pesanan telah tiba di tujuan',
            'rejected'              => 'Driver mengembalikan pesanan ke Sukses Jaya',
            'no_driver'             => 'Gagal menemukan driver terdekat',
            'on_hold'               => 'Pesanan ditunda karena terkendala cuaca/penerima tidak dapat dihubungi/demonstrasi/dll'
        ];
        date_default_timezone_set('UTC');
        header('Content-Type: application/json');
        acc_token();
    }

    private function select_from_prod(){
        return "(select merk_id, cat_id, group_id, prod_no, prod_code0, prod_name0, prod_uom as Satuan1, prod_sell_price as Harga1, if(prod_sell_price4<>0, prod_uom2, '') as Satuan2, if (prod_sell_price4<>0,prod_sell_price4,'') as Harga2 from ".$this->master_db.".tproduct where is_delete = 0)";
    }

    public function index(){   
        echo "FH Transaction Rest API";
    }

    function checkout(){
        if($this->api_version == '1'){
            $is_purchases_enabled = $this->m_global->get_data_all('config/mobile', null, ['config_id' => '1'])[0]->config_enable_purchases;
            if($is_purchases_enabled == '0') {
                echo response_builder(false, 406, null, 'Fitur pembelian saat ini belum tersedia');
                exit;
            }
            $user_id        = $this->headers['User-Id'];
            $delivery_type  = $this->input->post('delivery_type');
            $address        = $this->input->post('address');
            $address_note   = $this->input->post('address_note');
            $latitude       = $this->input->post('latitude');
            $longitude      = $this->input->post('longitude');
            $delivery_fee   = $this->input->post('delivery_fee') ?: '0';

            $config         = $this->m_global->get_data_all('config/mobile')[0];
            $user_data      = $this->m_global->get_data_all('tperson', null, ['person_no' => $user_id])[0];
            $current_date   = date('Y-m-d H:i:s');
            $open_date      = date('Y-m-d').' '.$config->config_open_hour;
            $close_date     = date('Y-m-d').' '.$config->config_close_hour;
            if(($current_date >= $open_date) && ($current_date <= $close_date)){
                $data['order_user_id']          = $user_id;
                $data['order_address']          = $address;
                $data['order_address_note']     = $address_note;
                $data['order_delivery_type']    = $delivery_type;
                $data['order_status']           = 'confirmation';
                $data['order_latitude']         = $latitude;
                $data['order_longitude']        = $longitude;
                if($delivery_fee != '0' && $delivery_type == 'onlinedriver'){
                    $data['order_delivery_price']  = $delivery_fee;
                }

                $date                           = date('Y-m-d H:i:s');
                $data['order_createddate']      = $date;
                $data['order_lastupdate']       = $date;
                $data['order_payment_expiration_date'] = date('Y-m-d H:i:s', strtotime($date. ' + 3 days'));
                
                $result = $this->m_global->insert('order/mobile', $data);
                if($result['status']){

                    $id         = $result['id'];
                    $order_code = "CB-M".date('ymd').str_pad($id, 5, '0', STR_PAD_LEFT);
                    $total      = $this->insert_items($order_code);
                    $result     = $this->m_global->update(
                        'order/mobile', 
                        ['order_code' => $order_code, 'order_total' => $total[0], 'order_total_items' => $total[1]], 
                        ['order_id' => $id]
                    );
                    
                    if($result){
                        echo response_builder(true, 201, ['transaction_id' => $order_code]);
                    }else{
                        echo response_builder(false, 406, null, 'Gagal melakukan checkout');
                    }
                }else{
                    echo response_builder(false, 406, null, 'Gagal melakukan checkout');
                }
            }else{
                $opentime = explode(':', UTCtoWIBtime($config->config_open_hour));
                echo response_builder(false, 406, null, 'Toko sedang tutup, silahkan coba lagi pada jam '.$opentime[0].':'.$opentime[1].' WIB');
            }
        }else{
            echo response_builder(false, 900);
        }
    }


    private function insert_items($order_code){
        $user_id        = $this->headers['User-Id'];
        $total          = 0;
        $total_items    = 0;
        $data_cart      = $this->m_global->get_data_all('cart/mobile', null, ['cart_user_id' => $user_id]);
        foreach($data_cart as $value){
            $data_item['orderproduct_order_code']   = $order_code;
            $data_item['orderproduct_product_id']   = $value->cart_product_id;
            $data_item['orderproduct_price']        = $value->cart_price;
            $data_item['orderproduct_qty']          = $value->cart_qty;
            $data_item['orderproduct_unit']         = $value->cart_unit;
            $data_item['orderproduct_uom']          = $value->cart_uom;
            $data_item['orderproduct_note']         = $value->cart_note;
            $this->m_global->insert('order_products/mobile', $data_item);
            $total = $total + ($value->cart_qty * $value->cart_price);
            $total_items = $total_items + $value->cart_qty;
        }
        $this->m_global->delete('cart/mobile', ['cart_user_id' => $user_id]);
        return [$total, $total_items];
    }

    function transaction_list(){   
        if($this->api_version == '1'){
            $user_id    = $this->headers['User-Id'];
            $status     = $this->input->post('status');
            $date_start = $this->input->post('date_start');
            $date_end   = $this->input->post('date_end');
            $res_data   = [];
            $where['order_user_id'] = $user_id;
            $where_e    = null;    

            if($status != ''){
                if($status != 'unspecified'){
                    $where['order_status'] = $status; 
                }
            }

            if($date_start != '' && $date_end != ''){
                $where_e = "order_createddate between '$date_start 00:00:00' and '$date_end 23:59:59'";
            }

            $data       = $this->m_global->get_data_all('order/mobile', null, $where, '*', $where_e, ['order_createddate', 'desc']);

            foreach($data as $value){
                $res_data[] = [
                    'id' => $value->order_code,
                    'total' => $value->order_total,
                    'date' => $value->order_createddate,
                    'status' => $value->order_status,
                    'total_items' => (double)$value->order_total_items
                ];
            }

            echo response_builder(true, 200, $res_data);
        }else{
            echo response_builder(false, 900);
        }
    }

    function detail(){   
        if($this->api_version == '1'){
            $order_code = $this->input->post('transaction_id');
            $join = [
                ['table' => 'payment_merchant', 'on' => 'paymerchant_id=order_payment_merchant_id'],
                ['table' => 'payment_method', 'on' => 'paymerchant_method_id=paymethod_id']
            ];
            $data           = $this->m_global->get_data_all('order/mobile', $join, ['order_code' => $order_code])[0];
            $data_config    = $this->m_global->get_data_all('config/mobile', null, ['config_id' => '1'])[0];

            $res_data['id']                         = $order_code;
            $res_data['total']                      = (Int) $data->order_total;
            $res_data['grand_total']                = $data->order_total + $data->order_delivery_price;
            $res_data['date']                       = $data->order_createddate;
            $res_data['status']                     = $data->order_status;
            $res_data['total_items']                = (double) $data->order_total_items;
            $res_data['payment']['method']          = $data->paymethod_name;
            $res_data['payment']['merchant']        = $data->paymerchant_name;
            $res_data['payment']['merchant_icon']   = $data->paymerchant_thumbnail;
            $res_data['payment']['account_no']      = $data->paymerchant_account_no;
            $res_data['payment']['account_name']    = $data->paymerchant_account_name;
            $res_data['payment']['expiration']      = $data->order_payment_expiration_date;
            $res_data['delivery']['address']        = $data->order_address;
            $res_data['delivery']['delivery_type']  = $data->order_delivery_type;
            $res_data['delivery']['delivery_fee']   = (Int) $data->order_delivery_price;
            $res_data['delivery']['driver_name']    = $data->order_driver_name ?: "";
            $res_data['delivery']['driver_phone']   = $data->order_driver_phone ?: "";
            $res_data['delivery']['store_address']  = $data_config->config_store_address;
            $res_data['delivery']['store_latitude'] = $data_config->config_store_latitude;
            $res_data['delivery']['store_longitude'] = $data_config->config_store_longitude;
            $res_data['delivery']['destination_latitude'] = $data->order_latitude;
            $res_data['delivery']['destination_longitude'] = $data->order_longitude;
            if($data->order_gosend_no != ''){
                $data_gosend = $this->gosend_booking_status($data->order_gosend_no);
                $res_data['delivery']['tracking_url'] = $data_gosend->liveTrackingUrl; 
                $res_data['delivery']['status']       = $data_gosend->bookingStatus; 
                $res_data['delivery']['status_description'] = $this->status_gosend[$data_gosend->bookingStatus]; 
            }

            $join = [
                ['table' => $this->select_from_prod().' tp', 'on' => 'tp.prod_no=orderproduct_product_id']
            ];
            $data_product   = $this->m_global->get_data_all('order_products/mobile', $join, ['orderproduct_order_code' => $order_code]);
            $res_data_product   = [];
            foreach($data_product as $value){
                $subtotal = $value->orderproduct_qty * $value->orderproduct_price;
                $data_image = $this->m_global->get_data_all('product_thumbnails/mobile', null, ['prodthumb_product_id' => $value->prod_no], 'prodthumb_url', null, null, 0, 1);
                if(count($data_image)){
                    $image = $data_image[0]->prodthumb_url;
                }else{
                    $image = "";
                }
                $res_data['products'][] = [
                    'product_id' => $value->orderproduct_product_id,
                    'product_thumbnail' => $image,
                    'product_title' => $value->prod_name0,
                    'product_note' => $value->orderproduct_note ?: "",
                    'product_qty' => $value->orderproduct_qty,
                    'product_sub_total' => $subtotal,
                    'product_purchased_price' => ['price' => $value->orderproduct_price, 'uom' => $value->orderproduct_uom]
                ];
            }

            echo response_builder(true, 200, $res_data);
        }else{
            echo response_builder(false, 900);
        }
    }

    function update_status(){
        if($this->api_version == '1'){
            $order_code = $this->input->post('transaction_id');
            $status     = $this->input->post('status');

            $data['order_cancel_reason']    = ($status == 'canceled' ? 'Order canceled by Customer' : '');
            $data['order_status']           = $status;
            $result = $this->m_global->update('order/mobile', $data, ['order_code' => $order_code]);
            if($result){
                echo response_builder(true, 201, null, ($status == 'success' ? 'Pesananmu telah selesai' : 'Pesananmu telah dibatalkan'));
            }else{
                echo response_builder(false, 406, null, 'failed update data');
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    function payment(){
        if($this->api_version == '1'){
            $order_code         = $this->input->post('transaction_id');
            $payment_proof_link = $this->input->post('payment_proof_link');

            $order_data         = $this->m_global->get_data_all('order/mobile', null, ['order_code' => $order_code])[0];
            $datenow            = new DateTime(date('Y-m-d H:i:s'));
            $date_exp           = new DateTime($order_data->order_payment_expiration_date);
            if($datenow > $date_exp){
                echo response_builder(false, 406, null, 'Batas waktu pembayaran sudah habis');
                exit;
            }

            $data['order_payment_proof'] = $payment_proof_link;
            $data['order_status']        = 'payment_confirmation';
            $result = $this->m_global->update('order/mobile', $data, ['order_code' => $order_code]);
            if($result){
                echo response_builder(true, 201);
            }else{
                echo response_builder(false, 406, null, 'Gagal mengunggah bukti pembayaran');
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    function test_lepek(){
        $data = $this->m_global->get_data_all('tsales_order');
        foreach ($data as $value) {
            $datadet = $this->m_global->get_data_all('td_sales_order');
            $data['detail'] = $datadet;
        }
        echo response_builder(true, 200, $data);
    }

    function gosend_booking_status($gosend_no){
        $ch  = curl_init();
        curl_setopt( $ch, CURLOPT_URL, GOSEND_BOOKING_URL.'/orderno/'.$gosend_no);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
            'Client-ID: '.GOSEND_CLIENT_ID,
            'Pass-Key: '.GOSEND_PASS_KEY
        ));
        $result = curl_exec($ch);
        $result = json_decode($result, true);
        curl_close($ch);

        if(array_key_exists('status', $result)){
            return (object) $result;
        }else{
            return '';
        }
    }

}

/* End of file config.php */
/* banner: ./application/modules/config/controllers/config.php */