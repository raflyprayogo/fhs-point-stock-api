<?php
$type = 'TrueType';
$name = 'MSSansSerif6';
$desc = array('Ascent'=>772,'Descent'=>-227,'CapHeight'=>772,'Flags'=>32,'FontBBox'=>'[-45 -272 1045 818]','ItalicAngle'=>0,'StemV'=>70,'MissingWidth'=>640);
$up = 100;
$ut = 50;
$cw = array(
	chr(0)=>640,chr(1)=>640,chr(2)=>640,chr(3)=>640,chr(4)=>640,chr(5)=>640,chr(6)=>640,chr(7)=>640,chr(8)=>640,chr(9)=>640,chr(10)=>640,chr(11)=>640,chr(12)=>640,chr(13)=>640,chr(14)=>640,chr(15)=>640,chr(16)=>640,chr(17)=>640,chr(18)=>640,chr(19)=>640,chr(20)=>640,chr(21)=>640,
	chr(22)=>640,chr(23)=>640,chr(24)=>640,chr(25)=>640,chr(26)=>640,chr(27)=>640,chr(28)=>640,chr(29)=>640,chr(30)=>640,chr(31)=>640,' '=>272,'!'=>182,'"'=>318,'#'=>545,'$'=>545,'%'=>954,'&'=>727,'\''=>136,'('=>318,')'=>272,'*'=>409,'+'=>545,
	','=>182,'-'=>318,'.'=>182,'/'=>318,'0'=>500,'1'=>363,'2'=>545,'3'=>545,'4'=>545,'5'=>545,'6'=>545,'7'=>545,'8'=>545,'9'=>545,':'=>182,';'=>182,'<'=>591,'='=>591,'>'=>591,'?'=>500,'@'=>1045,'A'=>682,
	'B'=>682,'C'=>772,'D'=>727,'E'=>772,'F'=>772,'G'=>772,'H'=>682,'I'=>182,'J'=>454,'K'=>772,'L'=>636,'M'=>818,'N'=>727,'O'=>818,'P'=>727,'Q'=>818,'R'=>772,'S'=>682,'T'=>727,'U'=>682,'V'=>682,'W'=>1000,
	'X'=>772,'Y'=>727,'Z'=>682,'['=>272,'\\'=>318,']'=>272,'^'=>454,'_'=>545,'`'=>318,'a'=>591,'b'=>591,'c'=>545,'d'=>545,'e'=>591,'f'=>272,'g'=>545,'h'=>500,'i'=>182,'j'=>182,'k'=>545,'l'=>182,'m'=>818,
	'n'=>500,'o'=>591,'p'=>591,'q'=>545,'r'=>363,'s'=>500,'t'=>272,'u'=>500,'v'=>500,'w'=>818,'x'=>500,'y'=>500,'z'=>500,'{'=>318,'|'=>182,'}'=>318,'~'=>545,chr(127)=>640,chr(128)=>640,chr(129)=>640,chr(130)=>640,chr(131)=>640,
	chr(132)=>640,chr(133)=>640,chr(134)=>640,chr(135)=>640,chr(136)=>640,chr(137)=>640,chr(138)=>640,chr(139)=>640,chr(140)=>640,chr(141)=>640,chr(142)=>640,chr(143)=>640,chr(144)=>640,chr(145)=>182,chr(146)=>182,chr(147)=>318,chr(148)=>318,chr(149)=>640,chr(150)=>640,chr(151)=>640,chr(152)=>640,chr(153)=>640,
	chr(154)=>640,chr(155)=>640,chr(156)=>640,chr(157)=>640,chr(158)=>640,chr(159)=>640,chr(160)=>640,chr(161)=>640,chr(162)=>640,chr(163)=>640,chr(164)=>640,chr(165)=>640,chr(166)=>640,chr(167)=>640,chr(168)=>640,chr(169)=>640,chr(170)=>640,chr(171)=>640,chr(172)=>640,chr(173)=>640,chr(174)=>640,chr(175)=>640,
	chr(176)=>640,chr(177)=>640,chr(178)=>640,chr(179)=>640,chr(180)=>640,chr(181)=>640,chr(182)=>640,chr(183)=>640,chr(184)=>640,chr(185)=>640,chr(186)=>640,chr(187)=>640,chr(188)=>640,chr(189)=>640,chr(190)=>640,chr(191)=>640,chr(192)=>640,chr(193)=>640,chr(194)=>640,chr(195)=>640,chr(196)=>640,chr(197)=>640,
	chr(198)=>640,chr(199)=>640,chr(200)=>640,chr(201)=>640,chr(202)=>640,chr(203)=>640,chr(204)=>640,chr(205)=>640,chr(206)=>640,chr(207)=>640,chr(208)=>640,chr(209)=>640,chr(210)=>640,chr(211)=>640,chr(212)=>640,chr(213)=>640,chr(214)=>640,chr(215)=>640,chr(216)=>640,chr(217)=>640,chr(218)=>640,chr(219)=>640,
	chr(220)=>640,chr(221)=>640,chr(222)=>640,chr(223)=>640,chr(224)=>640,chr(225)=>640,chr(226)=>640,chr(227)=>640,chr(228)=>640,chr(229)=>640,chr(230)=>640,chr(231)=>640,chr(232)=>640,chr(233)=>640,chr(234)=>640,chr(235)=>640,chr(236)=>640,chr(237)=>640,chr(238)=>640,chr(239)=>640,chr(240)=>640,chr(241)=>640,
	chr(242)=>640,chr(243)=>640,chr(244)=>640,chr(245)=>640,chr(246)=>640,chr(247)=>640,chr(248)=>640,chr(249)=>640,chr(250)=>640,chr(251)=>640,chr(252)=>640,chr(253)=>640,chr(254)=>640,chr(255)=>640);
$enc = 'cp1252';
$uv = array(0=>array(0,128),128=>8364,130=>8218,131=>402,132=>8222,133=>8230,134=>array(8224,2),136=>710,137=>8240,138=>352,139=>8249,140=>338,142=>381,145=>array(8216,2),147=>array(8220,2),149=>8226,150=>array(8211,2),152=>732,153=>8482,154=>353,155=>8250,156=>339,158=>382,159=>376,160=>array(160,96));
$file = 'ms-sans-serif-6.z';
$originalsize = 23872;
$subsetted = true;
?>
